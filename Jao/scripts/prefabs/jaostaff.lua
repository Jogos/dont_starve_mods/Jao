-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943).             --
-- Any unauthorized use will be reported to the DMCA.                            --
-- To use any file or sprite ask my permission.                                  --
--                                                                               --   
-- Author: Paulo Victor de Oliveira Leal                                         --
-- Contact: ciclopiano@gmail.com                                                 --

local assets= {
    Asset("ANIM", "anim/jaostaff.zip"),
    Asset("ANIM", "anim/swap_jaostaff.zip"),
    ------------------------------------------
    Asset("SOUND", "sound/common.fsb"),
    ------------------------------------------
    Asset("ATLAS", "images/inventoryimages/jaostaff.xml"),
    Asset("IMAGE", "images/inventoryimages/jaostaff.tex"),
}

local prefabs = {"torchfire",}


local function OnSave(inst, data)
    if inst.respawntime ~= nil then
        local time = GetTime()
        if inst.respawntime > time then
            data.respawntimeremaining = inst.respawntime - time
        end
    end
end

local function OnLoad(inst, data)
    if data == nil then
        return
    end

    if data.respawntimeremaining ~= nil then
        inst.respawntime = data.respawntimeremaining + GetTime()
    end
end

local function onattack_jaostaff(inst, attacker, target)
    if attacker and attacker.components.sanity then
        attacker.components.sanity:DoDelta(-1)
    end

    if target.components.combat ~= nil then
        target.components.combat:SuggestTarget(attacker)
    end

    if target.components.burnable ~= nil then
        if target.components.burnable:IsBurning() then
            target.components.burnable:Extinguish()
        elseif target.components.burnable:IsSmoldering() then
            target.components.burnable:SmotherSmolder()
        end
    end

    if target.components.freezable ~= nil then
        target.components.freezable:AddColdness(1)
        target.components.freezable:SpawnShatterFX()
    end
end


local function fn()

    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("jaostaff")
    inst.AnimState:SetBuild("jaostaff")
    inst.AnimState:PlayAnimation("idle")
    
    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddTag("shadow")

    local function OnEquip(inst, owner)
        owner.AnimState:OverrideSymbol("swap_object", "swap_jaostaff", "swap_orbstaff")
        owner.AnimState:Show("ARM_carry")
        owner.AnimState:Hide("ARM_normal")

        if inst.fire == nil then
            inst.fire = SpawnPrefab("yellowamuletlight")
            local follower = inst.fire.entity:AddFollower()
            follower:FollowSymbol(owner.GUID, "swap_object", 0, -220, 1)
        end
    end

    local function OnUnequip(inst, owner)
        owner.AnimState:OverrideSymbol("swap_object", "swap_jaostaff", "swap_orbstaff")
        owner.AnimState:Hide("ARM_carry")
        owner.AnimState:Show("ARM_normal")
        owner.components.combat.damage = owner.components.combat.defaultdamage 

        if inst.fire ~= nil then
            inst.fire:Remove()
            inst.fire = nil
        end
    end

    local function onblink(staff, pos, caster)
        if caster.components.sanity ~= nil then
            caster.components.sanity:DoDelta(-1)
        end
    end 

    inst.entity:SetPristine()

    inst:AddComponent("lighter")


    inst:AddComponent("blinkstaff")
    inst.components.blinkstaff.onblinkfn = onblink

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.keepondeath = true
    inst.components.inventoryitem.imagename = "jaostaff"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/jaostaff.xml"

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip( OnEquip )
    inst.components.equippable:SetOnUnequip( OnUnequip )
    inst.components.inventoryitem.keepondeath = true

    inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab("ice")
    inst.components.periodicspawner:SetRandomTimes(TUNING.MUSHROOMHAT_SPORE_TIME, 1, true)


    inst:AddComponent("inspectable")

    inst:AddComponent("weapon")
    inst.components.weapon:SetOnAttack(onattack_jaostaff)
    inst.components.weapon:SetDamage(10)
    inst.components.weapon:SetRange(8, 10)
    inst.components.weapon:SetProjectile("ice_projectile")

    inst.OnLoad = OnLoad
    inst.OnSave = OnSave
    
    return inst 
end

return  Prefab("common/inventory/jaostaff", fn, assets)