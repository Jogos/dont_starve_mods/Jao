-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		     --
-- Any unauthorized use will be reported to the DMCA. 				             --
-- To use any file or sprite ask my permission.					                 --
--										                                         --
-- Author: Paulo Victor de Oliveira Leal					                     --
-- Contact: ciclopiano@gmail.com						                         --


local brain = require "brains/summonrhinobrain"
local summon = require "base/summon"

local assets = {
    Asset("ATLAS", "images/map_icons/summons/rhino.xml"),
    Asset("ATLAS", "images/map_icons/summons/rhino.xml"),
    Asset("ANIM", "anim/rook_rhino.zip"),
    Asset("SOUND", "sound/chess.fsb"),
}

local prefabs = {
    "jao",
    "sourceofmagic",
}

local function onSmashOther(inst, other)
    if not other:IsValid() then
        return
    elseif other.components.health ~= nil and not other.components.health:IsDead() then
        if other:HasTag("smashable") then
            other.components.health:Kill()
        else
            SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
            inst.SoundEmitter:PlaySound("dontstarve/creatures/rook/explo")
            inst.components.combat:DoAttack(other)
        end
    elseif other.components.workable ~= nil and other.components.workable:CanBeWorked() then
        SpawnPrefab("collapse_small").Transform:SetPosition(other.Transform:GetWorldPosition())
        other.components.workable:Destroy(inst)
    end
    if (not other:HasTag("smallcreature")) and (not other.prefab == "seeds") and (not other.prefab == "seeds_cooked") and
       (not other.prefab == "flower") and (not other.prefab == "flower_evil") and (not other.prefab == "depleted_grass") and 
       (not other.prefab == "grass") and (not other.prefab == "summonedbyplayer") and (not other.prefab == "player") then 
        inst.components.locomotor:Stop()
    end    
end

local function onCollide(inst, other)
    if other == nil or not other:IsValid() or other:HasTag("player") or
        Vector3(inst.Physics:GetVelocity()):LengthSq() < 42 then
            return
    end
    ShakeAllCameras(CAMERASHAKE.SIDE, .5, .05, .1, inst, 40)
    inst:DoTaskInTime(2 * FRAMES, onSmashOther, other)
end


-- Principal
local function fn()
    local inst = summon:create_summon(TUNING.JAO.SPEECH.RHINO, true)
    MakeCharacterPhysics(inst, 100, 2.2)
    inst.DynamicShadow:SetSize(5, 3)

    inst.Physics:SetCylinder(2.2, 4)
    inst.Physics:SetCollisionCallback(onCollide)

    summon:removeIfAlreadyExist("summonrhino")

    inst.kind = ""
    inst.soundpath = "dontstarve/creatures/rook/"
    inst.effortsound = "dontstarve/creatures/rook/steam"
    inst.AnimState:SetBank("rook")
    inst.AnimState:SetBuild("rook_rhino")

    summon:addToMinimap(inst, "rhino.tex")

    summon:addTag(inst, "summonrhino")

    summon:canTalk(inst)
    summon:canWalk(inst, TUNING.MINOTAUR_WALK_SPEED + 3, TUNING.MINOTAUR_RUN_SPEED)


    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    summon:canFight(inst, 40, TUNING.MINOTAUR_ATTACK_PERIOD, true)
    summon:canBeBurned(inst, "pig_torso")    
    summon:canBeHurt(inst)
    summon:canFollow(inst, true)
    summon:hasHealth(inst, 500)
    summon:hasInventory(inst)
    inst:RemoveTag("running")
    summon:setScale(inst, 0)
    summon:canEat(inst, {FOODTYPE.MEAT})
    summon:canPoop(inst, "poop")
    summon:canTrade(inst)
    summon:canSleep(inst)
    summon:addLeader(inst)

    inst:SetBrain(brain)
    inst:SetStateGraph("SGsummonrhino")

    inst.destroy = false
    inst.target = nil
    inst.targetEntity = nil

    inst:ListenForEvent("death", function ( inst )
        inst.components.follower.leader.rhinoInvocado = false
    end)

    return inst
end

return Prefab("common/summons/summonrhino", fn, assets, prefabs)