-----------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		     --
-- Any unauthorized use will be reported to the DMCA. 				             --
-- To use any file or sprite ask my permission.					                 --
--										                                         --
-- Author: Paulo Victor de Oliveira Leal					                     --
-- Contact: ciclopiano@gmail.com						                         --

local brain = require "brains/summonskipbrain"
local summon = require "base/summon"

local assets = {
    Asset("ATLAS", "images/map_icons/summons/rocky.xml"),
    Asset("ATLAS", "images/map_icons/summons/rocky.xml"),
    Asset("ANIM",  "anim/rocky.zip"),
    Asset("SOUND", "sound/rocklobster.fsb"),
}

local prefabs = {
    "jao",
    "sourceofmagic"
}

local function fn()
    local inst = summon:create_summon(TUNING.JAO.SPEECH.SKIP, true)    
    MakeCharacterPhysics(inst, 30, .3)        
    inst.DynamicShadow:SetSize(2, 1.5)

    summon:removeIfAlreadyExist("summonskip")

    inst.AnimState:SetBank("rocky")
    inst.AnimState:SetBuild("rocky")
    inst.AnimState:PlayAnimation("idle_loop")

    summon:addToMinimap(inst, "rocky.tex")

    summon:addTag(inst, "summonskip")
    
    summon:canTalk(inst)

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    summon:canFight(inst, 5, 5, false)
    summon:canBeHurt(inst)
    summon:affectsAura(inst, 10)
    summon:canFollow(inst, true)
    summon:hasHealth(inst, 800)
    summon:hasInventory(inst)
    summon:canWalk(inst, 6, 9)
    summon:setScale(inst, 0)
    summon:canEat(inst, {FOODTYPE.ELEMENTAL})
    summon:canPoop(inst, "poop")
    summon:canTrade(inst)
    summon:canSleep(inst)
    summon:addLeader(inst)

    inst:SetBrain(brain)
    inst:SetStateGraph("SGsummonskip")

    inst.pick = false

    inst:ListenForEvent("death", function ( inst )
        inst.components.follower.leader.skipInvocado = false
    end)

    return inst
end

return Prefab("common/summons/summonskip", fn, assets, prefabs)