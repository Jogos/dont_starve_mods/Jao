-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		     --
-- Any unauthorized use will be reported to the DMCA. 				             --
-- To use any file or sprite ask my permission.					                 --
--										                                         --
-- Author: Paulo Victor de Oliveira Leal					                     --
-- Contact: ciclopiano@gmail.com						                         --

local normalbrain = require "brains/summonaronbrain"
local werearonbrain = require "brains/summonaronbrain"
local summon = require "base/summon"

local assets = {
    Asset("SOUND", "sound/pig.fsb"),
    Asset("ANIM", "anim/manrabbit_basic.zip"),
    Asset("ANIM", "anim/manrabbit_actions.zip"),
    Asset("ANIM", "anim/manrabbit_attacks.zip"),
    Asset("ANIM", "anim/manrabbit_build.zip"),
    Asset("SOUND", "sound/bunnyman.fsb"),
    Asset("ANIM", "anim/manrabbit_beard_build.zip"),
    Asset("ANIM", "anim/manrabbit_beard_basic.zip"),
    Asset("ANIM", "anim/manrabbit_beard_actions.zip"),
}

local prefabs = {
    "jao",
    "sourceofmagic"
}

local function WerearonRetargetFn(inst)
    return FindEntity(
        inst,
        SpringCombatMod(TUNING.PIG_TARGET_DIST),
        function(guy)
            return inst.components.combat:CanTarget(guy) and (not guy:HasTag("wall")) and (not guy:HasTag("summonedbyplayer")) and (not guy:HasTag("jaobuilder"))
               and not (guy.sg ~= nil and guy.sg:HasStateTag("transform"))
        end,
        { "_combat" }, { "werepig", "alwaysblock", "beaver" }
    )
end

local function SetNormalAron(inst)
    inst.AnimState:SetBuild("rabbit_winter_build")
    inst.AnimState:SetBank("rabbit")
    inst.AnimState:Hide("hat")

    inst:SetBrain(normalbrain)
    inst:SetStateGraph("SGsummonaronbasic")

    inst:RemoveTag("werearon")

    summon:canFight(inst, 1, 5)  
    summon:canWalk(inst, 8, 12)

    inst.components.werebeast:SetOnNormalFn(SetNormalAron)
end

local function SetWereAron(inst)
    inst.AnimState:SetBuild("manrabbit_build")
    inst.AnimState:SetBank("manrabbit")

    inst:AddTag("werearon")
    summon:canWalk(inst, 6, 9)

    inst:SetBrain(werearonbrain)
    inst:SetStateGraph("SGsummonaron")

    if inst.hunterMode then
        summon:canFight(inst, 45, 1)
        inst.components.combat:SetRetargetFunction(3, WerearonRetargetFn)
    else
        summon:canFight(inst, 1, 5)  
    end            
end

local function change_form( inst )
    local fx = SpawnPrefab("statue_transition_2")
    fx.Transform:SetScale(3, 3, 3)
    fx.Transform:SetPosition(inst:GetPosition():Get())
    inst.SoundEmitter:PlaySound("dontstarve/common/deathpoof")
    if inst.transformed then
        inst.components.talker:Say(TUNING.JAO.SPEECH.ARON.ACTION.DESTRANSFORMA)
        SetNormalAron(inst)
    else
        inst.components.talker:Say(TUNING.JAO.SPEECH.ARON.ACTION.TRANSFORMA)
        SetWereAron(inst)
    end
    inst.transformed = not inst.transformed
end

local function common()
    local inst = summon:create_summon(TUNING.JAO.SPEECH.ARON, true)   
    MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, .75)

    summon:removeIfAlreadyExist("summonaron")
    
    inst.AnimState:SetBank("rabbit")
    inst.AnimState:Hide("hat")
    
    summon:addToMinimap(inst, "aron.tex")

    summon:addTag(inst, "summonaron")

    summon:canTalk(inst)

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    summon:canFight(inst, 45, 1)
    summon:canBeHurt(inst)
    summon:canFollow(inst, true)
    summon:hasHealth(inst, 350)
    summon:hasInventory(inst)
    summon:canWalk(inst, 8, 5)
    summon:setScale(inst, 0.5)
    summon:canEat(inst, {FOODTYPE.OMNI})
    summon:canSleep(inst)
    summon:canPoop(inst, "poop")
    summon:canTrade(inst)
    summon:canSleep(inst)
    summon:addLeader(inst)
    summon:canBeBurned(inst, "pig_torso")
    summon:canBeFreezed(inst, "pig_torso") 

    inst:AddComponent("werebeast")
    inst.components.werebeast:SetOnWereFn(SetWereAron)
    inst.components.werebeast:SetTriggerLimit(4)

    inst.setWP = SetWereAron
    inst.setNP = SetNormalAron
    inst.transformed = false
    inst.morph = change_form
    inst.hunterMode = false
    inst.area = true

    inst.components.combat:SetKeepTargetFunction(function (inst, target)
        return inst.components.combat:CanTarget(target)
               and not target:HasTag("summonedbyplayer")
               and not target:HasTag("player")
               and not target:HasTag("jaobuilder")
               and not (target.sg ~= nil and target.sg:HasStateTag("transform"))
    end)

    inst:ListenForEvent("onhitother", function (inst, data)
        local other = data.target
        local random = math.random(0,6)
        if other and other.components.freezable and random == 1 and inst.area then
            other.components.freezable:AddColdness(2)
            other.components.freezable:SpawnShatterFX()
        end
    end)

    inst:ListenForEvent("death", function ( inst )
        inst.components.follower.leader.aronInvocado = false
    end)

    return inst
end

local function normal()
    local inst = common()

    if not TheWorld.ismastersim then
        return inst
    end

    SetNormalAron(inst)

    return inst
end

return Prefab("summon/summonaron", normal, assets, prefabs)