-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		     --
-- Any unauthorized use will be reported to the DMCA. 				             --
-- To use any file or sprite ask my permission.					                 --
--										                                         --
-- Author: Paulo Victor de Oliveira Leal					                     --
-- Contact: ciclopiano@gmail.com						                         --

local brain = require "brains/summonchopbrain"
local summon = require "base/summon"

local assets = {
    ------------------------------------------
    Asset("ATLAS", "images/map_icons/summons/chop.xml"),
    Asset("ATLAS", "images/map_icons/summons/chop.xml"),
    Asset("ANIM", "anim/leif_walking.zip"),
    Asset("ANIM", "anim/leif_actions.zip"),
    Asset("ANIM", "anim/leif_attacks.zip"),
    Asset("ANIM", "anim/leif_idles.zip"),
    Asset("ANIM", "anim/leif_build.zip"),
    Asset("ANIM", "anim/leif_lumpy_build.zip"),
    Asset("SOUND", "sound/leif.fsb"),
}

local prefabs = {
    "jao",
    "groundpound_fx",
    "groundpoundring_fx",
    "character_fire",
    "sourceofmagic"
}

local function spawnFireflies(inst, x, z  )
    local base_x, base_y, base_z =  inst.Transform:GetWorldPosition()
    local firefly = SpawnPrefab("fireflies")
    firefly.Transform:SetPosition( base_x+x, base_y, base_z+z)
end

local function fn()

    local inst = summon:create_summon(TUNING.JAO.SPEECH.CHOP, true)
    MakeCharacterPhysics(inst, 1000, .5)
    inst.DynamicShadow:SetSize(4, 1.5)

    summon:removeIfAlreadyExist("summonchop")

    inst.AnimState:SetBank("leif")
    inst.AnimState:SetBuild("leif_build")
    inst.AnimState:PlayAnimation("idle_loop")

    summon:addToMinimap(inst, "chop.tex")

    summon:addTag(inst, "summonchop")
    inst:AddTag("tree")

    summon:canTalk(inst)

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    summon:canFight(inst, 75, 2, false)
    summon:canBeHurt(inst)
    summon:canFollow(inst, true)
    summon:hasHealth(inst, 600)
    summon:hasInventory(inst)
    summon:canWalk(inst, 3.5, 3.5)
    summon:setScale(inst, 0)
    summon:canEat(inst, {FOODTYPE.VEGGIE})
    summon:canPoop(inst, "pinecone")
    summon:canTrade(inst)
    summon:addLeader(inst)
    summon:canGroundPound(inst)
    summon:canBeBurned(inst, "marker")
    summon:canBeFreezed(inst, "marker")

    inst:SetBrain(brain)
    inst:SetStateGraph("SGsummonchop")

    inst.protegendo = false

    inst:ListenForEvent("death", function ( inst )
        inst.components.follower.leader.chopInvocado = false
        local base_x, base_y, base_z = inst.Transform:GetWorldPosition()
        local chopinhos = TheSim:FindEntities(base_x, base_y, base_z, 40, {"summonminichop"})
        for n, minichop in pairs(chopinhos) do
            minichop.components.health:Kill( )
        end
    end)

    inst:WatchWorldState("isnight", function ( inst )
        spawnFireflies(inst, 10, 0)
        spawnFireflies(inst, 0, 10)
        spawnFireflies(inst, -10, 0)
        spawnFireflies(inst, 0, -10)
        spawnFireflies(inst, 0, 0)
    end)

    return inst
end

return Prefab("common/summons/summonchop", fn, assets, prefabs)