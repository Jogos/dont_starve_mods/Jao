-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		     --
-- Any unauthorized use will be reported to the DMCA. 				             --
-- To use any file or sprite ask my permission.					                 --
--										                                         --
-- Author: Paulo Victor de Oliveira Leal					                     --
-- Contact: ciclopiano@gmail.com						                         --

local brain = require "brains/summonjarvibrain"
local summon = require "base/summon"


local assets = {
    Asset("ANIM", "anim/shadow_insanity2_basic.zip"),
}


local prefabs = {
    "jao",
    "sourceofmagic"
}

local SPEECH = TUNING.JAO.SPEECH


local function CalcSanityAura(inst, observer)
    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x,y,z, 25, {"summonedbyplayer"})
    local n = 0
    for k,aron in pairs(ents) do
        n = k
    end
    if n == 1 or n == 0 then
        return TUNING.SANITYAURA_HUGE
    elseif n == 2 then
        return TUNING.SANITYAURA_LARGE
    elseif n == 3 then
        return TUNING.SANITYAURA_MED
    elseif n == 4 then
        return TUNING.SANITYAURA_SMALL
    elseif n > 5 then
        return TUNING.SANITYAURA_TINY
    else
        return 1                    
    end    
    
end

local function getPetStatus(inst)
    local x,y,z = inst.Transform:GetWorldPosition()
    local pets = TheSim:FindEntities(x,y,z, 25, {"summonedbyplayer"})
    for k,v in pairs(pets) do
        local vida = v.components.health:GetPercent()
        if v:HasTag("summonskip") then
            if vida < .3 then
                inst.components.talker:Say(SPEECH.JARVI.SKIP)
            end
        elseif v:HasTag("summonrhino") then
            if vida < .3 then
                inst.components.talker:Say(SPEECH.JARVI.RHINO)
            end
        elseif v:HasTag("summonchop") then
            if vida < .3 then
                inst.components.talker:Say(SPEECH.JARVI.CHOP)
            end
        elseif v:HasTag("summonjill") then
            if vida < .3 then
                inst.components.talker:Say(SPEECH.JARVI.JILL)
            end
        elseif v:HasTag("summonaron") then
            if vida < .3 then
                inst.components.talker:Say(SPEECH.JARVI.ARON)
            end        
        end
    end
end

local function getMasterStatus( inst )
    local mestre = inst.components.follower.leader
    if mestre ~= nil then
        if mestre.components.health:GetPercent() < 0.5 then
            inst.components.talker:Say(SPEECH.JARVI.JAO.HEALTH)
        end
        
        if mestre.components.sanity:GetPercent() < 0.5 then
            inst.components.talker:Say(SPEECH.JARVI.JAO.SANITY)
        end
        
        if mestre.components.hunger:GetPercent() < 0.5 then
            inst.components.talker:Say(SPEECH.JARVI.JAO.HUNGER)
        end
    end
end

local function prepareEnemies( inst )
    local mestre = inst.components.follower.leader
    if mestre ~= nil then
        local numMons = 0
        local x,y,z = mestre.Transform:GetWorldPosition()
        local monsters = TheSim:FindEntities(x,y,z, 10)
        for n,v in pairs(monsters) do
            if v:HasTag("epic") then
                inst.components.talker:Say(SPEECH.JARVI.ENEMIES.GIANT)
                numMons = numMons + 1
            elseif v:HasTag("monster") or v:HasTag("hostile") or v:HasTag("spider") or v:HasTag("tentacle") then
                numMons = numMons + 1   
            end
        end
        if numMons < 4 and numMons > 0 then
            inst.components.talker:Say(SPEECH.JARVI.ENEMIES.SMALL)
        elseif numMons > 3 and numMons < 8 then
            inst.components.talker:Say(SPEECH.JARVI.ENEMIES.HUGE)
        end
    end
end


local function fn()

    local sounds =
    {
        attack = "dontstarve/sanity/creature2/attack",
        attack_grunt = "dontstarve/sanity/creature2/attack_grunt",
        death = "dontstarve/sanity/creature2/die",
        idle = "dontstarve/sanity/creature2/idle",
        taunt = "dontstarve/sanity/creature2/taunt",
        appear = "dontstarve/sanity/creature2/appear",
        disappear = "dontstarve/sanity/creature2/dissappear",
    }

    local inst = summon:create_summon(TUNING.JAO.SPEECH.JARVI, true)
    MakeCharacterPhysics(inst, 30, .3)
    inst.DynamicShadow:SetSize(2, 1.5)

    summon:removeIfAlreadyExist("summonjill")

    inst.AnimState:SetBank("shadowcreature2")
    inst.AnimState:SetBuild("shadow_insanity2_basic")
    inst.AnimState:PlayAnimation("idle_loop", true)

    summon:addTag(inst, "summonjarvi")

    inst.entity:SetPristine()

    summon:canTalk(inst)

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aurafn = CalcSanityAura

    summon:canFollow(inst, true)
    summon:hasInventory(inst)
    summon:hasHealth(inst, 100)
    inst.components.health:SetInvincible(true)
    summon:canWalk(inst, 40, 40)
    summon:setScale(inst, -((TUNING.ROCKY_MIN_SCALE)/3 * 2))
    summon:canPoop(inst, "nightmarefuel")
    summon:addLeader(inst)
    inst.sounds = sounds

    inst:DoPeriodicTask(3, function() getPetStatus(inst) end)
    inst:DoPeriodicTask(7, function() getMasterStatus(inst) end)
    inst:DoPeriodicTask(2, function() prepareEnemies(inst) end)

    inst:SetBrain(brain)
    inst:SetStateGraph("SGshadowcreature")

    return inst
end

return Prefab("common/summons/summonjarvi", fn, assets, prefabs)