-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		     --
-- Any unauthorized use will be reported to the DMCA. 				             --
-- To use any file or sprite ask my permission.					                 --
--										                                         --
-- Author: Paulo Victor de Oliveira Leal					                     --
-- Contact: ciclopiano@gmail.com						                         --

local assets = {
    Asset("ANIM", "anim/beefalo_basic.zip"),
    Asset("ANIM", "anim/beefalo_actions.zip"),
    Asset("ANIM", "anim/beefalo_actions_domestic.zip"),
    Asset("ANIM", "anim/beefalo_actions_quirky.zip"),
    Asset("ANIM", "anim/beefalo_build.zip"),
    Asset("ANIM", "anim/beefalo_shaved_build.zip"),
    Asset("ANIM", "anim/beefalo_baby_build.zip"),
    ------------------------------------------
    Asset("ANIM", "anim/beefalo_domesticated.zip"),
    Asset("ANIM", "anim/beefalo_personality_docile.zip"),
    Asset("ANIM", "anim/beefalo_personality_ornery.zip"),
    Asset("ANIM", "anim/beefalo_personality_pudgy.zip"),
    ------------------------------------------
    Asset("ANIM", "anim/beefalo_fx.zip"),
    ------------------------------------------
    Asset("SOUND", "sound/beefalo.fsb"),
    ------------------------------------------
    Asset("ATLAS", "images/map_icons/summons/jill.xml"),
    Asset("ATLAS", "images/map_icons/summons/jill.xml"),
    
}

------------------------------------------
-- Prefabs Necessarios
------------------------------------------
local prefabs = {
    "jao",
    "meat",
    "poop",
    "beefalowool",
    "horn",
}


local brain = require("brains/summonjillbrain")
local summon = require "base/summon"

local sounds = {
    walk = "dontstarve/beefalo/walk",
    grunt = "dontstarve/beefalo/grunt",
    yell = "dontstarve/beefalo/yell",
    swish = "dontstarve/beefalo/tail_swish",
    curious = "dontstarve/beefalo/curious",
    angry = "dontstarve/beefalo/angry",
}

local SPEECH = TUNING.JAO.SPEECH

local function ClearBuildOverrides(inst, animstate)
    if animstate ~= inst.AnimState then
        animstate:ClearOverrideBuild("beefalo_build")
    end
    animstate:ClearOverrideBuild("beefalo_personality_docile")
    inst.AnimState:Hide("HEAT")
end

local function ApplyBuildOverrides(inst, animstate)
    local basebuild = "beefalo_domesticated"
    if animstate ~= nil and animstate ~= inst.AnimState then
        animstate:AddOverrideBuild(basebuild)
    else
        animstate:SetBuild(basebuild)
    end
    inst.AnimState:Hide("HEAT")
end



local function beefalo()
    local inst = summon:create_summon()
    MakeCharacterPhysics(inst, 100, .5)
    inst.DynamicShadow:SetSize(6, 2)
    inst.Transform:SetSixFaced()

    summon:removeIfAlreadyExist("summonjill")

    inst.AnimState:SetBank("beefalo")
    inst.AnimState:SetBuild("beefalo_build")
    inst.AnimState:PlayAnimation("idle_loop", true)
    inst.AnimState:Hide("HEAT")
    inst.sounds = sounds

    summon:addToMinimap(inst, "jill.tex")

    summon:addTag(inst, "summonjill")

    inst:AddTag("beefalo")
    inst:AddTag("animal")
    inst:AddTag("largecreature")
    inst:AddTag("bearded")
  --  inst:AddTag("herdmember")
    inst:AddTag("saddleable")
    inst:AddTag("rideable")
    inst:AddTag("saddled")
    inst:AddTag("companion")
  --  inst:AddTag("domesticatable")

    summon:canTalk(inst)

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    summon:canBeBrushed(inst)
    summon:canEat(inst, { FOODTYPE.VEGGIE, FOODTYPE.ROUGHAGE })
    summon:canFight(inst, 0, 10, false)
    summon:canBeHurt(inst)
    summon:hasHealth(inst, 100)
    summon:hasInventory(inst)
    summon:canWalk(inst, 6, 9)
    summon:canPoop(inst, "poop")
    summon:canFollow(inst, false)
    inst.components.follower.canaccepttarget = false
    summon:canBeRided(inst)
    summon:canTrade(inst)
    summon:addLeader(inst)
    summon:hasContainer(inst)
    summon:canBeBurned(inst, "beefalo_body")
    summon:canBeFreezed(inst, "beefalo_body")
    inst:AddComponent("hunger")

    inst.ApplyBuildOverrides = ApplyBuildOverrides
    inst.ClearBuildOverrides = ClearBuildOverrides

    inst:SetBrain(brain)
    inst:SetStateGraph("SGsummonjill")

    inst.OnSave = function (inst, data) end
    inst.OnLoad = function (inst, data) end

    inst:ListenForEvent("death", function (inst, data)
        inst.components.follower.leader.jillInvocado = false
        inst.components.container:DropEverything()
        if inst.components.rideable:IsBeingRidden() then
            inst.components.rideable:Buck(true)
        end

    end)

    return inst
end

return Prefab("summons/summonjill", beefalo, assets, prefabs)
