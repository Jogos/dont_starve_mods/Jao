
Summon = {}

local sanityValue = 0;

local function CalcSanityAura(inst, observer)
    return inst.components.combat.target ~= nil 
    and (TUNING.SANITYAURA_LARGE) + sanityValue
    or 1
end

local function NormalRetargetFn(inst)
    return FindEntity(inst, TUNING.PIG_TARGET_DIST, function(guy)
        return guy:HasTag("monster") and not guy:HasTag("summonedbyplayer") and guy.components.health and not guy.components.health:IsDead() and (not guy:HasTag("summonedbyplayer")) and (not guy:HasTag("jaobuilder"))
        and inst.components.combat:CanTarget(guy)
    end, nil, { "character" }, nil)
end

local function OnEat( inst, food )
    local health = inst.components.health:GetPercent()
    local currentHealth = inst.components.health.currenthealth
    if health >= 1 then
        inst.components.talker:Say(inst.speechLines.EAT.FULL)
    else
        inst.components.health:DoDelta(50)
        inst.components.talker:Say(inst.speechLines.EAT.EMPTY)
        inst.sg:GoToState("eat")    
    end
end

local function ShouldAcceptItem(inst, item)
    return inst.components.eater:CanEat(item)
end

local function OnGetItemFromPlayer(inst, giver, item)
    if inst.components.eater:CanEat(item) then
        inst.components.eater:Eat(item)
        inst.sg:GoToState("idle_tendril")
        inst.SoundEmitter:PlaySound("dontstarve/creatures/rocklobster/foley") 
    else
        inst.components.inventory:DropItem(item)        
    end
end

local function OnRefuseItem(inst, item)
    inst.sg:GoToState("taunt")
    inst.components.talker:Say(inst.speechLines.REFUSE)
    inst.components.inventory:DropItem(item)
end

local function OnAttacked(inst, data)
    local attacker = data.attacker  
    if not attacker:HasTag("summonedbyplayer") and not attacker:HasTag("jaobuilder") and not attacker:HasTag("player") then
        inst.components.combat:SetTarget(attacker)
        inst.components.combat:ShareTarget(attacker, 30, function(dude)
            return dude:HasTag("summonedbyplayer") and 
                dude.components.follower.leader == inst.components.follower.leader 
            end, 5)
    end
end

local function OnAttackOther(inst, data)
    local target = data.target
    if not target:HasTag("summonedbyplayer") and not target:HasTag("jaobuilder") and not target:HasTag("player") then
        inst.components.talker:Say(inst.speechLines.ATTACK)
        inst.components.combat:ShareTarget(target, 30, function(dude)
            return dude:HasTag("summonedbyplayer") and 
                dude.components.follower.leader == inst.components.follower.leader
            end, 5)  
    end
end


local function linkToBuilder(inst, builder)
    if not builder.components.leader then
        builder:AddComponent("leader")
    end
    builder.components.leader:AddFollower(inst, true)
    
    builder.components.sanity:DoDelta(-30)
    
    builder.components.talker:Say(inst.speechLines.SUMMON)
    
    if builder.components.combat.hurtsound ~= nil and builder.SoundEmitter ~= nil then
        builder.SoundEmitter:PlaySound(builder.components.combat.hurtsound)
    end
    
    builder:PushEvent("damaged", {})
    
    local x, y, z =  builder.Transform:GetWorldPosition()
    
    local tile = TheWorld.Map:GetTileAtPoint(x+5, y, z+5)
    local point = Point (x+5, y, z+5)
    local canspawn = tile ~= GROUND.IMPASSABLE and tile ~= GROUND.INVALID
    if not canspawn then point = Point(x, y, z) end
    inst.Transform:SetPosition( (point):Get() )
    
    local fx = SpawnPrefab("statue_transition")
    fx.Transform:SetScale(1, 1, 1)
    fx.Transform:SetPosition(builder:GetPosition():Get())
    inst.SoundEmitter:PlaySound("dontstarve/sanity/creature2/taunt")
    
    local xf = SpawnPrefab("statue_transition_2")
    xf.Transform:SetScale(3, 3, 3)
    xf.Transform:SetPosition(inst:GetPosition():Get())
    inst.SoundEmitter:PlaySound("dontstarve/common/deathpoof")
    
end

function Summon.removeIfAlreadyExist(self, tag)
    local older = TheSim:FindFirstEntityWithTag(tag)
    if(older ~= nil) then
        older:Remove()
    end
end

function Summon.addTag (self, inst, summonTag)
    inst:AddTag(summonTag)
    inst:AddTag("sheltercarrier")
    inst:AddTag("summonedbyplayer")
    inst:AddTag("scarytoprey")
end

function Summon.setScale(self, inst, value )
    if(inst.components.scaler == nil) then
        inst:AddComponent("scaler")
    end
    inst.components.scaler:SetScale(TUNING.ROCKY_MAX_SCALE + value)
end

function Summon.canTalk(self, inst )
    inst:AddComponent("talker")
    inst.components.talker.ontalk = function (inst, script)
        inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
    end
    inst.components.talker.fontsize = 35
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0, -400, 0)
end

function Summon.canFight(self, inst, damage, period, area)
    if(inst.components.combat == nil) then
        inst:AddComponent("combat")
    end
    inst.components.combat:SetDefaultDamage(damage)
    inst.components.combat:SetAttackPeriod(period)
    if(area) then
        inst.components.combat:SetRange(3, 4)
    end
    inst.components.combat:SetRetargetFunction(3, NormalRetargetFn)
    inst:ListenForEvent("attacked", OnAttacked) 
end

function Summon.canGroundPound( self, inst )
    inst:AddComponent("tibbercracker")
    
    inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = true
    inst.components.groundpounder.damageRings = 2
    inst.components.groundpounder.destructionRings = 2
    inst.components.groundpounder.numRings = 3
    inst.cangroundpound = false
    
    inst:AddComponent("timer")
    
    inst:ListenForEvent("timerdone", function (inst, data)
        if data.name == "GroundPound" then
            inst.cangroundpound = true
        end
    end)
    
    inst.OnSave = function (inst, data)
        data.cangroundpound = inst.cangroundpound
    end
   
    inst.OnLoad = function ( inst, data )
        if data ~= nil then
            inst.cangroundpound = data.cangroundpound
        end
    end
end

function Summon.canWalk(self, inst, walkSpeed, runSpeed )
    if(inst.components.locomotor == nil) then
        inst:AddComponent("locomotor")
    end
    inst.components.locomotor.runspeed = runSpeed
    inst.components.locomotor.walkspeed = walkSpeed
end

function Summon.canBeBurned(self, inst, region )
    MakeMediumBurnableCharacter(inst, region)
end

function Summon.canBeFreezed(self, inst, region )
    MakeHugeFreezableCharacter(inst, region)
end

function Summon.canBeHurt(self, inst )
    local actual = inst.components.combat
    local old = actual.GetAttacked
    function actual:GetAttacked(attacker, damage, weapon, stimuli)
                if attacker and ((attacker:HasTag("player") and not attacker:HasTag("jaobuilder"))  or  attacker:HasTag("summonedbyplayer")) then
            return true
        end
        if(inst.components.rideable ~= nil) then
            inst.components.rideable.saddle:Remove()
        end
        return old(actual, attacker, damage, weapon, stimuli)
    end
end

function Summon.canFollow(self, inst, sit )
    inst:AddComponent("follower")
    if(sit) then
        inst:AddComponent("followersitcommand")
    end
end

function Summon.canEat( self, inst, foodtype)
    inst:AddComponent("eater")
    inst.components.eater:SetDiet(foodtype, foodtype)
    inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetCanEatRaw()
    inst.components.eater.strongstomach = true 
    inst.components.eater:SetOnEatFn(OnEat)
end

function Summon.canPoop( self, inst, poopType )
    inst:AddComponent("periodicspawner")
    inst.components.periodicspawner:SetPrefab(poopType)
    inst.components.periodicspawner:SetRandomTimes(40, 60)
    inst.components.periodicspawner:SetDensityInRange(20, 2)
    inst.components.periodicspawner:SetMinimumSpacing(8)
    inst.components.periodicspawner:Start()
end

function Summon.canTrade( self, inst )
    inst:AddComponent("trader")
    inst.components.trader:SetAcceptTest(ShouldAcceptItem)
    inst.components.trader.deleteitemonaccept = true
    inst.components.trader.onaccept = OnGetItemFromPlayer
    inst.components.trader.onrefuse = OnRefuseItem
    inst.components.trader:Enable()
end

function Summon.canSleep( self, inst )
    inst:AddComponent("sleeper")
    inst.components.sleeper:SetResistance(3)
    inst.components.sleeper.sleeptestfn = DefaultSleepTest
    inst.components.sleeper.wakeuptestfn = DefaultWakeTest
end

function Summon.canBeBrushed(self, inst)
    inst:AddComponent("brushable")
    inst.components.brushable.regrowthdays = 1
    inst.components.brushable.max = 2
    inst.components.brushable.prize = "beefalowool"
    inst:ListenForEvent("brushed", function (inst, data)
        if data.numprizes > 0 then
            SpawnPrefab("beefalowool") 
        end
    end)

    inst:AddComponent("beard")
    inst.components.beard.bits = 3
    inst.components.beard.daysgrowth = TUNING.BEEFALO_HAIR_GROWTH_DAYS + 1
    inst.components.beard.prize = "beefalowool"
    inst.components.beard.onreset = function (inst) end
    inst.components.beard.canshavetest =  function (inst) return false end
end

function Summon.canBeRided( self, inst)
    inst:AddComponent("rideable")
    inst.components.rideable:SetRequiredObedience(0)
    inst.components.rideable.canride = true
    inst.components.rideable:SetSaddleable(true)
        
    inst:DoPeriodicTask(0, function() 
        if not inst.components.rideable:IsBeingRidden() then
            if inst.components.rideable.saddle ~= nil then
                inst.components.rideable.saddle:Remove()
            end
            inst.components.rideable:SetSaddle(nil, SpawnPrefab("saddle_race"))
        end
    end)
end

function Summon.hasHealth(self, inst, value )
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(value)
    inst.components.health.fire_damage_scale = 0
end

function Summon.hasInventory(self, inst )
    inst:AddComponent("inspectable")
    inst:AddComponent("inventory")
    inst:AddComponent("lootdropper")
end

function Summon.hasContainer(self, inst)
    inst:AddComponent("container")
    inst:AddTag("fridge")
    inst.components.container:WidgetSetup("shadowchester")
    inst.components.container.onopenfn = function (inst) end
    inst.components.container.onclosefn = function (inst) end
end

function Summon.affectsAura(self, inst, value )
    inst:AddComponent("sanityaura")
    sanityValue = value
    inst.components.sanityaura.aurafn = CalcSanityAura
end

function Summon.addToMinimap(self, inst, icon )
    inst.MiniMapEntity:SetIcon(icon)
    inst.MiniMapEntity:SetPriority(4)
end

function Summon.addLeader( self, inst )
    inst.OnBuilt = linkToBuilder
end

function Summon.create_summon( speech, face )
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    inst.speechLines = speech
    
    if(face) then
        inst.Transform:SetFourFaced()
    end

    inst:AddComponent("perishable")
    inst.components.perishable.onperishreplacement = "sourceofmagic"
    
    return inst;
end

return Summon