-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		     --
-- Any unauthorized use will be reported to the DMCA. 				             --
-- To use any file or sprite ask my permission.					                 --
--										                                         --
-- Author: Paulo Victor de Oliveira Leal					                     --
-- Contact: ciclopiano@gmail.com						                         --
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

------------------------------------------
-- Esta funcao de biblioteca nos permite usar um arquivo em um local especificado. 
-- Permite usar para chamar variaveis globais de ambiente sem inicializa-los em nossos arquivos.
------------------------------------------
modimport("libs/env.lua")
modimport("libs/engine.lua")

------------------------------------------
-- Scripts a carregar
------------------------------------------
Load "chatinputscreen"
Load "consolescreen"
Load "textedit"

------------------------------------------
-- Acao de inicializacao.
------------------------------------------
use "data/actions/init"

------------------------------------------
-- Componente de inicializacao.
------------------------------------------
use "data/components/init"

------------------------------------------
-- Scripts necessarios
------------------------------------------
PrefabFiles = {
    "jao", "jaostaff",        
    "summons/summonskip",     
    "summons/summonrhino",    
    "summons/summonchop",     
    "summons/summonjill",    
    "summons/summonaron",    
    "summons/summonminichop",
    "summons/summonjarvi",
    "sourceofmagic",
}

------------------------------------------
-- Arquivos de importacao de imagens e animacao
------------------------------------------
Assets = {
    Asset( "IMAGE", "images/saveslot_portraits/jao.tex" ),
    Asset( "ATLAS", "images/saveslot_portraits/jao.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/selectscreen_portraits/jao.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/jao.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/selectscreen_portraits/jao_silho.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/jao_silho.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "bigportraits/jao.tex" ),
    Asset( "ATLAS", "bigportraits/jao.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/map_icons/jao.tex" ),
    Asset( "ATLAS", "images/map_icons/jao.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/avatars/avatar_jao.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_jao.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/avatars/avatar_ghost_jao.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_jao.xml" ),
    ------------------------------------------
    Asset("ATLAS", "images/hud/magictab.xml"),
    Asset("IMAGE", "images/hud/magictab.tex"),
    ------------------------------------------
    Asset( "IMAGE", "images/inventoryimages/sourceofmagic.tex" ),
    Asset( "ATLAS", "images/inventoryimages/sourceofmagic.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/inventoryimages/summons/summonaron.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summons/summonaron.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/inventoryimages/summons/summonjill.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summons/summonjill.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/inventoryimages/summons/summonchop.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summons/summonchop.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/inventoryimages/summons/summonrhino.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summons/summonrhino.xml" ),
    ------------------------------------------
    Asset( "IMAGE", "images/inventoryimages/summons/summonskip.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summons/summonskip.xml" ),
    ------------------------------------------
}

------------------------------------------
-- Variaveis globais
------------------------------------------
local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS
------------------------------------------
local SITCOMMAND = GLOBAL.Action(4, true, true, 10,	false, false, nil)
local SITCOMMAND_CANCEL = GLOBAL.Action(4, true, true, 10, false, false, nil)
------------------------------------------
local MARCAR = GLOBAL.Action(4, true, true, 10,	false, false, nil)
local HUNT = GLOBAL.Action(4, true, true, 10,	false, false, nil)
------------------------------------------
local petSpeech = require "speech_pets"
local SPEECHBR = petSpeech.SPEECH_PETS.PORTUGUES
local SPEECHEN = petSpeech.SPEECH_PETS.ENGLISH

------------------------------------------
-- Variaveis Recebidas do ModInfo
------------------------------------------
GLOBAL.TUNING.JAO = {}
GLOBAL.TUNING.JAO.LANG  = GetModConfigData("lang")
GLOBAL.TUNING.JAO.KEYF1 = GetModConfigData("keyf1") or 282
GLOBAL.TUNING.JAO.KEYF2 = GetModConfigData("keyf2") or 283
GLOBAL.TUNING.JAO.KEYF3 = GetModConfigData("keyf3") or 284
GLOBAL.TUNING.JAO.KEYF4 = GetModConfigData("keyf4") or 285
GLOBAL.TUNING.JAO.KEYF5 = GetModConfigData("keyf5") or 286
GLOBAL.TUNING.JAO.KEYZ  = GetModConfigData("keyz")  or 122
GLOBAL.TUNING.JAO.KEYX  = GetModConfigData("keyx")  or 120
GLOBAL.TUNING.JAO.KEYC  = GetModConfigData("keyc")  or 99
GLOBAL.TUNING.JAO.KEYV  = GetModConfigData("keyv")  or 118
GLOBAL.TUNING.JAO.KEYB  = GetModConfigData("keyb")  or 98
 
 GLOBAL.TUNING.JAO.GROUNDTEST1 = GLOBAL.GROUND.IMPASSABLE
 GLOBAL.TUNING.JAO.GROUNDTEST2 = GLOBAL.GROUND.INVALID;
 
------------------------------------------
-- Recuperar Linguagem Definida Pelo Usario
------------------------------------------
local function definirLinguagem( )
    local speech_choosed = TUNING.JAO.LANG
    if speech_choosed == 0 then
        return SPEECHEN
    else
        return SPEECHBR
    end
    return SPEECHEN
end


------------------------------------------
-- Definir Linguagem Padrao
------------------------------------------
local SPEECH = definirLinguagem( )
GLOBAL.TUNING.JAO.SPEECH  = SPEECH

------------------------------------------
-- Resposta do Skip
------------------------------------------
local function saySkipResp(inst)
    local summon = TheSim:FindFirstEntityWithTag("summonskip")
    if summon ~= nil then
        local health = summon.components.health:GetPercent()
        if health == 1 then
            summon.components.talker:Say(SPEECH.SKIP.STATUS.FULL)
        elseif health > 0.5 then
            summon.components.talker:Say(SPEECH.SKIP.STATUS.HALF)
        else
            summon.components.talker:Say(SPEECH.SKIP.STATUS.EMPTY)
        end            
    end
end

------------------------------------------
-- Esperando pelo clique de F1
------------------------------------------
AddModRPCHandler("jao", "infoSkip", saySkipResp )

------------------------------------------
-- Trocar se o Skip pode pegar coisas no chao ou nao
------------------------------------------
local function actionSkip(inst)   
    local summon = TheSim:FindFirstEntityWithTag("summonskip")
    if summon ~= nil then
        summon.components.talker:Say(SPEECH.SKIP.ACTION.PICK)
        summon.pick = not (summon.pick)           
    end
end

------------------------------------------
-- Esperando pelo clique de Z
------------------------------------------
AddModRPCHandler("jao", "actionSkip", actionSkip )

------------------------------------------
-- Trocar se o Skip pode jogar coisas no chao
------------------------------------------
local function dropSkip(inst)
    local summon = TheSim:FindFirstEntityWithTag("summonskip")
    if summon ~= nil then
        if summon.components.inventory:NumItems( ) > 0 then
            summon.components.talker:Say(SPEECH.SKIP.ACTION.DROP)
            summon.components.inventory:DropEverything(true)      
        else
            summon.components.talker:Say(SPEECH.SKIP.ACTION.FAIL)       
        end
    end     
end

------------------------------------------
-- Esperando pelo clique de X
------------------------------------------
AddModRPCHandler("jao", "actionDrop", dropSkip )

------------------------------------------
-- Falas Do Rhino
------------------------------------------
local function sayRhinoInfo( inst )
    local summon = TheSim:FindFirstEntityWithTag("summonrhino")
    if summon ~= nil then
        local health = summon.components.health:GetPercent()
        if health == 1 then
            summon.components.talker:Say(SPEECH.RHINO.STATUS.FULL)
        elseif health > 0.5 then
            summon.components.talker:Say(SPEECH.RHINO.STATUS.HALF)
        else    
            summon.components.talker:Say(SPEECH.RHINO.STATUS.EMPTY)       
        end
    end            
end

------------------------------------------
-- Esperando pelo clique de F1
------------------------------------------
AddModRPCHandler("jao", "infoRhino", sayRhinoInfo )

------------------------------------------
-- Definir Alvo Para o Rhino
------------------------------------------
local function alvoRhino(inst)   
    local x,y,z = inst.Transform:GetWorldPosition()
    local alvos = TheSim:FindEntities(x,y,z, 3)
    local summon = TheSim:FindFirstEntityWithTag("summonrhino")
    if summon ~= nil then
        for t,alvo in pairs(alvos) do
            if alvo:HasTag("workable") or alvo:HasTag("boulder") or 
                alvo:HasTag("tree") or alvo:HasTag("structure") or ("wall") then
                summon.target = Point(alvo.Transform:GetWorldPosition())
                summon.targetEntity = alvo
                break
            end
        end
        
        if not summon.destroy and summon.target ~= nil then     
            summon.destroy = true
            summon.components.talker:Say(SPEECH.RHINO.ACTION.SMASH)
        else        
            summon.components.talker:Say(SPEECH.RHINO.ACTION.FAIL)
        end
    end     
end

------------------------------------------
-- Esperando pelo clique de C
------------------------------------------
AddModRPCHandler("jao", "actionDestroy", alvoRhino )

------------------------------------------
-- Falas Do Chop
------------------------------------------
local function sayChopInfo( inst )
    local summon = TheSim:FindFirstEntityWithTag("summonchop")
    if summon ~= nil then
        local health = summon.components.health:GetPercent()
        if health == 1 then
            summon.components.talker:Say(SPEECH.CHOP.STATUS.FULL)
        elseif health > 0.5 then
            summon.components.talker:Say(SPEECH.CHOP.STATUS.HALF)
        else    
            summon.components.talker:Say(SPEECH.CHOP.STATUS.EMPTY)       
        end
    end         
end

------------------------------------------
-- Esperando pelo clique de F1
------------------------------------------
AddModRPCHandler("jao", "infoChop", sayChopInfo )


local function removeOldMiniChops(inst)
    local x,y,z = inst.Transform:GetWorldPosition()
    local chopinhos = TheSim:FindEntities(x,y,z, 25, {"summonminichop"})
    for n,minichop in pairs(chopinhos) do
        minichop:Remove()
    end
end

local function spawnMiniChop (inst, x, z)
    local base_x, base_y, base_z = inst.Transform:GetWorldPosition()
    local guard = SpawnPrefab("summonminichop")

    guard.Transform:SetPosition( (Point(base_x+x, base_y, base_z+z)):Get() )
    guard.components.knownlocations:ForgetLocation("torre")
    guard.components.knownlocations:RememberLocation("torre", (Point(base_x+(x*2), base_y, base_z+(z*2))), true)

    local fx = SpawnPrefab("green_leaves")
    fx.Transform:SetScale(3, 2, 3)
    fx.Transform:SetPosition(guard:GetPosition():Get())
end

local function protectPerimeter(inst)
    local summon = TheSim:FindFirstEntityWithTag("summonchop")
    if summon ~= nil then
        removeOldMiniChops(inst)
        if not summon.protegendo then
            summon.components.followersitcommand:SetStaying(true)
            summon.components.followersitcommand:RememberSitPos("currentstaylocation", GLOBAL.Point(summon.Transform:GetWorldPosition()))
            summon.components.talker:Say(SPEECH.CHOP.ACTION.PROTECT)

            spawnMiniChop(summon,  5,  5)
            spawnMiniChop(summon, -5,  5)
            spawnMiniChop(summon, -5, -5)
            spawnMiniChop(summon,  5, -5)
        else
            summon.components.talker:Say(SPEECH.CHOP.ACTION.FAIL)
            summon.components.followersitcommand:SetStaying(false)
        end
        summon.protegendo = not summon.protegendo
    end
end

------------------------------------------
-- Esperando pelo clique de C
------------------------------------------
AddModRPCHandler("jao", "actionProtect", protectPerimeter )

------------------------------------------
-- Respostas Jill
------------------------------------------
local function sayJillInfo( inst )
    local summon = TheSim:FindFirstEntityWithTag("summonjill")
    if summon ~= nil then
        local health = summon.components.health:GetPercent()
        if health == 1 then
            summon.components.talker:Say(SPEECH.JILL.STATUS.FULL)
        elseif health > 0.5  then
            summon.components.talker:Say(SPEECH.JILL.STATUS.HALF)    
        else
            summon.components.talker:Say(SPEECH.JILL.STATUS.EMPTY)       
        end
    end              
end

------------------------------------------
-- Esperando pelo clique de F4
------------------------------------------
AddModRPCHandler("jao", "infoJill", sayJillInfo )

------------------------------------------
-- Resposta do Aron
------------------------------------------
local function sayAronInfo( inst )
    local summon = TheSim:FindFirstEntityWithTag("summonaron")
    if summon ~= nil then
        local health = summon.components.health:GetPercent()
        local isWere = summon:HasTag("werearon")
        if health == 1 and isWere then
            summon.components.talker:Say(SPEECH.WEREARON.STATUS.FULL)
        elseif health == 1 and isWere == false then
            summon.components.talker:Say(SPEECH.ARON.STATUS.FULL)
        elseif health > 0.5 and isWere then
            summon.components.talker:Say(SPEECH.ARON.STATUS.HALF)
        elseif health > 0.5 and isWere == false then
            summon.components.talker:Say(SPEECH.ARON.STATUS.HALF)
        else
            summon.components.talker:Say(SPEECH.ARON.STATUS.EMPTY)
        end
    end
end

------------------------------------------
-- Esperando pelo clique de F5
------------------------------------------
AddModRPCHandler("jao", "infoAron", sayAronInfo )

------------------------------------------
-- Dizer Para o Aron Ativar o Modo Cacador ou Nao
------------------------------------------
local function transformAron(inst)
    local summon = TheSim:FindFirstEntityWithTag("summonaron")
    if summon ~= nil then
        if summon.transformed then
            summon.hunterMode = false
            summon:morph()
        else
            summon.hunterMode = true
            summon:morph()
        end
    end
end

------------------------------------------
-- Esperando pelo clique de B
------------------------------------------
AddModRPCHandler("jao", "actionTransform", transformAron )

------------------------------------------
-- RECEITAS ------------------------------
------------------------------------------

------------------------------------------
-- Codigo da aba de receitas
------------------------------------------
local recipe_tab = AddRecipeTab("Invocations and Spells", 999, "images/hud/magictab.xml", "magictab.tex", "jaobuilder" )

------------------------------------------
-- Receita da gema roxa
------------------------------------------
AddRecipe("purplegem", {Ingredient("goldnugget",5), Ingredient("tentaclespots", 3)}, recipe_tab, TECH.NONE, nil, nil, nil, 3)

------------------------------------------
-- Receita da gema 
------------------------------------------
AddRecipe("redgem", {Ingredient("goldnugget",5), Ingredient("mosquitosack", 3)}, recipe_tab, TECH.NONE, nil, nil, nil, 3)

------------------------------------------
-- Receita da gema 
------------------------------------------
AddRecipe("bluegem", {Ingredient("goldnugget",5), Ingredient("ice", 3)}, recipe_tab, TECH.NONE, nil, nil, nil, 3)

------------------------------------------
-- Receitas do livro - Passaros
------------------------------------------
AddRecipe("book_birds", {Ingredient("papyrus", 2), Ingredient("bird_egg", 2)}, recipe_tab, TECH.NONE)

------------------------------------------
-- Receitas do livro - Jardim
------------------------------------------
AddRecipe("book_gardening", {Ingredient("papyrus", 2), Ingredient("seeds", 1), Ingredient("poop", 1)}, recipe_tab, TECH.NONE)

------------------------------------------
-- Receitas do livro - Dormir
------------------------------------------
AddRecipe("book_sleep", {Ingredient("papyrus", 2), Ingredient("nightmarefuel", 2)}, recipe_tab, TECH.NONE)

------------------------------------------
-- Receitas do livro - Fogo
------------------------------------------
AddRecipe("book_brimstone", {Ingredient("papyrus", 2), Ingredient("redgem", 1)}, recipe_tab, TECH.NONE)

------------------------------------------
-- Receitas do livro - Tentaculos
------------------------------------------
AddRecipe("book_tentacles", {Ingredient("papyrus", 2), Ingredient("tentaclespots", 1)}, recipe_tab, TECH.NONE)							

------------------------------------------
-- Receita para invoca o Aron
------------------------------------------
local summonaron_recipe = AddRecipe("summonaron", {Ingredient(CHARACTER_INGREDIENT.HEALTH, 20)}, recipe_tab, TECH.NONE, nil, nil, nil, nil, nil,
"images/inventoryimages/summons/summonaron.xml", "summonaron.tex")
summonaron_recipe.tagneeded = false
summonaron_recipe.builder_tag ="jaobuilder"
summonaron_recipe.atlas = resolvefilepath("images/inventoryimages/summons/summonaron.xml")

------------------------------------------
-- Receita para invoca o Jill
------------------------------------------
local summonjill_recipe = AddRecipe("summonjill", {Ingredient(CHARACTER_INGREDIENT.HEALTH, 20)}, recipe_tab, TECH.NONE, nil, nil, nil, nil, nil,
"images/inventoryimages/summons/summonjill.xml", "summonjill.tex")
summonjill_recipe.tagneeded = false
summonjill_recipe.builder_tag ="jaobuilder"
summonjill_recipe.atlas = resolvefilepath("images/inventoryimages/summons/summonjill.xml")

------------------------------------------
-- Receita para invoca o Chop
------------------------------------------
local summonchop_recipe = AddRecipe("summonchop", {Ingredient(CHARACTER_INGREDIENT.HEALTH, 20)}, recipe_tab, TECH.NONE, nil, nil, nil, nil, nil,
"images/inventoryimages/summons/summonchop.xml", "summonchop.tex")
summonchop_recipe.tagneeded = false
summonchop_recipe.builder_tag ="jaobuilder"
summonchop_recipe.atlas = resolvefilepath("images/inventoryimages/summons/summonchop.xml")

------------------------------------------
-- Receita para invoca o Skip
------------------------------------------
local summonskip_recipe = AddRecipe("summonskip", {Ingredient(CHARACTER_INGREDIENT.HEALTH, 20)}, recipe_tab, TECH.NONE, nil, nil, nil, nil, nil,
"images/inventoryimages/summons/summonskip.xml", "summonskip.tex")
summonskip_recipe.tagneeded = false
summonskip_recipe.builder_tag ="jaobuilder"
summonskip_recipe.atlas = resolvefilepath("images/inventoryimages/summons/summonskip.xml")

------------------------------------------
-- Receita para invocar o Rhino
------------------------------------------
local summonrhino_recipe = AddRecipe("summonrhino", {Ingredient(CHARACTER_INGREDIENT.HEALTH, 20)}, recipe_tab, TECH.NONE, nil, nil, nil, nil, nil,
"images/inventoryimages/summons/summonrhino.xml", "summonrhino.tex")
summonrhino_recipe.tagneeded = false
summonrhino_recipe.builder_tag = "jaobuilder"
summonrhino_recipe.atlas = resolvefilepath("images/inventoryimages/summons/summonrhino.xml")

------------------------------------------
-- Dados do persongem
------------------------------------------
STRINGS.CHARACTER_TITLES.jao = "The Great Summoner"
STRINGS.CHARACTER_NAMES.jao = "Jao"
STRINGS.CHARACTER_DESCRIPTIONS.jao = "*Teleport\n*Range Attack\n*Summons"
STRINGS.CHARACTER_QUOTES.jao = "\"Did you see any of my invocations?\""

------------------------------------------
-- Dados do cajado
------------------------------------------
GLOBAL.STRINGS.NAMES.JAOSTAFF = "Jao's Staff"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.JAOSTAFF = "Teleportation source, light and fire Jão"

------------------------------------------
-- Dados do item
------------------------------------------
GLOBAL.STRINGS.NAMES.SOURCEOFMAGIC = "The Source of Magic"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SOURCEOFMAGIC = "Grants power of invocation of Jao!"

------------------------------------------
-- Dados do Jarvi
------------------------------------------
GLOBAL.STRINGS.NAMES.SUMMONJARVI = "Jarvi"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONJARVI = "A shadow that always helps to John"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONJARVI = ""

------------------------------------------
-- Dados do MiniChops
------------------------------------------
GLOBAL.STRINGS.NAMES.SUMMONMINICHOP = "Chop's guard"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONMINICHOP = "A guard for base."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONMINICHOP = "I already feel safer!"

------------------------------------------
-- Dados do Aron
------------------------------------------
GLOBAL.STRINGS.NAMES.SUMMONARON = "Aron"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONARON = "A cute rabbit."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONARON = "Be useful or turn you into a hat!"

------------------------------------------
-- Dados do Jill
------------------------------------------
GLOBAL.STRINGS.NAMES.SUMMONJILL = "Jill"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONJILL = "High speed riding."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONJILL = "Their hair is beautifull today, Jill..."

------------------------------------------
-- Dados do Chop
------------------------------------------
GLOBAL.STRINGS.NAMES.SUMMONCHOP = "Chop"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONCHOP = "Great strength but very slowly."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONCHOP = "Even a stone is faster than that! Let's Chop accelerates!..."

------------------------------------------
-- Dados do Skip
------------------------------------------
GLOBAL.STRINGS.NAMES.SUMMONSKIP = "Skip"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONSKIP = "Great defense and aura of sanity."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONSKIP = "It's just a bunch of rocks..."

------------------------------------------
--Dados do Rhino
------------------------------------------
GLOBAL.STRINGS.NAMES.SUMMONRHINO = "Rhino"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONRHINO = "Very strong, but quickly loses control."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONRHINO = "He is smarter than a door?!"

------------------------------------------
-- Icones do mapa de cada objeto/summon/jao
------------------------------------------
AddMinimapAtlas("images/map_icons/jao.xml")
------------------------------------------
AddMinimapAtlas("images/map_icons/summons/chop.xml")
AddMinimapAtlas("images/map_icons/summons/rocky.xml")
AddMinimapAtlas("images/map_icons/summons/rhino.xml")
AddMinimapAtlas("images/map_icons/summons/jill.xml")
AddMinimapAtlas("images/map_icons/summons/aron.xml")
AddMinimapAtlas("images/map_icons/summons/shelly.xml")
------------------------------------------
AddMinimapAtlas("images/map_icons/sourceofmagic_atlas.xml")
------------------------------------------

------------------------------------------
-- Funcoes de comando dos pets
------------------------------------------
AddReplicableComponent("followersitcommand")

------------------------------------------
-- Esperar
------------------------------------------
SITCOMMAND.id = "SITCOMMAND"
SITCOMMAND.str = "Wait"
SITCOMMAND.fn = function(act)
    local targ = act.target
    if targ and targ.components.followersitcommand and (not targ:HasTag("summonminichop")) and act.doer:HasTag("jaobuilder") then
        act.doer.components.locomotor:Stop()
        if targ:HasTag("summonskip") then
            act.doer.components.talker:Say(SPEECH.JAO.WAIT.SKIP)
        elseif targ:HasTag("summonrhino") then
            act.doer.components.talker:Say(SPEECH.JAO.WAIT.RHINO)    
        elseif targ:HasTag("summonchop") then
            act.doer.components.talker:Say(SPEECH.JAO.WAIT.CHOP)
        elseif targ:HasTag("summonjill") then
            act.doer.components.talker:Say(SPEECH.JAO.WAIT.JILL)
        elseif targ:HasTag("summonaron") then
            act.doer.components.talker:Say(SPEECH.JAO.WAIT.ARON)        
        end
        targ.components.followersitcommand:SetStaying(true)
        targ.components.followersitcommand:RememberSitPos("currentstaylocation", GLOBAL.Point(targ.Transform:GetWorldPosition())) 
        return true
    end
end
AddAction(SITCOMMAND)

------------------------------------------
-- Chamar
------------------------------------------
SITCOMMAND_CANCEL.id = "SITCOMMAND_CANCEL"
SITCOMMAND_CANCEL.str = "Call"
SITCOMMAND_CANCEL.fn = function(act)
    local targ = act.target
    if targ and targ.components.followersitcommand and (not targ:HasTag("summonminichop")) and (not targ:HasTag("summonchop")) and act.doer:HasTag("jaobuilder") then
        act.doer.components.locomotor:Stop()
        if targ:HasTag("summonskip") then
            act.doer.components.talker:Say(SPEECH.JAO.CALL.SKIP)
        elseif targ:HasTag("summonrhino") then
            act.doer.components.talker:Say(SPEECH.JAO.CALL.RHINO)                
        elseif targ:HasTag("summonjill") then
            act.doer.components.talker:Say(SPEECH.JAO.CALL.JILL)
        elseif targ:HasTag("summonaron") then
            act.doer.components.talker:Say(SPEECH.JAO.CALL.ARON)        
        end
        targ.components.followersitcommand:SetStaying(false)
        return true
    elseif targ and targ.components.followersitcommand and targ:HasTag("summonchop") and act.doer:HasTag("jaobuilder")  then
        act.doer.components.locomotor:Stop()
        act.doer.components.talker:Say(SPEECH.JAO.CALL.CHOP)
        targ.components.followersitcommand:SetStaying(false)
        local x1,y1,z1 = targ.Transform:GetWorldPosition()
        local chopinhos = TheSim:FindEntities(x1,y1,z1, 25, {"summonminichop"})
        for n,minichop in pairs(chopinhos) do
            minichop.components.health:Kill()
        end
        return true
    end    
end
AddAction(SITCOMMAND_CANCEL)

------------------------------------------
-- Comando de Esperar/Seguir
------------------------------------------
AddComponentAction("SCENE", "followersitcommand", function(inst, doer, actions, rightclick)      
    if rightclick and inst.replica.followersitcommand then  
        if not inst.replica.followersitcommand:IsCurrentlyStaying() and doer:HasTag("jaobuilder") and (not inst:HasTag("summonminichop")) and (not inst:HasTag("summonjarvi"))then
            table.insert(actions, GLOBAL.ACTIONS.SITCOMMAND)
        elseif inst.replica.followersitcommand:IsCurrentlyStaying() and (not inst:HasTag("summonminichop")) and (not inst:HasTag("summonjarvi")) and doer:HasTag("jaobuilder") then
            table.insert(actions, GLOBAL.ACTIONS.SITCOMMAND_CANCEL)
        end
    end
end)

------------------------------------------
-- Arquivo de falas do personagem
------------------------------------------
STRINGS.CHARACTERS.JAO = require "speech_jao"

------------------------------------------
-- Nome no jogo
------------------------------------------
STRINGS.NAMES.JAO = "Jao"

------------------------------------------
-- Falas genericas
------------------------------------------
STRINGS.CHARACTERS.GENERIC.DESCRIBE.JAO = {
    GENERIC = "It's Jao!",
    ATTACKER = "This Jão seems wise....",
    MURDERER = "Murderer!",
    REVIVER = "Jão, friend of lost souls.",
    GHOST = "Jão could use a heart.",
}

------------------------------------------
-- Genero do personagem (male, female, or robot)
------------------------------------------
table.insert(GLOBAL.CHARACTER_GENDERS.MALE, "jao")

------------------------------------------
-- Inicio
------------------------------------------
AddModCharacter("jao")