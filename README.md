# Personagem Jão

**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2015<br />

### Objetivo
Implementar o personagem Jão, no qual ele é um invocador capaz de criar diversos pets que o ajudam a sobreviver

### Observação

IDE:  [Visual Studio Code](https://code.visualstudio.com/)<br />
Linguagem: [Lua](https://www.lua.org/)<br />
Banco de dados: Não utiliza<br />

### Contribuição

Esse projeto está concluído e é livre para consulta

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->