PrefabFiles = {
	"paulciclops",
}

Assets = {
    Asset( "IMAGE", "images/saveslot_portraits/paulciclops.tex" ),
    Asset( "ATLAS", "images/saveslot_portraits/paulciclops.xml" ),

    Asset( "IMAGE", "images/selectscreen_portraits/paulciclops.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/paulciclops.xml" ),
	
    Asset( "IMAGE", "images/selectscreen_portraits/paulciclops_silho.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/paulciclops_silho.xml" ),

    Asset( "IMAGE", "bigportraits/paulciclops.tex" ),
    Asset( "ATLAS", "bigportraits/paulciclops.xml" ),
	
	Asset( "IMAGE", "images/map_icons/paulciclops.tex" ),
	Asset( "ATLAS", "images/map_icons/paulciclops.xml" ),
	
	Asset( "IMAGE", "images/avatars/avatar_paulciclops.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_paulciclops.xml" ),
	
	Asset( "IMAGE", "images/avatars/avatar_ghost_paulciclops.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_paulciclops.xml" ),
	
}

local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS

-- The character select screen lines
STRINGS.CHARACTER_TITLES.paulciclops = "The Great"
STRINGS.CHARACTER_NAMES.paulciclops = "PaulCiclops"
STRINGS.CHARACTER_DESCRIPTIONS.paulciclops = "*Perk 1\n*Perk 2\n*Perk 3"
STRINGS.CHARACTER_QUOTES.paulciclops = "\"Quote\""

-- Custom speech strings
STRINGS.CHARACTERS.PAULCICLOPS = require "speech_paulciclops"

-- The character's name as appears in-game 
STRINGS.NAMES.PAULCICLOPS = "PaulCiclops"

-- The default responses of examining the character
STRINGS.CHARACTERS.GENERIC.DESCRIBE.PAULCICLOPS = 
{
	GENERIC = "It's PaulCiclops!",
	ATTACKER = "That PaulCiclops looks shifty...",
	MURDERER = "Murderer!",
	REVIVER = "PaulCiclops, friend of ghosts.",
	GHOST = "PaulCiclops could use a heart.",
}


AddMinimapAtlas("images/map_icons/paulciclops.xml")

-- Add mod character to mod character list. Also specify a gender. Possible genders are MALE, FEMALE, ROBOT, NEUTRAL, and PLURAL.
AddModCharacter("paulciclops", "MALE")

