-----------
local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient
local RECIPETABS = GLOBAL.RECIPETABS
Recipe = GLOBAL.Recipe
local STRINGS = GLOBAL.STRINGS
local ACTIONS = GLOBAL.ACTIONS
TECH = GLOBAL.TECH
local IsServer = GLOBAL.TheNet:GetIsServer()
local containers = require("containers")
ACTIONS.GIVE.priority = 3
ACTIONS.ADDFUEL.priority = 4
local ThePlayer = GLOBAL.ThePlayer
-----------
local TheInput = GLOBAL.TheInput
-----------sleep--------
local Sleep_Princess = GetModConfigData("sleeping_princess")
-----------difficulty-- test
local Difficult = GetModConfigData("difficultover")

local Widget = GLOBAL.require('widgets/widget')
local Image = GLOBAL.require('widgets/image')
local Text = GLOBAL.require('widgets/text')
local PlayerBadge = GLOBAL.require("widgets/playerbadge")
local Badge = GLOBAL.require("widgets/badge")
-----------specific
modimport("libs/env.lua")
use "data/actions/init"
use "data/components/init"
  
 -----------
PrefabFiles = {
"musha",
"mushasword_frost",
"mushasword_base",
"mushasword",
"frosthammer",
"tentacle_frost",
"broken_frosthammer",
"armor_mushaa",
"armor_mushab",
"hat_mprincess",
"hat_mbunny",
"hat_mphoenix",
"musha_flute",

"musha_small",
"musha_egg",
"musha_egg1",
"musha_egg2",
"musha_egg3",
"musha_eggs1",
"musha_eggs2",
"musha_eggs3",
"musha_egg8",

"ghosthound",	
"ghosthound2",	
"shadowmusha",

"cristal",
"forcefieldfxx",
"glowdust",
"exp",

"tent_musha",

}

table.insert(GLOBAL.CHARACTER_GENDERS.FEMALE, "musha")
Assets = {
 
Asset("ATLAS", "images/status_stamina.xml"),

Asset( "ANIM", "anim/musha_normal.zip"),
Asset( "ANIM", "anim/musha_battle.zip"),
Asset( "ANIM", "anim/musha.zip"),
Asset( "ANIM", "anim/musha_hunger.zip"),
Asset( "ANIM", "anim/musha_hunger.zip"),
----
Asset( "ANIM", "anim/musha_old.zip"),
Asset( "ANIM", "anim/musha_normal_old.zip"),
Asset( "ANIM", "anim/musha_battle_old.zip"),
Asset( "ANIM", "anim/musha_hunger_old.zip"),
Asset( "ANIM", "anim/musha_full_p_old.zip"),
Asset( "ANIM", "anim/musha_normal_p_old.zip"),
Asset( "ANIM", "anim/musha_battle_p_old.zip"),
Asset( "ANIM", "anim/musha_hunger_p_old.zip"),
----
Asset("ANIM", "anim/musha_egg8.zip"),
Asset("ANIM", "anim/musha_egg.zip"),
Asset("ANIM", "anim/musha_egg1.zip"),
Asset("ANIM", "anim/musha_egg2.zip"),
Asset("ANIM", "anim/musha_egg3.zip"),
Asset("ANIM", "anim/musha_eggs1.zip"),
Asset("ANIM", "anim/musha_eggs2.zip"),
Asset("ANIM", "anim/musha_eggs3.zip"),
Asset("ANIM", "anim/musha_ice.zip"),
Asset("ANIM", "anim/musha_small.zip"),
Asset("ANIM", "anim/musha_teen.zip"),
Asset("ANIM", "anim/musha_tall.zip"),
Asset("ANIM", "anim/musha_tall2.zip"),
Asset("ANIM", "anim/musha_tall3.zip"),
Asset("ANIM", "anim/musha_tall4.zip"),
Asset("ANIM", "anim/musha_tall5.zip"),

Asset("ANIM", "anim/frosthammer.zip"),
Asset("ANIM", "anim/swap_frosthammer.zip"),
Asset("ANIM", "anim/broken_frosthammer.zip"),
Asset("ANIM", "anim/swap_frostback.zip"),
Asset("ANIM", "anim/swap_frostpocket.zip"),
Asset("ANIM", "anim/hat_mbunny.zip"),
Asset("ANIM", "anim/hat_mbunny2.zip"),
Asset("ANIM", "anim/hat_mphoenix.zip"),
Asset("ANIM", "anim/hat_mphoenix2.zip"),
Asset("ANIM", "anim/hat_mcrown.zip"),
Asset("ANIM", "anim/hat_mprincess.zip"),
Asset("ANIM", "anim/ghosthound.zip"),
Asset("ANIM", "anim/armor_mushaa.zip"),
Asset("ANIM", "anim/armor_mushab.zip"),

Asset( "ANIM", "anim/tent_musha.zip"),
Asset( "ANIM", "anim/tent_musha_on.zip"),
Asset( "ANIM", "anim/tent_musha_broken.zip"),
--box 

Asset("ANIM", "anim/ui_chest_yamche0.zip"),
Asset("ANIM", "anim/ui_chest_yamche1.zip"),
Asset("ANIM", "anim/ui_chest_yamche2.zip"),
Asset("ANIM", "anim/hat_yamche.zip"),
Asset("ANIM", "anim/ui_frostsmall.zip"),
Asset("ANIM", "anim/ui_chest_frosthammer.zip"),
Asset("ANIM", "anim/ui_chest_frosthammer2.zip"),
Asset("ANIM", "anim/ui_mushab_2x3.zip"),

Asset("ANIM", "anim/forcefieldx.zip"),
Asset("ANIM", "anim/glowdust.zip"),
Asset("ANIM", "anim/cristal.zip"),
Asset("ANIM", "anim/exp.zip"),
Asset("ANIM", "anim/musha_flute.zip"),
Asset("ANIM", "anim/mushasword_base.zip"),
Asset("ANIM", "anim/mushasword_frost.zip"),
Asset("ANIM", "anim/mushasword.zip"),
Asset("ANIM", "anim/mushasword2.zip"),
Asset("ANIM", "anim/mushasword3.zip"),
Asset("ANIM", "anim/mushasword4.zip"),
Asset("ANIM", "anim/swap_mushasword_base.zip"),
Asset("ANIM", "anim/swap_mushasword_frost1.zip"),
Asset("ANIM", "anim/swap_mushasword_frost2.zip"),
Asset("ANIM", "anim/swap_mushasword_frost3.zip"),
Asset("ANIM", "anim/swap_mushasword.zip"),
Asset("ANIM", "anim/swap_mushasword2.zip"),
Asset("ANIM", "anim/swap_mushasword3.zip"),
Asset("ANIM", "anim/swap_mushasword4.zip"),
Asset("ANIM", "anim/tentacle_frost.zip"),
Asset("IMAGE", "images/inventoryimages/musha_egg.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg1.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg2.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg3.tex"),
Asset("IMAGE", "images/inventoryimages/musha_eggs1.tex"),
Asset("IMAGE", "images/inventoryimages/musha_eggs2.tex"),
Asset("IMAGE", "images/inventoryimages/musha_eggs3.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg_cracked.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg_cracked1.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg_cracked2.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg_cracked3.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg_crackeds1.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg_crackeds2.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg_crackeds3.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg_cooked.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg8.tex"),
Asset("IMAGE", "images/inventoryimages/musha_egg_cracked8.tex"),
Asset("IMAGE", "images/inventoryimages/musha_small.tex"),
Asset("IMAGE", "images/inventoryimages/musha_teen.tex"),
Asset("IMAGE", "images/inventoryimages/musha_tall.tex"),
Asset("IMAGE", "images/inventoryimages/frosthammer.tex"),
Asset("IMAGE", "images/inventoryimages/broken_frosthammer.tex"),
Asset("IMAGE", "images/inventoryimages/frostback.tex"),
Asset("IMAGE", "images/inventoryimages/hat_mbunny.tex"),
Asset("IMAGE", "images/inventoryimages/hat_mphoenix.tex"),
Asset("IMAGE", "images/inventoryimages/hat_mcrown.tex"),
Asset("IMAGE", "images/inventoryimages/hat_mprincess.tex"),
Asset("IMAGE", "images/inventoryimages/armor_mushaa.tex"),
Asset("IMAGE", "images/inventoryimages/armor_mushab.tex"),
Asset("IMAGE", "images/inventoryimages/musha_flute.tex"),
Asset("IMAGE", "images/inventoryimages/mushasword_base.tex"),
Asset("IMAGE", "images/inventoryimages/mushasword_frost.tex"),
Asset("IMAGE", "images/inventoryimages/mushasword.tex"),
Asset("IMAGE", "images/inventoryimages/mushasword2.tex"),
Asset("IMAGE", "images/inventoryimages/mushasword3.tex"),
Asset("IMAGE", "images/inventoryimages/mushasword4.tex"),
Asset("IMAGE", "images/inventoryimages/blade_b.tex"),
Asset("IMAGE", "images/inventoryimages/blade_1.tex"),
Asset("IMAGE", "images/inventoryimages/blade_2.tex"),
Asset("IMAGE", "images/inventoryimages/blade_3.tex"),
Asset("IMAGE", "images/inventoryimages/blade_4.tex"),
Asset("IMAGE", "images/inventoryimages/blade_f.tex"),
Asset("IMAGE", "images/inventoryimages/glowdust.tex"),
Asset("IMAGE", "images/inventoryimages/cristal.tex"),
Asset("IMAGE", "images/inventoryimages/exp.tex"),
Asset("ATLAS", "images/inventoryimages/frosthammer.xml"),
Asset("ATLAS", "images/inventoryimages/broken_frosthammer.xml"),
Asset("ATLAS", "images/inventoryimages/hat_mbunny.xml"),
Asset("ATLAS", "images/inventoryimages/hat_mcrown.xml"),
Asset("ATLAS", "images/inventoryimages/hat_mprincess.xml"),
Asset("ATLAS", "images/inventoryimages/armor_mushaa.xml"),
Asset("ATLAS", "images/inventoryimages/armor_mushab.xml"),
Asset("ATLAS", "images/inventoryimages/musha_flute.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword_base.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword2.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword3.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword4.xml"),
Asset("ATLAS", "images/inventoryimages/blade_f.xml"),
Asset("ATLAS", "images/inventoryimages/blade_b.xml"),
Asset("ATLAS", "images/inventoryimages/blade_1.xml"),
Asset("ATLAS", "images/inventoryimages/blade_2.xml"),
Asset("ATLAS", "images/inventoryimages/blade_3.xml"),
Asset("ATLAS", "images/inventoryimages/blade_4.xml"),
Asset("ATLAS", "images/musha.xml"),
Asset("ATLAS", "images/mushas.xml"),
Asset( "ATLAS", "images/saveslot_portraits/musha.xml" ),
Asset( "ATLAS", "images/selectscreen_portraits/musha.xml" ),
Asset( "ATLAS", "images/selectscreen_portraits/musha_silho.xml" ),
Asset( "ATLAS", "bigportraits/musha.xml" ),
Asset("ATLAS", "images/inventoryimages/frosthammer.xml"),
Asset("ATLAS", "images/inventoryimages/broken_frosthammer.xml"),
Asset("ATLAS", "images/inventoryimages/frostback.xml"),
Asset("ATLAS", "images/inventoryimages/hat_mbunny.xml"),
Asset("ATLAS", "images/inventoryimages/hat_mphoenix.xml"),
Asset("ATLAS", "images/inventoryimages/hat_mcrown.xml"),
Asset("ATLAS", "images/inventoryimages/hat_mprincess.xml"),
Asset("ATLAS", "images/inventoryimages/armor_mushaa.xml"),
Asset("ATLAS", "images/inventoryimages/armor_mushab.xml"),
Asset("ATLAS", "images/inventoryimages/musha_flute.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword_base.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword_frost.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword2.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword3.xml"),
Asset("ATLAS", "images/inventoryimages/mushasword4.xml"),
Asset("ATLAS", "images/inventoryimages/blade_b.xml"),
Asset("ATLAS", "images/inventoryimages/blade_1.xml"),
Asset("ATLAS", "images/inventoryimages/blade_2.xml"),
Asset("ATLAS", "images/inventoryimages/blade_3.xml"),
Asset("ATLAS", "images/inventoryimages/blade_4.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg8.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_cracked8.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg1.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg2.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg3.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_cracked.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_cracked1.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_cracked2.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_cracked3.xml"),
Asset("ATLAS", "images/inventoryimages/musha_eggs1.xml"),
Asset("ATLAS", "images/inventoryimages/musha_eggs2.xml"),
Asset("ATLAS", "images/inventoryimages/musha_eggs3.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_cracked.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_crackeds1.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_crackeds2.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_crackeds3.xml"),
Asset("ATLAS", "images/inventoryimages/musha_egg_cooked.xml"),
Asset("ATLAS", "images/inventoryimages/musha_small.xml"),
Asset("ATLAS", "images/inventoryimages/musha_teen.xml"),
Asset("ATLAS", "images/inventoryimages/musha_tall.xml"),
Asset("ATLAS", "images/inventoryimages/glowdust.xml"),
Asset("ATLAS", "images/inventoryimages/cristal.xml"),
Asset("ATLAS", "images/inventoryimages/exp.xml"),
Asset("ATLAS", "images/inventoryimages/tent_musha.xml"),
-----------------
Asset( "IMAGE", "images/saveslot_portraits/musha.tex"),
Asset( "ATLAS", "images/saveslot_portraits/musha.xml"),
Asset( "IMAGE", "images/saveslot_portraits/musha.tex" ),
Asset( "ATLAS", "images/saveslot_portraits/musha.xml" ),
Asset( "IMAGE", "images/selectscreen_portraits/musha.tex" ),
Asset( "ATLAS", "images/selectscreen_portraits/musha.xml" ),
Asset( "IMAGE", "bigportraits/musha.tex" ),
Asset( "ATLAS", "bigportraits/musha.xml" ),
Asset( "IMAGE", "images/musha.tex" ),
Asset( "ATLAS", "images/musha.xml" ),
Asset( "IMAGE", "images/musha_mapicon.tex" ),
Asset( "ATLAS", "images/musha_mapicon.xml" ),
Asset( "IMAGE", "images/avatars/avatar_musha.tex" ),
Asset( "ATLAS", "images/avatars/avatar_musha.xml" ),
Asset( "IMAGE", "images/avatars/avatar_ghost_musha.tex" ),
Asset( "ATLAS", "images/avatars/avatar_ghost_musha.xml" ),
}
AddMinimapAtlas("images/inventoryimages/hat_mphoenix.xml")
AddMinimapAtlas("images/inventoryimages/hat_mbunny.xml")
AddMinimapAtlas("images/inventoryimages/hat_mprincess.xml")
AddMinimapAtlas("images/inventoryimages/armor_mushaa.xml")
AddMinimapAtlas("images/inventoryimages/armor_mushab.xml")
AddMinimapAtlas("images/musha.xml")
AddMinimapAtlas("images/musha_mapicon.xml")
AddMinimapAtlas("images/mushas.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg8.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg_cracked8.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg_cracked.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg1.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg_cracked1.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg2.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg_cracked2.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg3.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg_cracked3.xml")
AddMinimapAtlas("images/inventoryimages/musha_eggs1.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg_crackeds1.xml")
AddMinimapAtlas("images/inventoryimages/musha_eggs2.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg_crackeds2.xml")
AddMinimapAtlas("images/inventoryimages/musha_eggs3.xml")
AddMinimapAtlas("images/inventoryimages/musha_egg_crackeds3.xml")
AddMinimapAtlas("images/inventoryimages/musha_small.xml")
AddMinimapAtlas("images/inventoryimages/musha_teen.xml")
AddMinimapAtlas("images/inventoryimages/musha_tall.xml")
AddMinimapAtlas("images/inventoryimages/frosthammer.xml")
--AddMinimapAtlas("images/inventoryimages/frostback.xml")
AddMinimapAtlas("images/inventoryimages/broken_frosthammer.xml")
AddMinimapAtlas("images/inventoryimages/mushasword_base.xml")
AddMinimapAtlas("images/inventoryimages/mushasword.xml")
--AddMinimapAtlas("images/inventoryimages/mushasword2.xml")
--AddMinimapAtlas("images/inventoryimages/mushasword3.xml")
--AddMinimapAtlas("images/inventoryimages/mushasword4.xml")
AddMinimapAtlas("images/inventoryimages/blade_f.xml")
AddMinimapAtlas("images/inventoryimages/blade_b.xml")
AddMinimapAtlas("images/inventoryimages/blade_1.xml")
--AddMinimapAtlas("images/inventoryimages/blade_2.xml")
--AddMinimapAtlas("images/inventoryimages/blade_3.xml")
--AddMinimapAtlas("images/inventoryimages/blade_4.xml")
AddMinimapAtlas("images/inventoryimages/cristal.xml")
AddMinimapAtlas("images/inventoryimages/exp.xml")
------------------box
--
-----------
local oldwidgetsetup = containers.widgetsetup
containers.widgetsetup = function(container, prefab)
if not prefab and container.inst.prefab == "armor_mushaa" then
prefab = "icepack" end
if not prefab and container.inst.prefab == "broken_frosthammer" then
prefab = "piggyback" end
if not prefab and container.inst.prefab == "armor_mushab" then
prefab = "piggyback" end
  if not prefab and container.inst.prefab == "musha_tall3" then  prefab = "chester" end   if not prefab and container.inst.prefab == "musha_tallrrr1" then  prefab = "chester" end   if not prefab and container.inst.prefab == "musha_tallrrr2" then  prefab = "chester" end   if not prefab and container.inst.prefab == "musha_tallrrr3" then  prefab = "chester" end   if not prefab and container.inst.prefab == "musha_tallrrr4" then  prefab = "chester" end   if not prefab and container.inst.prefab == "musha_tallrrr5" then  prefab = "chester" end   if not prefab and container.inst.prefab == "musha_tallrrrice" then  prefab = "chester" end 
oldwidgetsetup(container, prefab)
end

---------------
--small icebox1
local params = {}
local OVERRIDE_WIDGETSETUP = false
local containers_widgetsetup_base = containers.widgetsetup
function containers.widgetsetup(container, prefab)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
        if OVERRIDE_WIDGETSETUP then
            container.type = "frostsmall"
        end
    else
        containers_widgetsetup_base(container, prefab)
    end
end
local function frostsmall()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_backpack_2x4",
            animbuild = "ui_chest_frosthammer",
            pos = GLOBAL.Vector3(-5, 100, 0),
            side_align_tip = 160,
        },
		 issidewidget = true,
        type = "chest",
    }
    
	for y = 0, 1 do
		table.insert(container.widget.slotpos, GLOBAL.Vector3(-126, -y*75 + 114 ,-126 +75, -y*75 + 114 ))

end
    return container
end
params.frostsmall = frostsmall()
for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end
local containers_widgetsetup_custom = containers.widgetsetup
local MAXITEMSLOTS = containers.MAXITEMSLOTS
AddPrefabPostInit("world_network", function(inst)
    if containers.widgetsetup ~= containers_widgetsetup_custom then
        OVERRIDE_WIDGETSETUP = true
        local containers_widgetsetup_base2 = containers.widgetsetup
        function containers.widgetsetup(container, prefab)
            containers_widgetsetup_base2(container, prefab)
            if container.type == "frostsmall" then
                container.type = "chest"
            end end end
    if containers.MAXITEMSLOTS < MAXITEMSLOTS then
        containers.MAXITEMSLOTS = MAXITEMSLOTS
    end
end)

function params.frostsmall.itemtestfn(container, item, slot)
    return not item:HasTag("heatrock") 

end
--------------------------------------------------
--icebox1
local params = {}
local OVERRIDE_WIDGETSETUP = false
local containers_widgetsetup_base = containers.widgetsetup
function containers.widgetsetup(container, prefab)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
        if OVERRIDE_WIDGETSETUP then
            container.type = "frostbox"
        end
    else
        containers_widgetsetup_base(container, prefab)
    end
end
local function frostbox()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_backpack_2x4",
            animbuild = "ui_chest_frosthammer2",
            pos = GLOBAL.Vector3(-5, -70, 0),
            side_align_tip = 160,
        },
		 issidewidget = true,
			type = "pack",
    }
    	for y = 0, 4 do
	table.insert(container.widget.slotpos, GLOBAL.Vector3(-162, -y*58 + 124 ,0))
	table.insert(container.widget.slotpos, GLOBAL.Vector3(-162 +75, -y*58 + 124 ,0))

end
    return container
end
params.frostbox = frostbox()
for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end
local containers_widgetsetup_custom = containers.widgetsetup
local MAXITEMSLOTS = containers.MAXITEMSLOTS
AddPrefabPostInit("world_network", function(inst)
    if containers.widgetsetup ~= containers_widgetsetup_custom then
        OVERRIDE_WIDGETSETUP = true
        local containers_widgetsetup_base2 = containers.widgetsetup
        function containers.widgetsetup(container, prefab)
            containers_widgetsetup_base2(container, prefab)
            if container.type == "frostbox" then
                container.type = "pack"
            end end end
    if containers.MAXITEMSLOTS < MAXITEMSLOTS then
        containers.MAXITEMSLOTS = MAXITEMSLOTS
    end
end)

function params.frostbox.itemtestfn(container, item, slot)
    return not item:HasTag("heatrock") 

end
--------------------------------------------------

--box1
local params = {}
local OVERRIDE_WIDGETSETUP = false
local containers_widgetsetup_base = containers.widgetsetup
function containers.widgetsetup(container, prefab)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
        if OVERRIDE_WIDGETSETUP then
            container.type = "chest_yamche0"
        end
    else
        containers_widgetsetup_base(container, prefab)
    end
end
local function chest_yamche0()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_chest_3x2",
            animbuild = "ui_chest_yamche0",
            pos = GLOBAL.Vector3(0, 200, 0),
            side_align_tip = 160,
        },
        type = "chest",
    }
    for y = 1, 0, -1 do
        table.insert(container.widget.slotpos, GLOBAL.Vector3(74*y-74*2+70, 0))
 
end
    return container
end
params.chest_yamche0 = chest_yamche0()
for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end
local containers_widgetsetup_custom = containers.widgetsetup
local MAXITEMSLOTS = containers.MAXITEMSLOTS
AddPrefabPostInit("world_network", function(inst)
    if containers.widgetsetup ~= containers_widgetsetup_custom then
        OVERRIDE_WIDGETSETUP = true
        local containers_widgetsetup_base2 = containers.widgetsetup
        function containers.widgetsetup(container, prefab)
            containers_widgetsetup_base2(container, prefab)
            if container.type == "chest_yamche0" then
                container.type = "chest"
            end end end
    if containers.MAXITEMSLOTS < MAXITEMSLOTS then
        containers.MAXITEMSLOTS = MAXITEMSLOTS
    end
end)
---------------------------------------------------------------
--box2
local params = {}
local OVERRIDE_WIDGETSETUP = false
local containers_widgetsetup_base = containers.widgetsetup
function containers.widgetsetup(container, prefab)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
        if OVERRIDE_WIDGETSETUP then
            container.type = "chest_yamche1"
        end
    else
        containers_widgetsetup_base(container, prefab)
    end
end
local function chest_yamche1()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_chest_3x3",
            animbuild = "ui_chest_yamche1",
            pos = GLOBAL.Vector3(0, 200, 0),
            side_align_tip = 160,
        },
        type = "chest",
    }
 for y = 1, 0, -1 do
    for x = 0, 1 do
        table.insert(container.widget.slotpos, GLOBAL.Vector3(80*x-80*2+78, 80*y-80*2+80,0))
    end
end
    return container
end
params.chest_yamche1 = chest_yamche1()
for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end
local containers_widgetsetup_custom = containers.widgetsetup
local MAXITEMSLOTS = containers.MAXITEMSLOTS
AddPrefabPostInit("world_network", function(inst)
    if containers.widgetsetup ~= containers_widgetsetup_custom then
        OVERRIDE_WIDGETSETUP = true
        local containers_widgetsetup_base2 = containers.widgetsetup
        function containers.widgetsetup(container, prefab)
            containers_widgetsetup_base2(container, prefab)
            if container.type == "chest_yamche1" then
                container.type = "chest"
            end end end
    if containers.MAXITEMSLOTS < MAXITEMSLOTS then
        containers.MAXITEMSLOTS = MAXITEMSLOTS
    end
end)
---------------------------------------------------------------
--box3
local params = {}
local OVERRIDE_WIDGETSETUP = false
local containers_widgetsetup_base = containers.widgetsetup
function containers.widgetsetup(container, prefab)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
        if OVERRIDE_WIDGETSETUP then
            container.type = "chest_yamche2"
        end
    else
        containers_widgetsetup_base(container, prefab)
    end
end
local function chest_yamche2()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_chest_3x3",
            animbuild = "ui_chest_yamche2",
            pos = GLOBAL.Vector3(0, 200, 0),
            side_align_tip = 160,
        },
        type = "chest",
    }
		
for y = 2, 0, -1 do
    for x = 0, 1 do
        table.insert(container.widget.slotpos, GLOBAL.Vector3(80*x-80*2+78, 80*y-80*2+80,0))
    end
end
    return container
end
params.chest_yamche2 = chest_yamche2()
for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end
local containers_widgetsetup_custom = containers.widgetsetup
local MAXITEMSLOTS = containers.MAXITEMSLOTS
AddPrefabPostInit("world_network", function(inst)
    if containers.widgetsetup ~= containers_widgetsetup_custom then
        OVERRIDE_WIDGETSETUP = true
        local containers_widgetsetup_base2 = containers.widgetsetup
        function containers.widgetsetup(container, prefab)
            containers_widgetsetup_base2(container, prefab)
            if container.type == "chest_yamche2" then
                container.type = "chest"
        end end end
    if containers.MAXITEMSLOTS < MAXITEMSLOTS then
        containers.MAXITEMSLOTS = MAXITEMSLOTS
    end
end)
---------------------------------------------------------------
--box5
local params = {}
local OVERRIDE_WIDGETSETUP = false
local containers_widgetsetup_base = containers.widgetsetup
function containers.widgetsetup(container, prefab)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
        if OVERRIDE_WIDGETSETUP then
            container.type = "chest_yamche4"
        end
    else
        containers_widgetsetup_base(container, prefab)
    end
end
local function chest_yamche4()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_chest_3x3",
            animbuild = "ui_chest_3x3",
            pos = GLOBAL.Vector3(0, 200, 0),
            side_align_tip = 160,
        },
        type = "chest",
    }
for y = 3, 0, -1 do
    for x = 0, 2 do
        table.insert(container.widget.slotpos, GLOBAL.Vector3(75*x-75*2+75, 60*y-60*2+32,0))
    end
end
    return container
end
params.chest_yamche4 = chest_yamche4()
for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end
local containers_widgetsetup_custom = containers.widgetsetup
local MAXITEMSLOTS = containers.MAXITEMSLOTS
AddPrefabPostInit("world_network", function(inst)
    if containers.widgetsetup ~= containers_widgetsetup_custom then
        OVERRIDE_WIDGETSETUP = true
        local containers_widgetsetup_base2 = containers.widgetsetup
        function containers.widgetsetup(container, prefab)
            containers_widgetsetup_base2(container, prefab)
            if container.type == "chest_yamche4" then
                container.type = "chest"
        end end end
    if containers.MAXITEMSLOTS < MAXITEMSLOTS then
        containers.MAXITEMSLOTS = MAXITEMSLOTS
    end
end)
---------------------------------------------------------------
--box6
local params = {}
local OVERRIDE_WIDGETSETUP = false
local containers_widgetsetup_base = containers.widgetsetup
function containers.widgetsetup(container, prefab)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
        if OVERRIDE_WIDGETSETUP then
            container.type = "chest_yamche5"
        end
    else
        containers_widgetsetup_base(container, prefab)
    end
end
local function chest_yamche5()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_chest_3x3",
            animbuild = "ui_chest_3x3",
            pos = GLOBAL.Vector3(0, 200, 0),
            side_align_tip = 160,
        },
        type = "chest",
    }
for y = 3, 0, -1 do
    for x = 0, 3 do
        table.insert(container.widget.slotpos, GLOBAL.Vector3(60*x-60*2+30, 60*y-60*2+30,0))
    end
end
    return container
end
params.chest_yamche5 = chest_yamche5()
for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end
local containers_widgetsetup_custom = containers.widgetsetup
local MAXITEMSLOTS = containers.MAXITEMSLOTS
AddPrefabPostInit("world_network", function(inst)
    if containers.widgetsetup ~= containers_widgetsetup_custom then
        OVERRIDE_WIDGETSETUP = true
        local containers_widgetsetup_base2 = containers.widgetsetup
        function containers.widgetsetup(container, prefab)
            containers_widgetsetup_base2(container, prefab)
            if container.type == "chest_yamche5" then
                container.type = "chest"
    end end end
    if containers.MAXITEMSLOTS < MAXITEMSLOTS then
        containers.MAXITEMSLOTS = MAXITEMSLOTS
    end
end)

---------------------------------------------------------------
------------------
STRINGS.NAMES.MUSHA = "Musha"
STRINGS.CHARACTER_TITLES.musha = "Puppy Princess Musha in DST"
STRINGS.CHARACTER_NAMES.musha = "Musha"
STRINGS.CHARACTER_DESCRIPTIONS.musha = "[Level up and unlock to Skills]\nStat(L) Skill(K) Visual(P)\nLightning/Valkyrie(R) Shield(C) Performance/Sleep(X) Yamche(Z,V,B))"
STRINGS.CHARACTER_QUOTES.musha = "\" I am True Princess ! \""
STRINGS.CHARACTERS.MUSHA= require "speech_musha"

--STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA = "Feels good.."
STRINGS.CHARACTERS.MUSHA.DESCRIBE.MUSHA = {"she pretty", "Resembles me.."}
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.MUSHA = ("maybe she can join my Books club members.")
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.MUSHA = ("Royal blood ? Hmm...")
STRINGS.CHARACTERS.WOODIE.DESCRIBE.MUSHA = ("she can transform?..no, maybe not")
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.MUSHA = ("She is weak force .. but seems going to help us.")
STRINGS.CHARACTERS.WENDY.DESCRIBE.MUSHA = ("I feel a strong spirit..")
STRINGS.CHARACTERS.WX78.DESCRIBE.MUSHA = ("I feel my heart.")
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA = 
{
GENERIC = "Feels good!",
ATTACKER = "Fight fight fight",
MURDERER = "Assasin!",
REVIVER = "Return..",
GHOST = "go away !",
}
--STRINGS.CHARACTERS.MUSHA.ANNOUNCE_NOSANITY = ""	

-----------[[--recipe--]]-----------

local bladeb = Ingredient( "mushasword_base", 1)
bladeb.atlas = "images/inventoryimages/mushasword_base.xml"
local armora = Ingredient( "armor_mushaa", 1)
armora.atlas = "images/inventoryimages/armor_mushaa.xml"
local phoenixb = Ingredient( "mushasword", 1)
phoenixb.atlas = "images/inventoryimages/mushasword.xml"

local phoenix_egg = Ingredient( "musha_egg", 1)
phoenix_egg.atlas = "images/inventoryimages/musha_egg.xml"
local phoenix_eggs1 = Ingredient( "musha_eggs1", 1)
phoenix_eggs1.atlas = "images/inventoryimages/musha_eggs1.xml"
local phoenix_eggs2 = Ingredient( "musha_eggs2", 1)
phoenix_eggs2.atlas = "images/inventoryimages/musha_eggs2.xml"
local phoenix_eggs3 = Ingredient( "musha_eggs3", 1)
phoenix_eggs3.atlas = "images/inventoryimages/musha_eggs3.xml"
local phoenix_egg1 = Ingredient( "musha_egg1", 1)
phoenix_egg1.atlas = "images/inventoryimages/musha_egg1.xml"
local phoenix_egg2 = Ingredient( "musha_egg2", 1)
phoenix_egg2.atlas = "images/inventoryimages/musha_egg2.xml"
local phoenix_egg3 = Ingredient( "musha_egg3", 1)
phoenix_egg3.atlas = "images/inventoryimages/musha_egg3.xml"
local phoenix_egg8 = Ingredient( "musha_egg8", 1)
phoenix_egg8.atlas = "images/inventoryimages/musha_egg8.xml"

local glowdust = Ingredient( "glowdust", 1)
glowdust.atlas = "images/inventoryimages/glowdust.xml"
local glowdust3 = Ingredient( "glowdust", 3)
glowdust3.atlas = "images/inventoryimages/glowdust.xml"
local glowdust5 = Ingredient( "glowdust", 5)
glowdust5.atlas = "images/inventoryimages/glowdust.xml"
local glowdust10 = Ingredient( "glowdust", 10)
glowdust10.atlas = "images/inventoryimages/glowdust.xml"
----
--tent
local tent_musha = GLOBAL.Recipe("tent_musha", { glowdust5, Ingredient("fireflies", 6), Ingredient("papyrus", 12), Ingredient("twigs", 12), }, RECIPETABS.MAGIC, TECH.NONE, "tent_placer" ) 
tent_musha.atlas = "images/inventoryimages/tent_musha.xml"
tent_musha.tagneeded = false
tent_musha.builder_tag ="musha"
----
local mushasword_base = AddRecipe("mushasword_base", { glowdust, GLOBAL.Ingredient("goldnugget", 2)}, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/mushasword_base.xml", "mushasword_base.tex" )
mushasword_base.tagneeded = false
mushasword_base.builder_tag ="musha"

local mushasword = AddRecipe("mushasword", {bladeb, GLOBAL.Ingredient("redgem", 5), glowdust}, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/mushasword.xml", "mushasword.tex" )
mushasword.tagneeded = false
mushasword.builder_tag ="musha"

local mushasword_frost = AddRecipe("mushasword_frost", {bladeb, GLOBAL.Ingredient("bluegem", 4), glowdust}, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/mushasword_frost.xml", "mushasword_frost.tex" )
mushasword_frost.tagneeded = false
mushasword_frost.builder_tag ="musha"

local frosthammer = AddRecipe("frosthammer", {GLOBAL.Ingredient( "deerclops_eyeball", 1), GLOBAL.Ingredient( "bluegem", 6), GLOBAL.Ingredient("livinglog", 2), GLOBAL.Ingredient("gears", 2)}, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/frosthammer.xml", "frosthammer.tex" )
frosthammer.tagneeded = false
frosthammer.builder_tag ="musha"

local armor_mushaa = AddRecipe("armor_mushaa", {glowdust3, GLOBAL.Ingredient("goldnugget", 20), GLOBAL.Ingredient("nitre", 5)}, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/armor_mushaa.xml", "armor_mushaa.tex" )
armor_mushaa.tagneeded = false
armor_mushaa.builder_tag ="musha"		

local broken_frosthammer = AddRecipe("broken_frosthammer", {armora, GLOBAL.Ingredient("bluegem", 8), GLOBAL.Ingredient("butterfly", 10),GLOBAL.Ingredient("gears", 3)}, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/broken_frosthammer.xml", "broken_frosthammer.tex" )
broken_frosthammer.tagneeded = true
broken_frosthammer.builder_tag ="musha"	

local armor_mushab = AddRecipe("armor_mushab", {armora, GLOBAL.Ingredient("purplegem", 25), GLOBAL.Ingredient("thulecite", 4),GLOBAL.Ingredient("thulecite_pieces", 8)}, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/armor_mushab.xml", "armor_mushab.tex" )
armor_mushab.tagneeded = false
armor_mushab.builder_tag ="musha"	

--manrabbit_tail
local hat_mbunny = AddRecipe("hat_mbunny", {GLOBAL.Ingredient("walrus_tusk", 2), GLOBAL.Ingredient("purplegem", 2), GLOBAL.Ingredient("rabbit", 2),GLOBAL.Ingredient("beefalowool", 15)}, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/hat_mbunny.xml", "hat_mbunny.tex" )
hat_mbunny.tagneeded = false
hat_mbunny.builder_tag ="musha"

local hat_mprincess = AddRecipe("hat_mprincess", {glowdust5, GLOBAL.Ingredient("purplegem", 4), GLOBAL.Ingredient("amulet", 1) }, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/hat_mprincess.xml", "hat_mprincess.tex" )
hat_mprincess.tagneeded = false
hat_mprincess.builder_tag ="musha"	
--phoenixb
local hat_mphoenix = AddRecipe("hat_mphoenix", { armora, GLOBAL.Ingredient("panflute", 1), GLOBAL.Ingredient("redgem", 20), GLOBAL.Ingredient("feather_robin", 10)}, RECIPETABS.WAR, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/hat_mphoenix.xml", "hat_mphoenix.tex" )
hat_mphoenix.tagneeded = false
hat_mphoenix.builder_tag ="musha"
--magic
--flute
local musha_flute = AddRecipe("musha_flute", { glowdust10, GLOBAL.Ingredient("horn", 1), GLOBAL.Ingredient("spidergland", 40)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/musha_flute.xml", "musha_flute.tex" )
musha_flute.tagneeded = false
musha_flute.builder_tag ="musha"
--glowdust
local glowdust = AddRecipe("glowdust", { GLOBAL.Ingredient("berries", 5),GLOBAL.Ingredient("goldnugget", 1)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/glowdust.xml", "glowdust.tex" )
glowdust.tagneeded = false
glowdust.builder_tag ="musha"
--phoenix_egg
local musha_egg = AddRecipe("musha_egg", { GLOBAL.Ingredient("purplegem", 6), GLOBAL.Ingredient("redgem", 12), GLOBAL.Ingredient("bluegem", 8)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/musha_egg.xml", "musha_egg.tex" )
musha_egg.tagneeded = false
musha_egg.builder_tag ="musha"

local musha_eggs1 = AddRecipe("musha_eggs1", { phoenix_egg, GLOBAL.Ingredient("goldnugget", 1)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/musha_eggs1.xml", "musha_eggs1.tex" )
musha_eggs1.tagneeded = false
musha_eggs1.builder_tag ="musha"

local musha_eggs2 = AddRecipe("musha_eggs2", { phoenix_eggs1, GLOBAL.Ingredient("goldnugget", 10)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/musha_eggs2.xml", "musha_eggs2.tex" )
musha_eggs2.tagneeded = false
musha_eggs2.builder_tag ="musha"

local musha_eggs3 = AddRecipe("musha_eggs3", { phoenix_eggs2, GLOBAL.Ingredient("goldnugget", 20)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/musha_eggs3.xml", "musha_eggs3.tex" )
musha_eggs3.tagneeded = false
musha_eggs3.builder_tag ="musha"

local musha_egg1 = AddRecipe("musha_egg1", { phoenix_eggs3, GLOBAL.Ingredient("goldnugget", 30), GLOBAL.Ingredient("purplegem", 1)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/musha_egg1.xml", "musha_egg1.tex" )
musha_egg1.tagneeded = false
musha_egg1.builder_tag ="musha"

local musha_egg2 = AddRecipe("musha_egg2", { phoenix_egg1, GLOBAL.Ingredient("goldnugget", 45), GLOBAL.Ingredient("purplegem", 2)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/musha_egg2.xml", "musha_egg2.tex" )
musha_egg2.tagneeded = false
musha_egg2.builder_tag ="musha"

local musha_egg3 = AddRecipe("musha_egg3", { phoenix_egg2, GLOBAL.Ingredient("goldnugget", 60), GLOBAL.Ingredient("purplegem", 5)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/musha_egg3.xml", "musha_egg3.tex" )
musha_egg3.tagneeded = false
musha_egg3.builder_tag ="musha"

local musha_egg8 = AddRecipe("musha_egg8", { phoenix_egg3, GLOBAL.Ingredient("goldnugget", 80), GLOBAL.Ingredient("purplegem", 10)}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/musha_egg8.xml", "musha_egg8.tex" )
musha_egg8.tagneeded = false
musha_egg8.builder_tag ="musha"


--crystal
local cristal = AddRecipe("cristal", { glowdust10}, RECIPETABS.MAGIC, TECH.NONE, nil, nil, nil, nil, nil, "images/inventoryimages/cristal.xml", "cristal.tex" )
cristal.tagneeded = false
cristal.builder_tag ="musha"

--function rockss(inst)
--[[AddRecipe("nitre", {Ingredient( "goldnugget",1),}, RECIPETABS.REFINE, {SCIENCE=0})]]
AddRecipe("bluegem", {Ingredient( "redgem",1),Ingredient( "nitre",1)}, RECIPETABS.REFINE, {SCIENCE=0})
AddRecipe("redgem", {Ingredient( "bluegem",1),Ingredient( "nitre",1)}, RECIPETABS.REFINE, {SCIENCE=0})

----BOOK----

--end
--AddPrefabPostInit("musha", rockss)
--elemental
local function elemental( inst )
inst:AddComponent("fuel")
inst.components.fuel.fuelvalue = TUNING.SMALL_FUEL
--inst.components.fuel.fuelvalue = TUNING.MED_LARGE_FUEL
inst.components.fuel.fueltype = "CHEMICAL"
inst:AddTag("test")
end
AddPrefabPostInit("rocks",elemental)
AddPrefabPostInit("goldnugget",elemental)
AddPrefabPostInit("flint",elemental)
AddPrefabPostInit("moonrocknugget",elemental)
AddPrefabPostInit("marble",elemental)
AddPrefabPostInit("stinger",elemental)
AddPrefabPostInit("spidergland",elemental)
AddPrefabPostInit("houndstooth",elemental)

function gruefix(inst)
inst:AddTag("gruef")
end
AddPrefabPostInit("grue", gruefix)

--active key
-- Import the lib use.
modimport("libs/use.lua")

-- Import the mod environment as our environment.
use "libs/mod_env"(env)
-- Imports to keep the keyhandler from working while typing in chat.
use "data/widgets/controls"
use "data/screens/chatinputscreen"
use "data/screens/consolescreen"

GLOBAL.TUNING.MUSHA = {}
GLOBAL.TUNING.MUSHA.KEY = GetModConfigData("key") or 108  --L
GLOBAL.TUNING.MUSHA.KEY2 = GetModConfigData("key2") or 114  --R
GLOBAL.TUNING.MUSHA.KEY3 = GetModConfigData("key3") or 99  --C
GLOBAL.TUNING.MUSHA.KEY4 = GetModConfigData("key4") or 120  --X  
GLOBAL.TUNING.MUSHA.KEY5 = GetModConfigData("key5") or 107  --K
GLOBAL.TUNING.MUSHA.KEY6 = GetModConfigData("key6") or 122  --Z
GLOBAL.TUNING.MUSHA.KEY7 = GetModConfigData("key7") or 112  --P
GLOBAL.TUNING.MUSHA.KEY8 = GetModConfigData("key8") or 118  --V
GLOBAL.TUNING.MUSHA.KEY9 = GetModConfigData("key9") or 98  --B
GLOBAL.TUNING.MUSHA.KEY10 = GetModConfigData("key10") or 110  --N
GLOBAL.TUNING.MUSHA.KEY11 = GetModConfigData("key11") or 103  --G
GLOBAL.TUNING.MUSHA.KEY12 = GetModConfigData("key12") or 116  --T
---------------
local function on_yamcheinfo(inst)
local x,y,z = inst.Transform:GetWorldPosition()
local ents = TheSim:FindEntities(x,y,z, 25, {"yamcheb"})
for k,v in pairs(ents) do
if inst.components.leader:IsFollower(v) then
v.yamcheinfo = true
v.yamche = true
end end end

local function INFO(inst)
--Active info level?
local TheInput = TheInput
local max_exp = 999997000
local level = math.min(inst.level, max_exp)
	local max_stamina = 100
	local min_stamina = 0
	local max_fatigue = 100
	local min_fatigue = 0	
	local max_music = 100
	local min_music = 0	
			local mx=math.floor(max_stamina-min_stamina)
			local cur=math.floor(inst.stamina-min_stamina)
			local mx2=math.floor(max_fatigue-min_fatigue)
			local cur2=math.floor(inst.fatigue-min_fatigue)
			local mxx=math.floor(max_music-min_music)
			local curr=math.floor(inst.music-min_music)
			sleep = ""..math.floor(cur*100/mx).."%"
			sleepy = ""..math.floor(cur2*100/mx2).."%"
			music = ""..math.floor(curr*100/mxx).."%"
			
inst.components.talker:Say("[Next: Level Up] [EXP]: ".. (inst.level) .."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
on_yamcheinfo(inst)
if inst.level <5 then
inst.components.talker:Say("Level 1 [EXP]: ".. (inst.level) .."/ 5".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=5 and inst.level <9  then
inst.components.talker:Say("Level 2 [EXP]: ".. (inst.level) .."/ 10".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=10 and inst.level <29  then
inst.components.talker:Say("Level 3 [EXP]: ".. (inst.level) .."/ 30".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=30 and inst.level <49  then
inst.components.talker:Say("Level 4 [EXP]: ".. (inst.level) .."/ 50".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=50 and inst.level <79  then
inst.components.talker:Say("Level 5 [EXP]: ".. (inst.level) .."/ 80".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=80 and inst.level <124  then
inst.components.talker:Say("Level 6 [EXP]: ".. (inst.level) .."/ 125".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=125 and inst.level <199  then
inst.components.talker:Say("Level 7 [EXP]: ".. (inst.level) .."/ 200".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=200 and inst.level <339  then
inst.components.talker:Say("Level 8 [EXP]: ".. (inst.level) .."/ 340".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=340 and inst.level <429  then
inst.components.talker:Say("Level 9 [EXP]: ".. (inst.level) .."/ 430".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=430 and inst.level <529  then
inst.components.talker:Say("Level 10 [EXP]: ".. (inst.level) .."/ 530".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=530 and inst.level <639  then
inst.components.talker:Say("Level 11 [EXP]: ".. (inst.level) .."/ 640".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=640 and inst.level <759  then
inst.components.talker:Say("Level 12 [EXP]: ".. (inst.level) .."/ 760".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=760 and inst.level <889  then
inst.components.talker:Say("Level 13 [EXP]: ".. (inst.level) .."/ 890".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=890 and inst.level <1029  then
inst.components.talker:Say("Level 14 [EXP]: ".. (inst.level) .."/ 1030".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=1030 and inst.level <1179  then
inst.components.talker:Say("Level 15 [EXP]: ".. (inst.level) .."/ 1180".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=1180 and inst.level <1339  then
inst.components.talker:Say("Level 16 [EXP]: ".. (inst.level) .."/ 1340".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=1340 and inst.level <1509  then
inst.components.talker:Say("Level 17 [EXP]: ".. (inst.level) .." 1510".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=1510 and inst.level <1689  then
inst.components.talker:Say("Level 18 [EXP]: ".. (inst.level) .."/ 1690".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=1690 and inst.level <1879  then
inst.components.talker:Say("Level 19 [EXP]: ".. (inst.level) .."/ 1880".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=1880 and inst.level <2079  then
inst.components.talker:Say("Level 20 [EXP]: ".. (inst.level) .."/ 2080".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=2080 and inst.level <2289  then
inst.components.talker:Say("Level 21 [EXP]: ".. (inst.level) .."/ 2289".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=2290 and inst.level <2499  then
inst.components.talker:Say("Level 22 [EXP]: ".. (inst.level) .."/ 2500".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=2500 and inst.level <2849  then
inst.components.talker:Say("Level 23 [EXP]: ".. (inst.level) .."/ 2850".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=2850 and inst.level <3199  then
inst.components.talker:Say("Level 24 [EXP]: ".. (inst.level) .."/ 3200".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=3200 and inst.level <3699  then
inst.components.talker:Say("Level 25 [EXP]: ".. (inst.level) .."/ 3700".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=3700 and inst.level <4199  then
inst.components.talker:Say("Level 26 [EXP]: ".. (inst.level) .."/ 4200".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=4200 and inst.level <4699  then
inst.components.talker:Say("Level 27 [EXP]: ".. (inst.level) .."/ 4700".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=4700 and inst.level <5499 then
inst.components.talker:Say("Level 28 [EXP]: ".. (inst.level) .."/ 5500".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=5500 and inst.level <6999 then
inst.components.talker:Say("Level 29 [EXP]: ".. (inst.level) .."/ 7000".."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
elseif inst.level >=7000  then
inst.components.talker:Say("Level 30 \n[MAX]\n Extra EXP ".. (inst.level -7000).."\n[Sleep]: "..(sleep).."   [Tired]: "..(sleepy).."\n[Performance]: "..(music))	
end
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end
AddModRPCHandler("musha", "INFO", INFO)

--active skill?  --skill_info
local function INFO2(inst)
local TheInput = TheInput
local max_exp = 999997000
local level = math.min(inst.level, max_exp)

if inst.level >=0 and inst.level <=4 then --level[1]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [0/4]-(R)\n[*]Spark Shield		[1/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [0/14] \n[*]Berserk         [0/3] \n[*]Electric Shield [0/5] \n[*]Critical Hit    [0/7] \n[*]Double Damage [0/1]")
elseif inst.level >4 and inst.level <10 then --level[2]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [0/4]-(R)\n[*]Spark Shield     [1/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [0/14] \n[*]Berserk         [0/3] \n[*]Electric Shield [1/5] \n[*]Critical Hit    [0/7] \n[*]Double Damage [0/1]")
elseif inst.level >10 and inst.level <30  then --level[3]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [1/4]-(R)\n[*]Spark Shield     [1/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [1/14] \n[*]Berserk         [0/3] \n[*]Electric Shield [1/5] \n[*]Critical Hit    [0/7] \n[*]Double Damage [0/1]")
elseif inst.level >=30 and inst.level <50  then --level[4]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [1/4]-(R)\n[*]Spark Shield     [1/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [1/14] \n[*]Berserk         [0/3] \n[*]Electric Shield [1/5] \n[*]Critical Hit    [1/7] \n[*]Double Damage [0/1]")
elseif inst.level >=50 and inst.level <80  then --level[5]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [1/4]-(R)\n[*]Spark Shield     [1/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [2/14] \n[*]Berserk         [0/3] \n[*]Electric Shield [1/5] \n[*]Critical Hit    [1/7] \n[*]Double Damage [0/1]")
elseif inst.level >=80 and inst.level <124  then --level[6]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [1/4]-(R)\n[*]Spark Shield     [1/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [2/14] \n[*]Berserk         [0/3] \n[*]Electric Shield [2/5] \n[*]Critical Hit    [1/7] \n[*]Double Damage [0/1]")
elseif inst.level >=125 and inst.level <200  then --level[7]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [1/4]-(R)\n[*]Spark Shield     [1/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [2/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [2/5] \n[*]Critical Hit    [1/7] \n[*]Double Damage [0/1]")
elseif inst.level >=200 and inst.level <340  then --level[8]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [1/4]-(R)\n[*]Spark Shield     [1/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [3/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [2/5] \n[*]Critical Hit    [1/7] \n[*]Double Damage [0/1]")
elseif inst.level >=340 and inst.level <430  then --level[9]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [1/4]-(R)\n[*]Spark Shield     [1/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [4/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [2/5] \n[*]Critical Hit    [1/7] \n[*]Double Damage [0/1]")
elseif inst.level >=430 and inst.level <530  then --level[10]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [4/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [3/5] \n[*]Critical Hit    [1/7] \n[*]Double Damage [0/1]")
elseif inst.level >=530 and inst.level <640  then --level[11]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [4/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [3/5] \n[*]Critical Hit    [2/7] \n[*]Double Damage [0/1]")
elseif inst.level >=640 and inst.level <760  then --level[12]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [4/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [3/5] \n[*]Critical Hit    [3/7] \n[*]Double Damage [0/1]")
elseif inst.level >=760 and inst.level <890  then --level[13]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [5/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [3/5] \n[*]Critical Hit    [3/7] \n[*]Double Damage [0/1]")
elseif inst.level >=890 and inst.level <1030  then --level[14]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [6/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [3/5] \n[*]Critical Hit    [3/7] \n[*]Double Damage [0/1]")
elseif inst.level >=1030 and inst.level <1180  then --level[15]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [6/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [4/5] \n[*]Critical Hit    [3/7] \n[*]Double Damage [0/1]")
elseif inst.level >=1180 and inst.level <1340  then --level[16]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [6/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [4/5] \n[*]Critical Hit    [4/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=1340 and inst.level <1510  then --level[17]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [7/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [4/5] \n[*]Critical Hit    [4/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=1510 and inst.level <1690  then --level[18]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [8/14] \n[*]Berserk         [1/3] \n[*]Electric Shield [4/5] \n[*]Critical Hit    [4/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=1690 and inst.level <1880  then --level[19]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [2/4]-(R)\n[*]Spark Shield     [2/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [8/14] \n[*]Berserk         [2/3] \n[*]Electric Shield [4/5] \n[*]Critical Hit    [4/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=1880 and inst.level <2080  then --level[20]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [8/14] \n[*]Berserk         [2/3] \n[*]Electric Shield [4/5] \n[*]Critical Hit    [5/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=2080 and inst.level <2290  then --level[21]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [8/14] \n[*]Berserk         [2/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [5/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=2290 and inst.level <2500  then --level[22]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [9/14] \n[*]Berserk         [2/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [5/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=2500 and inst.level <2850  then --level[23]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [10/14] \n[*]Berserk         [2/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [5/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=2850 and inst.level <3200  then --level[24]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [10/14] \n[*]Berserk         [3/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [5/7] \n[*]Double Damage [0/1]")
elseif inst.level >=3200 and inst.level <3700  then --level[25]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [10/14] \n[*]Berserk         [3/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [6/7] \n[*]Double Damage [0/1]")
elseif inst.level >=3700 and inst.level <4200  then --level[26]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [11/14] \n[*]Berserk         [3/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [6/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=4200 and inst.level <4700  then --level[27]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [11/14] \n[*]Berserk         [3/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [7/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=4700 and inst.level <5500 then --level[28]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [12/14] \n[*]Berserk         [3/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [7/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=5500 and inst.level <7000 then --level[29]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [3/4]-(R)\n[*]Spark Shield     [3/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [13/14] \n[*]Berserk         [3/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [7/7] \n[*]Double Damage [0/1]")	
elseif inst.level >=7000  then --level[30]
inst.components.talker:Say("[------Active Skill------] \n[*]Power Lightning [4/4]-(R)\n[*]Spark Shield     [4/4]-(C)\n[*]Perfomance       [1/1]-(X)\n[*]Hide in Shadow  [1/1]-(G)\n[------Passive Skill------] \n[*]Valkyrie       [14/14] \n[*]Berserk         [3/3] \n[*]Electric Shield [5/5] \n[*]Critical Hit    [7/7] \n[*]Double Damage [1/1]")	
end
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end
AddModRPCHandler("musha", "INFO2", INFO2)
--exp kill

local function spawnspirit(inst, x, y, z, scale)
    local fx = SpawnPrefab("wathgrithr_spirit")
    fx.Transform:SetPosition(x, y, z)
    fx.Transform:SetScale(scale, scale, scale)
end

local function onkilll2(inst, data)
local smallScale = 0.5
local medScale = 0.7
local largeScale = 1.1

	local victim = data.victim
	   if not (victim:HasTag("prey") or
            victim:HasTag("veggie") or
            victim:HasTag("eyeplant") or
            victim:HasTag("insect") or			
            victim:HasTag("structure")) then
    local delta = victim.components.combat.defaultdamage * 0.25
		   if math.random() < 0.5 then
    				inst.level = inst.level + 1
 		--inst.components.health:DoDelta(delta, false, "battleborn")
		inst.components.sanity:DoDelta(delta)
			inst.components.talker:Say("Lightning kill \nLucky [EXP](+1) \n".. (inst.level))	
			
            local time = victim.components.health.destroytime or 2
            local x, y, z = victim.Transform:GetWorldPosition()
            local scale = (victim:HasTag("smallcreature") and smallScale)
                        or (victim:HasTag("largecreature") and largeScale)
                        or medScale
					inst:DoTaskInTime(time, spawnspirit, x, y, z, scale)
				 	   if not inst.berserk and math.random() < 0.05 then
					   --inst.components.talker:Say("Come to me! Ghost puppy")
					     scheduler:ExecuteInTime(1, function() SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get()) SpawnPrefab("ghosthound2").Transform:SetPosition(victim:GetPosition():Get()) end) end
					end					
				end
		  if (victim:HasTag("prey") or
              victim:HasTag("insect") or			
            victim:HasTag("frog")) then
    local delta = victim.components.combat.defaultdamage * 0.1
		   if math.random() < 0.15 then
    				inst.level = inst.level + 1
 			inst.components.sanity:DoDelta(delta)
			inst.components.talker:Say("Lightning kill \nLucky [EXP](+1) \n".. (inst.level))					
            local time = victim.components.health.destroytime or 2
            local x, y, z = victim.Transform:GetWorldPosition()
            local scale = (victim:HasTag("smallcreature") and smallScale)
                        or (victim:HasTag("largecreature") and largeScale)
                        or medScale
					inst:DoTaskInTime(time, spawnspirit, x, y, z, scale)
					end end 	
							  if (victim:HasTag("epic") ) then
    local delta = victim.components.combat.defaultdamage * 0.1
		   if math.random() <= 1 then
    				inst.level = inst.level + 10
 			inst.components.sanity:DoDelta(delta)
			inst.components.talker:Say("Lightning kill \nLucky [EXP](+10) \n".. (inst.level))		
			
            local time = victim.components.health.destroytime or 2
            local x, y, z = victim.Transform:GetWorldPosition()
            local scale = (victim:HasTag("smallcreature") and smallScale)
                        or (victim:HasTag("largecreature") and largeScale)
                        or medScale
					inst:DoTaskInTime(time, spawnspirit, x, y, z, scale)
					end end 	
							  if ( victim:HasTag("deerclops") or 
							  victim:HasTag("bearger") or
							  victim:HasTag("moose") or 
							  victim:HasTag("dragonfly")  ) then
    local delta = victim.components.combat.defaultdamage * 0.1
		   if math.random() <= 1 then
    				inst.level = inst.level + 25
 			inst.components.sanity:DoDelta(delta)
			inst.components.talker:Say("Lightning kill \nLucky [EXP](+25) \n".. (inst.level))		
			
            local time = victim.components.health.destroytime or 2
            local x, y, z = victim.Transform:GetWorldPosition()
            local scale = (victim:HasTag("smallcreature") and smallScale)
                        or (victim:HasTag("largecreature") and largeScale)
                        or medScale
					inst:DoTaskInTime(time, spawnspirit, x, y, z, scale)
					end 
					end 	
					inst:RemoveEventCallback("killed", onkilll2)
				end 

--active lightning strike
local function on_hitLightnings_9(inst, data)
local other = data.target
if other and other.components.health and inst.level <=430  then
SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
SpawnPrefab("groundpoundring_fx").Transform:SetPosition(other:GetPosition():Get())
other.components.health:DoDelta(-40)
inst.components.sanity:DoDelta(-6)
inst.components.combat:SetRange(2)
other.burn = false
inst.AnimState:SetBloomEffectHandle( "" )
inst.switch = false
inst:RemoveEventCallback("onhitother", on_hitLightnings_9)
if inst.frost and other.components.freezable then
other.components.freezable:AddColdness(10)
other.components.health:DoDelta(-5)
elseif inst.fire and other.components.burnable then
other.components.burnable:Ignite()
other.components.health:DoDelta(-12)
end
elseif other and other.components.health and inst.level > 430 and inst.level <= 1880  then
SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
SpawnPrefab("groundpoundring_fx").Transform:SetPosition(other:GetPosition():Get())
other.components.health:DoDelta(-60)
inst.components.sanity:DoDelta(-8)
inst.components.combat:SetRange(2)
other.burn = false
inst.AnimState:SetBloomEffectHandle( "" )
inst.switch = false
inst:RemoveEventCallback("onhitother", on_hitLightnings_9)
if inst.frost and other.components.freezable then
other.components.freezable:AddColdness(20)
other.components.health:DoDelta(-10)
elseif inst.fire and other.components.burnable then
other.components.burnable:Ignite()
other.components.health:DoDelta(-24)
end
elseif other and other.components.health and inst.level > 1880 and inst.level <= 6999  then
SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
SpawnPrefab("groundpoundring_fx").Transform:SetPosition(other:GetPosition():Get())
other.components.health:DoDelta(-80)
inst.components.sanity:DoDelta(-10)
inst.components.combat:SetRange(2)
other.burn = false
inst.AnimState:SetBloomEffectHandle( "" )
inst.switch = false  
inst:RemoveEventCallback("onhitother", on_hitLightnings_9)
if inst.frost and other.components.freezable then
other.components.freezable:AddColdness(30)
other.components.health:DoDelta(-15)
elseif inst.fire and other.components.burnable then
other.components.burnable:Ignite()
other.components.health:DoDelta(-36)
end
elseif other and other.components.health and inst.level >= 7000  then
SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
SpawnPrefab("groundpoundring_fx").Transform:SetPosition(other:GetPosition():Get())
other.components.health:DoDelta(-100)
inst.components.sanity:DoDelta(-12)
inst.components.combat:SetRange(2)
other.burn = false
inst.AnimState:SetBloomEffectHandle( "" )
inst.switch = false  
inst:RemoveEventCallback("onhitother", on_hitLightnings_9)
if inst.frost and other.components.freezable then
other.components.freezable:AddColdness(40)
other.components.health:DoDelta(-20)
elseif inst.fire and other.components.burnable then
other.components.burnable:Ignite()
other.components.health:DoDelta(-48)
end
end 
if other.components.burnable and other.components.burnable:IsBurning() then
        other.components.burnable:Extinguish()
end
end
  

--power lightning

local function Lightning_a(inst)
if inst.stamina  >= 30 and not inst.components.health:IsDead() and not inst.active_valkyrie and not inst.switch and inst.valkyrie_on and not inst.besesrk then
inst.active_valkyrie = true
--inst:DoTaskInTime( 60, function() inst.active_valkyrie = false SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get()) end) 
elseif inst.stamina <= 30 and inst.valkyrie_on then
inst.components.talker:Say("Musha can not concentrate too sleepy..\nnot enough [sleep]")
inst.AnimState:SetBloomEffectHandle( "" )
inst.SoundEmitter:PlaySound("dontstarve/common/fireOut")
inst.switch = false
inst.active_valkyrie = false
 
end
if inst.stamina  >= 30 and not inst.switch and inst.components.sanity.current >= 15 and inst.level < 430  and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and not ( inst.vl1 or inst.vl2 or inst.vl3 or inst.vl4 or inst.vl5 or inst.vl6 or inst.vl7 or inst.vl8) and inst.valkyrie_on then
inst.components.combat:SetRange(9)
inst:ListenForEvent("onhitother", on_hitLightnings_9)
inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.switch = true
inst.components.sanity:DoDelta(-6)
if not inst.berserk then
inst.components.talker:Say("Valkyrie!") end
elseif inst.stamina  >= 30 and not inst.switch and inst.components.sanity.current >= 15 and inst.level >= 430 and inst.level < 1880  and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and not ( inst.vl1 or inst.vl2 or inst.vl3 or inst.vl4 or inst.vl5 or inst.vl6 or inst.vl7 or inst.vl8) and inst.valkyrie_on then
inst.components.combat:SetRange(11)
inst:ListenForEvent("onhitother", on_hitLightnings_9)
inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.switch = true
inst.components.sanity:DoDelta(-8)
if not inst.berserk then
inst.components.talker:Say("Valkyrie!") end
elseif inst.stamina  >= 30 and not inst.switch and inst.components.sanity.current >= 15 and inst.level >= 1880 and inst.level < 7000  and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and not ( inst.vl1 or inst.vl2 or inst.vl3 or inst.vl4 or inst.vl5 or inst.vl6 or inst.vl7 or inst.vl8) and inst.valkyrie_on then
inst.components.combat:SetRange(13)
inst:ListenForEvent("onhitother", on_hitLightnings_9)
inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.switch = true
inst.components.sanity:DoDelta(-10)
if not inst.berserk then
inst.components.talker:Say("Valkyrie!") end
elseif inst.stamina  >= 30 and not inst.switch and inst.components.sanity.current >= 15 and inst.level >= 7000 and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and not ( inst.vl1 or inst.vl2 or inst.vl3 or inst.vl4 or inst.vl5 or inst.vl6 or inst.vl7 or inst.vl8) and inst.valkyrie_on then
inst.components.combat:SetRange(15)
inst:ListenForEvent("onhitother", on_hitLightnings_9)
inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.switch = true
inst.components.sanity:DoDelta(-12)
if not inst.berserk then
inst.components.talker:Say("Valkyrie!") end
elseif not inst.switch and not inst.components.health:IsDead() and ( inst.vl1 or inst.vl2 or inst.vl3 or inst.vl4 or inst.vl5 or inst.vl6 or inst.vl7 or inst.vl8) and inst.valkyrie_on then
inst.components.combat:SetRange(2)
inst:RemoveEventCallback("onhitother", on_hitLightnings_9)
inst:RemoveEventCallback("killed", onkilll2)
inst.AnimState:SetBloomEffectHandle( "" )
inst.SoundEmitter:PlaySound("dontstarve/common/fireOut")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.switch = false
inst.active_valkyrie = false
inst.components.sanity:DoDelta(4)
elseif inst.switch and not inst.components.health:IsDead() and inst.valkyrie_on then
inst.components.combat:SetRange(2)
inst:RemoveEventCallback("onhitother", on_hitLightnings_9)
inst:RemoveEventCallback("killed", onkilll2)
inst.AnimState:SetBloomEffectHandle( "" )
inst.SoundEmitter:PlaySound("dontstarve/common/fireOut")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.switch = false
inst.active_valkyrie = false
inst.components.sanity:DoDelta(4)
elseif not inst.switch and inst.components.sanity.current < 15 and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.valkyrie_on then
inst.components.talker:Say("Not enough [Sanity]!")
elseif not inst.switch and inst:HasTag("playerghost") then
inst.components.talker:Say("Ooooh ! ohhoooho !!")
elseif inst.switch and inst:HasTag("playerghost") then
inst.components.talker:Say("Ooooh ? ooohhh ??")
end 
inst:ListenForEvent("killed", onkilll)
if not inst.valkyrie_on then
inst.components.talker:Say("Hmm? [Valkyrie]..?")
end
local x,y,z = inst.Transform:GetWorldPosition()
local ents = TheSim:FindEntities(x,y,z, 40, {"yamcheb"})
for k,v in pairs(ents) do
if inst.components.leader:IsFollower(v) and not inst.switch then
v.yamche_lightning = false
elseif inst.components.leader:IsFollower(v) and inst.switch then
v.yamche_lightning = true
end end
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end 
AddModRPCHandler("musha", "Lightning_a", Lightning_a)

--shield
--active shield
local function shield_go(inst, attacked, data)
if not inst.components.health:IsDead() and inst.level < 430 and not inst.activec0 and inst.shield_level1 then
--inst.components.talker:Say("Shield on!!")
inst.components.health:SetAbsorptionAmount(1)
local fx = SpawnPrefab("forcefieldfxx")
inst.on_sparkshield = true
inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/raise")
inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/pop")
fx.entity:SetParent(inst.entity)
fx.Transform:SetScale(0.8, 0.8, 0.8)
fx.Transform:SetPosition(0, 0.2, 0)
local fx_hitanim = function()
fx.AnimState:PlayAnimation("hit")
fx.AnimState:PushAnimation("idle_loop")
end
fx:ListenForEvent("blocked", fx_hitanim, inst)
inst.activec0 = true
inst.timec1 = true
inst:DoTaskInTime(--[[Duration]] 4, function()
fx:RemoveEventCallback("blocked", fx_hitanim, inst)
if inst:IsValid() then
fx.kill_fx(fx)
--inst.components.health:SetAbsorptionAmount(0)
inst.components.talker:Say("[Shield] cooldown -[180sec]")
inst.on_sparkshield = false
inst.components.health:SetAbsorptionAmount(0)
if not inst.berserk and not inst.valkyrie then
	inst.components.health:SetAbsorptionAmount(0)
elseif inst.berserk and inst.berserk_armor_1 then
	inst.components.health:SetAbsorptionAmount(0.15)
elseif inst.berserk and inst.berserk_armor_2 then
	inst.components.health:SetAbsorptionAmount(0.3)	
elseif inst.berserk and inst.berserk_armor_3 then
	inst.components.health:SetAbsorptionAmount(0.45)
elseif inst.valkyrie and inst.valkyrie_armor_1 then
	inst.components.health:SetAbsorptionAmount(0.06)	
elseif inst.valkyrie and inst.valkyrie_armor_2 then
	inst.components.health:SetAbsorptionAmount(0.12)	
elseif inst.valkyrie and inst.valkyrie_armor_3 then
	inst.components.health:SetAbsorptionAmount(0.18)	
elseif inst.valkyrie and inst.valkyrie_armor_4 then
	inst.components.health:SetAbsorptionAmount(0.24)	
end	
inst:DoTaskInTime(--[[Cooldown]] 180, function() inst.activec0 = false inst.timec1 = false end)
end end) 
end 
if not inst.components.health:IsDead() and inst.level >= 430 and inst.level < 1880 and not inst.activec0 and inst.shield_level2 then
--inst.components.talker:Say("Shield on!!")
inst.components.health:SetAbsorptionAmount(1)
local fx = SpawnPrefab("forcefieldfxx")
inst.on_sparkshield = true
inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/raise")
inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/pop")
fx.entity:SetParent(inst.entity)
fx.Transform:SetScale(0.8, 0.8, 0.8)
fx.Transform:SetPosition(0, 0.2, 0)
local fx_hitanim = function()
fx.AnimState:PlayAnimation("hit")
fx.AnimState:PushAnimation("idle_loop")
end
fx:ListenForEvent("blocked", fx_hitanim, inst)
inst.activec0 = true
inst.timec2 = true
inst:DoTaskInTime(--[[Duration]] 5, function()
fx:RemoveEventCallback("blocked", fx_hitanim, inst)
if inst:IsValid() then
fx.kill_fx(fx)
--inst.components.health:SetAbsorptionAmount(0)
inst.components.talker:Say("[Shield] cooldown -[140sec]")
inst.on_sparkshield = false
inst.components.health:SetAbsorptionAmount(0)
if not inst.berserk and not inst.valkyrie then
	inst.components.health:SetAbsorptionAmount(0)
elseif inst.berserk and inst.berserk_armor_1 then
	inst.components.health:SetAbsorptionAmount(0.15)
elseif inst.berserk and inst.berserk_armor_2 then
	inst.components.health:SetAbsorptionAmount(0.3)	
elseif inst.berserk and inst.berserk_armor_3 then
	inst.components.health:SetAbsorptionAmount(0.45)
elseif inst.valkyrie and inst.valkyrie_armor_1 then
	inst.components.health:SetAbsorptionAmount(0.06)	
elseif inst.valkyrie and inst.valkyrie_armor_2 then
	inst.components.health:SetAbsorptionAmount(0.12)	
elseif inst.valkyrie and inst.valkyrie_armor_3 then
	inst.components.health:SetAbsorptionAmount(0.18)	
elseif inst.valkyrie and inst.valkyrie_armor_4 then
	inst.components.health:SetAbsorptionAmount(0.24)	
end	
inst:DoTaskInTime(--[[Cooldown]] 140, function() inst.activec0 = false inst.timec2 = false end)
end end) 
end 
if not inst.components.health:IsDead() and inst.level >= 1880 and inst.level < 7000 and not inst.activec0 and inst.shield_level3 then
--inst.components.talker:Say("Shield on!!")
inst.components.health:SetAbsorptionAmount(1)
local fx = SpawnPrefab("forcefieldfxx")
inst.on_sparkshield = true
inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/raise")
inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/pop")
fx.entity:SetParent(inst.entity)
fx.Transform:SetScale(0.8, 0.8, 0.8)
fx.Transform:SetPosition(0, 0.2, 0)
local fx_hitanim = function()
fx.AnimState:PlayAnimation("hit")
fx.AnimState:PushAnimation("idle_loop")
end
fx:ListenForEvent("blocked", fx_hitanim, inst)
inst.activec0 = true
inst.timec3 = true
inst:DoTaskInTime(--[[Duration]] 6, function()
fx:RemoveEventCallback("blocked", fx_hitanim, inst)
if inst:IsValid() then
fx.kill_fx(fx)
--inst.components.health:SetAbsorptionAmount(0)
inst.components.talker:Say("[Shield] cooldown -[100sec]")
inst.on_sparkshield = false
inst.components.health:SetAbsorptionAmount(0)
if not inst.berserk and not inst.valkyrie then
	inst.components.health:SetAbsorptionAmount(0)
elseif inst.berserk and inst.berserk_armor_1 then
	inst.components.health:SetAbsorptionAmount(0.15)
elseif inst.berserk and inst.berserk_armor_2 then
	inst.components.health:SetAbsorptionAmount(0.3)	
elseif inst.berserk and inst.berserk_armor_3 then
	inst.components.health:SetAbsorptionAmount(0.45)
elseif inst.valkyrie and inst.valkyrie_armor_1 then
	inst.components.health:SetAbsorptionAmount(0.06)	
elseif inst.valkyrie and inst.valkyrie_armor_2 then
	inst.components.health:SetAbsorptionAmount(0.12)	
elseif inst.valkyrie and inst.valkyrie_armor_3 then
	inst.components.health:SetAbsorptionAmount(0.18)	
elseif inst.valkyrie and inst.valkyrie_armor_4 then
	inst.components.health:SetAbsorptionAmount(0.24)	
end	
inst:DoTaskInTime(--[[Cooldown]] 100, function() inst.activec0 = false inst.timec3 = false end)
end end) 
end 
if not inst.components.health:IsDead() and inst.level >= 7000 and not inst.activec0 and inst.shield_level4 then
--inst.components.talker:Say("Shield on!!")
inst.components.health:SetAbsorptionAmount(1)
local fx = SpawnPrefab("forcefieldfxx")
inst.on_sparkshield = true
inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/raise")
inst.SoundEmitter:PlaySound("dontstarve/creatures/chester/pop")
fx.entity:SetParent(inst.entity)
fx.Transform:SetScale(0.8, 0.8, 0.8)
fx.Transform:SetPosition(0, 0.2, 0)
local fx_hitanim = function()
fx.AnimState:PlayAnimation("hit")
fx.AnimState:PushAnimation("idle_loop")
end
fx:ListenForEvent("blocked", fx_hitanim, inst)
inst.activec0 = true
inst.timec4 = true
inst:DoTaskInTime(--[[Duration]] 7, function()
fx:RemoveEventCallback("blocked", fx_hitanim, inst)
if inst:IsValid() then
fx.kill_fx(fx)
--inst.components.health:SetAbsorptionAmount(0)
inst.components.talker:Say("[Shield] cooldown -[60sec]")
inst.on_sparkshield = false
inst.components.health:SetAbsorptionAmount(0)
if not inst.berserk and not inst.valkyrie then
	inst.components.health:SetAbsorptionAmount(0)
elseif inst.berserk and inst.berserk_armor_1 then
	inst.components.health:SetAbsorptionAmount(0.15)
elseif inst.berserk and inst.berserk_armor_2 then
	inst.components.health:SetAbsorptionAmount(0.3)	
elseif inst.berserk and inst.berserk_armor_3 then
	inst.components.health:SetAbsorptionAmount(0.45)
elseif inst.valkyrie and inst.valkyrie_armor_1 then
	inst.components.health:SetAbsorptionAmount(0.06)	
elseif inst.valkyrie and inst.valkyrie_armor_2 then
	inst.components.health:SetAbsorptionAmount(0.12)	
elseif inst.valkyrie and inst.valkyrie_armor_3 then
	inst.components.health:SetAbsorptionAmount(0.18)	
elseif inst.valkyrie and inst.valkyrie_armor_4 then
	inst.components.health:SetAbsorptionAmount(0.24)	
end	
inst:DoTaskInTime(--[[Cooldown]] 60, function() inst.activec0 = false inst.timec4 = false end)
end end) 
end 
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end

---
local function shieldgo(inst)
if not inst.activec0 and not inst.timec1 and inst.level < 430  then
inst.components.talker:Say("Full charged [Shield]")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.timec1 = true
elseif not inst.activec0 and not inst.timec2 and inst.level >= 430 and inst.level < 1880  then
inst.components.talker:Say("Full charged [Shield]")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.timec2 = true
elseif not inst.activec0 and not inst.timec3 and inst.level >= 1880 and inst.level < 7000  then
inst.components.talker:Say("Full charged [Shield]")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.timec3 = true
elseif not inst.activec0 and not inst.timec4 and inst.level >= 7000 then
inst.components.talker:Say("Full charged [Shield]")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
inst.timec4 = true
end 
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end
local function on_shield_act(inst)
inst:ListenForEvent("hungerdelta", shieldgo )
inst:ListenForEvent("attacked", Sparkshield_1)
if inst.level < 430 then
inst.shield_level1 = true
elseif inst.level >= 430 and inst.level < 1880 then
inst.shield_level2 = true
inst.shield_level1 = false
elseif inst.level >= 1880 and inst.level < 7000 then
inst.shield_level3 = true
inst.shield_level2 = false
inst.shield_level1 = false
elseif inst.level >= 7000 then
inst.shield_level4 = true
inst.shield_level3 = false
inst.shield_level2 = false
inst.shield_level1 = false
end
if inst.level < 430 and not inst.activec0 and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.stamina  >= 30 then
inst.components.talker:Say("Shield on!!")
shield_go(inst)
elseif inst.level >= 430 and inst.level < 1880  and not inst.activec0 and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.stamina  >= 30 then
inst.components.talker:Say("Shield on!!")
shield_go(inst)
elseif inst.level >= 1880 and inst.level < 7000  and not inst.activec0 and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.stamina  >= 30 then
inst.components.talker:Say("Shield on!!")
shield_go(inst)
elseif inst.level >= 7000 and not inst.activec0 and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.stamina  >= 30 then
inst.components.talker:Say("Shield on!!")
shield_go(inst)
elseif not inst:HasTag("playerghost") and inst.stamina <= 30 then
inst.components.talker:Say("Musha can not concentrate too sleepy..\nnot enough [sleep]")

elseif inst.activec0 and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("[Shield] is not yet charged.")
inst.shield_level1 = false
inst.shield_level2 = false
inst.shield_level3 = false
inst.shield_level4 = false
elseif inst.activec0 and inst:HasTag("playerghost") then
inst.components.talker:Say("Oooohhhhh.!!")
elseif not inst.activec0 and inst:HasTag("playerghost") then
inst.components.talker:Say("Ooooh....")
end
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end
  
 
AddModRPCHandler("musha", "on_shield_act", on_shield_act)

local function on_music_act1(inst)
if inst.components.playercontroller then

inst.components.playercontroller:Enable(false)
inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
local flute = inst.sg:GoToState("play_flute")
inst.AnimState:PlayAnimation("flute")
inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband")
inst.music = inst.music * 0
inst.entity:AddLight()
inst.Light:SetRadius(4)
inst.Light:SetFalloff(.8)
inst.Light:SetIntensity(.8)
inst.Light:SetColour(150/255,150/255,150/255)
inst.components.health:SetAbsorptionAmount(1)
inst.music_armor = true inst.switlight = false inst.nsleep = true
inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE*5)
scheduler:ExecuteInTime(3, function() inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") inst.sg:GoToState("play_flute")
scheduler:ExecuteInTime(3, function() inst.sg:GoToState("enter_onemanband")
scheduler:ExecuteInTime(3, function() inst.components.playercontroller:Enable(true) inst.AnimState:SetBloomEffectHandle("") inst.Light:Enable(true) inst.components.sanity:DoDelta(25) inst.components.talker:Say("[Light Aura] -on !") inst.nsleep = false  SpawnPrefab("statue_transition_2").Transform:SetPosition(inst:GetPosition():Get()) SpawnPrefab("ghosthound2").Transform:SetPosition(inst:GetPosition():Get())  inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE) inst.components.sanity:DoDelta(30) inst.music_armor = false scheduler:ExecuteInTime(180, function() inst.components.talker:Say("[Light Aura] -off ") inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") SpawnPrefab("statue_transition_2").Transform:SetPosition(inst:GetPosition():Get()) inst.components.sanityaura.aura = 0 inst.Light:SetRadius(0.5) scheduler:ExecuteInTime(5, function() inst.Light:Enable(false) inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") end) end)end)end)end)
end
end 
local function on_music_act2(inst)
if inst.components.playercontroller then

inst.components.playercontroller:Enable(false)
inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
local flute = inst.sg:GoToState("play_flute")
inst.AnimState:PlayAnimation("flute")
inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband")
inst.music = inst.music * 0
inst.entity:AddLight()
inst.Light:SetRadius(4)
inst.Light:SetFalloff(.8)
inst.Light:SetIntensity(.8)
inst.Light:SetColour(150/255,150/255,150/255)
inst.components.health:SetAbsorptionAmount(1)
inst.music_armor = true inst.switlight = false inst.nsleep = true
inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE*5)
scheduler:ExecuteInTime(3, function() inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") inst.sg:GoToState("play_flute")
scheduler:ExecuteInTime(3, function() inst.sg:GoToState("enter_onemanband")scheduler:ExecuteInTime(3, function() inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") inst.sg:GoToState("play_flute")
scheduler:ExecuteInTime(3, function() inst.components.playercontroller:Enable(true) inst.AnimState:SetBloomEffectHandle("") inst.Light:Enable(true) inst.components.sanity:DoDelta(25) inst.components.talker:Say("[Light Aura] -on !") inst.nsleep = false  SpawnPrefab("statue_transition_2").Transform:SetPosition(inst:GetPosition():Get()) SpawnPrefab("ghosthound").Transform:SetPosition(inst:GetPosition():Get())  inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE) inst.components.sanity:DoDelta(30) inst.music_armor = false scheduler:ExecuteInTime(180, function() inst.components.talker:Say("[Light Aura] -off ") inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") SpawnPrefab("statue_transition_2").Transform:SetPosition(inst:GetPosition():Get()) inst.components.sanityaura.aura = 0 inst.Light:SetRadius(0.5) scheduler:ExecuteInTime(5, function() inst.Light:Enable(false) inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") end) end)end)end)end)end)
end
end 
local function on_music_act3(inst)
if inst.components.playercontroller then

inst.components.playercontroller:Enable(false)
inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
local flute = inst.sg:GoToState("play_flute")
inst.AnimState:PlayAnimation("flute")
inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband")
inst.music = inst.music * 0
inst.entity:AddLight()
inst.Light:SetRadius(4)
inst.Light:SetFalloff(.8)
inst.Light:SetIntensity(.8)
inst.Light:SetColour(150/255,150/255,150/255)
inst.components.health:SetAbsorptionAmount(1)
inst.music_armor = true inst.switlight = false inst.nsleep = true
inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE*5)
scheduler:ExecuteInTime(3, function() inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") inst.sg:GoToState("play_flute")
scheduler:ExecuteInTime(3, function() inst.sg:GoToState("enter_onemanband")scheduler:ExecuteInTime(3, function() inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") inst.sg:GoToState("play_flute")scheduler:ExecuteInTime(3, function() inst.sg:GoToState("enter_onemanband")
scheduler:ExecuteInTime(3, function() inst.components.playercontroller:Enable(true) inst.AnimState:SetBloomEffectHandle("") inst.Light:Enable(true) inst.components.sanity:DoDelta(25) inst.components.talker:Say("[Light Aura] -on !") inst.nsleep = false  SpawnPrefab("statue_transition_2").Transform:SetPosition(inst:GetPosition():Get()) SpawnPrefab("shadowmusha").Transform:SetPosition(inst:GetPosition():Get())  inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE) inst.components.sanity:DoDelta(30) inst.music_armor = false scheduler:ExecuteInTime(180, function() inst.components.talker:Say("[Light Aura] -off ") inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") SpawnPrefab("statue_transition_2").Transform:SetPosition(inst:GetPosition():Get()) inst.components.sanityaura.aura = 0 inst.Light:SetRadius(0.5) scheduler:ExecuteInTime(5, function() inst.Light:Enable(false) inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") end) end)end)end)end)end)end)
end
end 
local function on_music_act4(inst)
if inst.components.playercontroller then

inst.components.playercontroller:Enable(false)
inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
local flute = inst.sg:GoToState("play_flute")
inst.AnimState:PlayAnimation("flute")
inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband")
inst.music = inst.music * 0
inst.entity:AddLight()
inst.Light:SetRadius(4)
inst.Light:SetFalloff(.8)
inst.Light:SetIntensity(.8)
inst.Light:SetColour(150/255,150/255,150/255)
inst.components.health:SetAbsorptionAmount(1)
inst.music_armor = true inst.switlight = false inst.nsleep = true
inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE*5)
scheduler:ExecuteInTime(3, function() inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") inst.sg:GoToState("play_flute")
scheduler:ExecuteInTime(3, function() inst.sg:GoToState("enter_onemanband")scheduler:ExecuteInTime(3, function() inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") inst.sg:GoToState("play_flute")scheduler:ExecuteInTime(3, function() inst.sg:GoToState("enter_onemanband")
scheduler:ExecuteInTime(3, function() inst.components.playercontroller:Enable(true) inst.AnimState:SetBloomEffectHandle("") inst.Light:Enable(true) inst.components.sanity:DoDelta(25) inst.components.talker:Say("[Light Aura] -on !") inst.nsleep = false  SpawnPrefab("statue_transition_2").Transform:SetPosition(inst:GetPosition():Get()) SpawnPrefab("tentacle_frost").Transform:SetPosition(inst:GetPosition():Get())  inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE) inst.components.sanity:DoDelta(30) inst.music_armor = false scheduler:ExecuteInTime(180, function() inst.components.talker:Say("[Light Aura] -off ") inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") SpawnPrefab("statue_transition_2").Transform:SetPosition(inst:GetPosition():Get()) inst.components.sanityaura.aura = 0 inst.Light:SetRadius(0.5) scheduler:ExecuteInTime(5, function() inst.Light:Enable(false) inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") end) end)end)end)end)end)end)
end
end 
local function on_music_act0(inst)
if inst.components.playercontroller then

inst.components.playercontroller:Enable(false)
inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
local flute = inst.sg:GoToState("play_flute")
inst.AnimState:PlayAnimation("flute")
inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband")
inst.music = inst.music * 0
inst.entity:AddLight()
inst.Light:SetRadius(4)
inst.Light:SetFalloff(.8)
inst.Light:SetIntensity(.8)
inst.Light:SetColour(150/255,150/255,150/255)
inst.components.health:SetAbsorptionAmount(1)
inst.music_armor = true inst.switlight = false inst.nsleep = true
inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE)
inst.components.sanityaura.aura = (TUNING.SANITYAURA_HUGE*5)
scheduler:ExecuteInTime(3, function() inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") inst.sg:GoToState("play_flute")
scheduler:ExecuteInTime(3, function() inst.sg:GoToState("enter_onemanband")
scheduler:ExecuteInTime(3, function() inst.components.playercontroller:Enable(true) inst.AnimState:SetBloomEffectHandle("") inst.Light:Enable(true) inst.components.sanity:DoDelta(25) inst.components.talker:Say("[Light Aura] -on !") inst.nsleep = false    inst.components.sanity:DoDelta(30) inst.music_armor = false scheduler:ExecuteInTime(180, function() inst.components.talker:Say("[Light Aura] -off ") inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") SpawnPrefab("statue_transition_2").Transform:SetPosition(inst:GetPosition():Get()) inst.components.sanityaura.aura = 0 inst.Light:SetRadius(0.5) scheduler:ExecuteInTime(5, function() inst.Light:Enable(false) inst.SoundEmitter:PlaySound("dontstarve/common/fireOut") end) end)end)end)end)
end
end 

local function on_sleep(inst)
local bedroll = inst.sg:GoToState("bedroll")
local sleep = inst.sg:GoToState("sleeping")
--inst.sg:AddStateTag("sleeping")
inst.sg:AddStateTag("busy")
inst.AnimState:OverrideSymbol("swap_bedroll", "swap_bedroll_straw", "bedroll_straw")
inst.AnimState:PlayAnimation("bedroll")
scheduler:ExecuteInTime(2, function() inst.sleep_on = true end)
--inst.sleepheal = inst:DoPeriodicTask(5, function() onsleepheal(inst) end)
end 

local function on_wakeup(inst)
local wakeup = inst.sg:GoToState("wakeup")
inst.sleep_on = false
inst.tiny_sleep = false
if not inst.music_check then
--inst.sg.statemem.iswaking = true
inst.AnimState:PlayAnimation("wakeup")
inst.entity:AddLight()
inst.Light:SetRadius(1)
inst.Light:SetFalloff(.8)
inst.Light:SetIntensity(.8)
inst.Light:SetColour(150/255,150/255,150/255)
inst.components.health:SetAbsorptionAmount(1)
inst.music_armor = true
inst.Light:Enable(true)
scheduler:ExecuteInTime(2.5, function() inst.Light:Enable(false) inst.music_armor = false end)
end
--if inst.sleepheal then inst.sleepheal:Cancel() inst.sleepheal = nil end
if inst.music_check then
--inst.sg.statemem.iswaking = true
inst.sg:GoToState("wakeup")
inst.entity:AddLight()
inst.Light:SetRadius(1)
inst.Light:SetFalloff(.8)
inst.Light:SetIntensity(.8)
inst.Light:SetColour(150/255,150/255,150/255)
inst.components.health:SetAbsorptionAmount(1)
inst.music_armor = true
inst.Light:Enable(true)
inst.AnimState:SetBloomEffectHandle( "" )
scheduler:ExecuteInTime(2, function() inst.components.playercontroller:Enable(false) inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" ) inst.sg:GoToState("enter_onemanband") inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband")
scheduler:ExecuteInTime(2, function() inst.components.talker:Say("Musha Ready to [performance]") inst.sg:GoToState("play_flute") inst.music_check = false inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") 
scheduler:ExecuteInTime(3, function() inst.Light:Enable(false) inst.music_armor = false inst.music_check = false inst.switlight = true inst.components.playercontroller:Enable(true) inst.AnimState:SetBloomEffectHandle( "" ) inst.SoundEmitter:PlaySound("dontstarve/wilson/onemanband") end) end) end)
end
end

local function on_buff_act(inst)
local performance0 = 1
local performance1 = 0.25
local performance2 = 0.2
local performance3 = 0.15
local performance4 = 0.1

if math.random() < performance1 and inst.switlight and not inst.sleep_on and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Now, look at me!\nPerformance[type1]")
on_music_act1(inst)
elseif math.random() < performance2 and inst.switlight and not inst.sleep_on and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Now, look at me!\nPerformance[type2]")
on_music_act2(inst)
elseif math.random() < performance3 and inst.switlight and not inst.sleep_on and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Now, look at me!\nPerformance[type3]")
on_music_act3(inst)
elseif math.random() < performance4 and inst.switlight and not inst.sleep_on and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Now, look at me!\nPerformance[type4]")
on_music_act4(inst)
elseif math.random() < performance1 and inst.switlight and not inst.sleep_on and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Now, look at me!\nPerformance[type1]")
on_music_act1(inst)
elseif math.random() < performance2 and inst.switlight and not inst.sleep_on and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Now, look at me!\nPerformance[type2]")
on_music_act2(inst)
elseif math.random() < performance3 and inst.switlight and not inst.sleep_on and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Now, look at me!\nPerformance[type3]")
on_music_act3(inst)
elseif math.random() < performance4 and inst.switlight and not inst.sleep_on and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Now, look at me!\nPerformance[type4]")
on_music_act4(inst)
elseif math.random() < performance0 and inst.switlight and not inst.sleep_on and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Now, look at me!\nPerformance[type0]")
on_music_act0(inst)

elseif not inst.switlight and not inst.components.health:IsDead() and not inst.sleep_on and not inst.nsleep and not inst:HasTag("playerghost") then
inst.components.talker:Say("Musha needs more sleep !")

elseif inst.components.health:IsDead() or inst:HasTag("playerghost") then
inst.components.talker:Say("Oooh Ohoooh..")
end
inst.components.talker.colour = Vector3(1, 0.85, 0.75, 1)
end

AddModRPCHandler("musha", "buff", on_buff_act)
 
 
 
 local function on_sleeping(inst)
 
if not inst.components.health:IsDead() and not inst.sleep_on and not inst.nsleep and not inst:HasTag("playerghost") then
if TheWorld.state.isday and not inst.tiny_sleep then
if inst.warm_on then
inst.components.talker:Say("Musha love the fire..\nbut sunny is too bright.")
end
if inst.stamina >=40 and not inst.warm_on then
inst.components.talker:Say("feel a little dizzy..")
elseif inst.stamina < 40 and inst.stamina >=25  and not inst.warm_on then
inst.components.talker:Say("Musha so tired..")
elseif inst.stamina < 25 and inst.stamina >=5  and not inst.warm_on then
inst.components.talker:Say("Musha fell asleep forever..")
elseif inst.stamina < 5  and not inst.warm_on then
inst.components.talker:Say("Musha, almost died..")
end
scheduler:ExecuteInTime(1, function() inst.sg:GoToState("knockout") inst.tiny_sleep = true end)
inst.sg:AddStateTag("busy")
elseif TheWorld.state.isday and inst.tiny_sleep then 
on_wakeup(inst)
end
end 
if not TheWorld.state.isday	and not inst.sleep_on and not inst.tiny_sleep and not inst.nsleep and not inst.warm_on then
if inst.stamina >=40 then
inst.components.talker:Say("feel dizzy..")
elseif inst.stamina < 40 and inst.stamina >=25 then
inst.components.talker:Say("Musha many tired..")
elseif inst.stamina < 25 and inst.stamina >=5 then
inst.components.talker:Say("Musha fell asleep forever..")
elseif inst.stamina < 5 then
inst.components.talker:Say("Musha, almost died..")
end
scheduler:ExecuteInTime(1, function() inst.sg:GoToState("knockout") inst.tiny_sleep = true end)
inst.sg:AddStateTag("busy")

elseif not TheWorld.state.isday	and not inst.sleep_on and not inst.tiny_sleep and not inst.nsleep and inst.warm_on and not inst.sleep_no then
if inst.stamina >= 95 then
inst.components.talker:Say("[Princess always sleeping.]")
elseif inst.stamina < 95 and inst.stamina >=75 then
inst.components.talker:Say("[Always sleepy..]")
elseif inst.stamina < 75 and inst.stamina >=70 then
inst.components.talker:Say("[Musha going to sing in a dream.]\ncheck time [perfomance]")
elseif inst.stamina < 70 then
inst.components.talker:Say("[Musha love the campfire.]\ncheck time [perfomance]")
end
on_sleep(inst)

elseif not TheWorld.state.isday	and not inst.sleep_on and not inst.tiny_sleep and not inst.nsleep and inst.warm_on and  inst.sleep_no then
local random1 = 0.2
if math.random() < random1 then
inst.components.talker:Say("Smells something danger.")
elseif math.random() < random1 then
inst.components.talker:Say("Musha feels Something wrong..")
elseif math.random() < random1 then
inst.components.talker:Say("This place is not my tomb.")
elseif math.random() < random1 then
inst.components.talker:Say("Wake up Musha. you can do it!")
elseif math.random() < 1 then
inst.components.talker:Say("No! wake up!")
end
elseif not inst.components.health:IsDead() and (inst.sleep_on or inst.tiny_sleep) and not inst.nsleep and not inst:HasTag("playerghost") then
on_wakeup(inst)

elseif inst.components.health:IsDead() or inst:HasTag("playerghost") then
inst.components.talker:Say("Oooh Ohoooh..")
end
inst.components.talker.colour = Vector3(1, 0.85, 0.75, 1)
end
 
 AddModRPCHandler("musha", "sleeping", on_sleeping)
 
 
local function yamche_update(inst)
local x,y,z = inst.Transform:GetWorldPosition()
local ents = TheSim:FindEntities(x,y,z, 25, {"yamcheb"})
for k,v in pairs(ents) do
if inst.follow and v.components.follower and not v.components.follower.leader and not inst.components.leader:IsFollower(v) and inst.components.leader:CountFollowers("yamcheb") == 0 and not v.hat then
inst.components.leader:AddFollower(v)
--on_yamche(inst)
v.yamche = true
v.sleepn = false 
v.fightn = false

--off_yamche(inst)
elseif not inst.follow and v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and inst.components.leader:CountFollowers("yamcheb") == 1 and not v.hat then
v.sleepn = true
v.yamche = true 
v.fightn = true
v.active_hunt = false
inst.components.leader:RemoveFollowersByTag("yamcheb")

if v.pick1 then
v.components.talker:Say("[Dropping collected stuffs]")
v.pick1 = false
v.working_food = false
end
end end end
 
local function order_yamche(inst)
if inst.yamche_follow and not inst.follow and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Yamche, Follow me!")
inst.follow = true
--master_yamche(inst)
yamche_update(inst)
elseif inst.yamche_follow and inst.follow and not inst.components.health:IsDead() and not inst:HasTag("playerghost") then
inst.components.talker:Say("Yamche, stay away")
inst.follow = false
inst.yamche_follow = false
--master_yamche(inst)
yamche_update(inst)
elseif not inst.yamche_follow and not inst:HasTag("playerghost") then
inst.components.talker:Say("Musha can't call Yamche..")
elseif inst.yamche_follow and not inst.follow and inst:HasTag("playerghost") then
inst.components.talker:Say("Fooolooowooomeeee...")
inst.follow = true
yamche_update(inst)
elseif inst.yamche_follow and inst.follow and inst:HasTag("playerghost") then
inst.components.talker:Say("Soooootaaaaayyyyyyyee...")
inst.follow = false
inst.yamche_follow = false
yamche_update(inst)
elseif not inst.yamche_follow and inst:HasTag("playerghost") then
inst.components.talker:Say("OOOoooooohhhhh...")

end
end 

AddModRPCHandler("musha","yamche", order_yamche)

--sneak attack --hide in shadow

local function on_Sneak_pang(inst, data)
local other = data.target
if other:HasTag("structure") or other:HasTag("wall") or other:HasTag("tentacle_pillar") then
	inst.components.sanity:DoDelta(80)
	inst.components.colourtweener:StartTween({1,1,1,1}, 0)
	inst.sneak_pang = false	
	inst.components.talker:Say("Successful Backstab !!\nStructure target : [Reduced damage]")
        other.components.health:DoDelta(-100)
	local dark1 = SpawnPrefab("statue_transition")
	local pos = Vector3(other.Transform:GetWorldPosition())
	dark1.Transform:SetPosition(pos:Get())
	dark1.Transform:SetScale(0.5,4,0.5)
	local fx = SpawnPrefab("explode_small")
	local pos = Vector3(other.Transform:GetWorldPosition())
	fx.Transform:SetPosition(pos:Get())
	inst:RemoveTag("notarget")
	inst:RemoveEventCallback("onhitother", on_Sneak_pang)
elseif not (other:HasTag("structure") or other:HasTag("wall") or other:HasTag("tentacle_pillar")) then
if inst.sneak_pang and not other.sg:HasStateTag("attack") and not other.sg:HasStateTag("shield") and not other.sg:HasStateTag("moving") and not other.sg:HasStateTag("frozen") then
	inst.components.sanity:DoDelta(80)
	inst.components.colourtweener:StartTween({1,1,1,1}, 0)
	inst.sneak_pang = false	
	inst.components.talker:Say("Successful Backstab !!")
        other.components.health:DoDelta(-400)
	local dark1 = SpawnPrefab("statue_transition")
	local pos = Vector3(other.Transform:GetWorldPosition())
	dark1.Transform:SetPosition(pos:Get())
	dark1.Transform:SetScale(0.5,4,0.5)
	local fx = SpawnPrefab("explode_small")
	local pos = Vector3(other.Transform:GetWorldPosition())
	fx.Transform:SetPosition(pos:Get())
	inst:RemoveTag("notarget")
	inst:RemoveEventCallback("onhitother", on_Sneak_pang)
elseif inst.sneak_pang and not other.sg:HasStateTag("attack") and not other.sg:HasStateTag("shield") and not other.sg:HasStateTag("moving") and other.sg:HasStateTag("frozen") then
	inst.components.sanity:DoDelta(80)
	inst.components.colourtweener:StartTween({1,1,1,1}, 0)
	inst.sneak_pang = false	
	inst.components.talker:Say("Successful Backstab !!\nFrozen target : [Reduced damage]")
        other.components.health:DoDelta(-100)
	local dark1 = SpawnPrefab("statue_transition")
	local pos = Vector3(other.Transform:GetWorldPosition())
	dark1.Transform:SetPosition(pos:Get())
	dark1.Transform:SetScale(0.5,4,0.5)
	local fx = SpawnPrefab("explode_small")
	local pos = Vector3(other.Transform:GetWorldPosition())
	fx.Transform:SetPosition(pos:Get())
	inst:RemoveTag("notarget")
	inst:RemoveEventCallback("onhitother", on_Sneak_pang)
elseif inst.sneak_pang and other.sg:HasStateTag("attack") or other.sg:HasStateTag("shield") or other.sg:HasStateTag("moving") then
	inst.components.colourtweener:StartTween({1,1,1,1}, 0)
	inst.sneak_pang = false 
	inst.components.talker:Say("Failed Backstab..")
	local fx = SpawnPrefab("splash")
	local pos = Vector3(other.Transform:GetWorldPosition())
	fx.Transform:SetPosition(pos:Get())
	inst:RemoveTag("notarget")
inst:RemoveEventCallback("onhitother", on_Sneak_pang)
	end
end 
end
local function hide_discorved(inst, data)
    if inst.sneak_pang then
	inst.components.colourtweener:StartTween({1,1,1,1}, 0)
	inst.sneak_pang = false		
	inst:RemoveTag("notarget")
	inst:RemoveEventCallback("onhitother", on_Sneak_pang)
	inst:RemoveEventCallback("attacked", hide_discorved)
	local fx = SpawnPrefab("statue_transition_2")
        fx.entity:SetParent(inst.entity)
	fx.Transform:SetScale(1.2, 1.2, 1.2)
        fx.Transform:SetPosition(0, 0, 0)
	inst.components.talker:Say("enemies found me!")
    end end

local function InShadow(inst, data)
local player = GLOBAL.ThePlayer
local x,y,z = inst.Transform:GetWorldPosition()	
local ents = TheSim:FindEntities(x, y, z, 15)
for k,v in pairs(ents) do
if v.components.combat and v.components.combat.target == inst and not (v:HasTag("berrythief") or v:HasTag("prey") or v:HasTag("bird") or v:HasTag("butterfly")) then
		v.components.combat.target = nil
 end
 end end

local function HideIn(inst)	
    if not inst.sneak_pang and inst.components.sanity.current >= 80 and inst.stamina >= 30 then
	inst.components.talker:Say("Hide in shadow !")
		 inst.components.sanity:DoDelta(-80)
			inst.sneak_pang = true  
				inst.components.colourtweener:StartTween({0.3,0.3,0.3,1}, 0)		
local fx = SpawnPrefab("statue_transition_2")
      fx.entity:SetParent(inst.entity)
	  fx.Transform:SetScale(1.2, 1.2, 1.2)
      fx.Transform:SetPosition(0, 0, 0)
		inst:AddTag("notarget")
inst:DoTaskInTime( 4, function() if inst.sneak_pang then inst.components.talker:Say("Musha is shadow !") SpawnPrefab("statue_transition").Transform:SetPosition(inst:GetPosition():Get()) inst.components.colourtweener:StartTween({0.1,0.1,0.1,1}, 0) InShadow(inst)  end	end)		
	inst:ListenForEvent("onhitother", on_Sneak_pang)
	inst:ListenForEvent("attacked", hide_discorved)
elseif not inst.sneak_pang and inst.components.sanity.current < 80 and inst.stamina >= 30 then
		inst.components.talker:Say("Not enough [Sanity]..")
elseif not inst.sneak_pang and inst.components.sanity.current > 80 and inst.stamina < 30 then
		inst.components.talker:Say("Musha needs more [Sleep]..")
elseif not inst.sneak_pang and inst.components.sanity.current < 80 and inst.stamina < 30 then
		inst.components.talker:Say("Musha realy tired..")	
elseif inst.sneak_pang then		
		inst.components.talker:Say("Unhide!")	
			inst.components.colourtweener:StartTween({1,1,1,1}, 0)
			local fx = SpawnPrefab("statue_transition_2")
      fx.entity:SetParent(inst.entity)
	  fx.Transform:SetScale(1.2, 1.2, 1.2)
      fx.Transform:SetPosition(0, 0, 0)
		inst.components.sanity:DoDelta(80)
	inst.sneak_pang = false		
	inst:RemoveTag("notarget")
	inst:RemoveEventCallback("onhitother", on_Sneak_pang)
	inst:RemoveEventCallback("attacked", hide_discorved)
end	end	
		
AddModRPCHandler("musha","shadows", HideIn)


--specific
local function BabyPostInit(inst)
	if not inst.components.characterspecific then
		inst:AddComponent("characterspecific")	end
	inst.components.characterspecific:SetOwner("musha")
	inst.components.characterspecific:SetStorable(true)
	inst.components.characterspecific:SetComment("Phoenix seems difficult to tame..")
	return inst
end
AddPrefabPostInit("musha_small", BabyPostInit)
-----------------------------------------------
local function visual_hold(inst)
if not inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and not inst.full_hold and not inst.normal_hold and not inst.berserk_hold and not inst.valkyrie_hold and not inst.hold_old1 and not inst.hold_old2 and not inst.hold_old3 and not inst.hold_old4 and not inst.hold_old5 and not inst.hold_old6 and not inst.hold_old7 and not inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[Full]")
inst.visual_hold = true

inst.full_hold = true
inst.normal_hold = false
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and not inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[Normal]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = false
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_normal")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and not inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[Valkyrie]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = false
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_battle")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and not inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[Berserk]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = false
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_hunger")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and not inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[old visual 1]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = false
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_old")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and not inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[old visual 2]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = false
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_normal_old")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and not inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[old visual 3]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = false
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_battle_old")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and not inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[old visual 4]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = false
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_hunger_old")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and not inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[old visual 5]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = false
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_full_p_old")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and not inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[old visual 6]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = false
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_normal_p_old")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and not inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[old visual 7]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = false
inst.AnimState:SetBuild("musha_battle_p_old")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and not inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - On] \nVisual:[old visual 8]")
inst.visual_hold = true
inst.full_hold = true
inst.normal_hold = true
inst.valkyrie_hold = true
inst.berserk_hold = true
inst.hold_old1 = true
inst.hold_old2 = true
inst.hold_old3 = true
inst.hold_old4 = true
inst.hold_old5 = true
inst.hold_old6 = true
inst.hold_old7 = true
inst.hold_old8 = true
inst.AnimState:SetBuild("musha_hunger_p_old")

elseif inst.visual_hold and not inst.components.health:IsDead() and not inst:HasTag("playerghost") and inst.full_hold and inst.normal_hold and inst.valkyrie_hold and inst.berserk_hold and inst.hold_old1 and inst.hold_old2 and inst.hold_old3 and inst.hold_old4 and inst.hold_old5 and inst.hold_old6 and inst.hold_old7 and inst.hold_old8 then
inst.components.talker:Say("[Visual Hold - Off] \nVisual:[Auto]")
inst.visual_hold = false
inst.full_hold = false
inst.normal_hold = false
inst.valkyrie_hold = false
inst.berserk_hold = false
inst.hold_old1 = false
inst.hold_old2 = false
inst.hold_old3 = false
inst.hold_old4 = false
inst.hold_old5 = false
inst.hold_old6 = false
inst.hold_old7 = false
inst.hold_old8 = false
end 
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end 
AddModRPCHandler("musha","visual_hold", visual_hold)

local function yamche2(inst)

--hunt --defense --avoid
local x,y,z = inst.Transform:GetWorldPosition()
local ents = TheSim:FindEntities(x,y,z, 30, {"musha_slave"})
for k,v in pairs(ents) do
if not inst.components.leader:IsFollower(v) and v:HasTag("yamche") and not inst:HasTag("playerghost") then
inst.components.talker:Say("He needs some rest..")
elseif v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and not v.peace and not v.active_hunt and not v.defense and not inst:HasTag("playerghost") then
v.yamche = true 
if v:HasTag("yamche") then
inst.components.talker:Say("Yamche, Lets hunt begin!")
--v.components.talker:Say("[Aggrasive]\nArmor:40")
v.components.talker:Say("[Aggrasive]")
v.peace = false
v.active_hunt = true
v.defense = false
end
elseif v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and not v.peace and v.active_hunt and not v.defense and inst.components.leader:IsFollower(v) and not inst:HasTag("playerghost") then
v.yamche = true 
if v:HasTag("yamche") then
inst.components.talker:Say("Yamche, Avoid battle!")
--v.components.talker:Say("[Avoidance]\nArmor:95")
v.components.talker:Say("[Avoidance]")
v.peace = true
v.active_hunt = false
v.defense = true
end
elseif v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and v.peace and not v.active_hunt and v.defense and inst.components.leader:IsFollower(v) and not inst:HasTag("playerghost") then
v.yamche = true 
if v:HasTag("yamche") then
inst.components.talker:Say("Yamche, Protect me!")
--v.components.talker:Say("[Defensive]\nArmor:60")
v.components.talker:Say("[Defensive]")
v.peace = false
v.active_hunt = false
v.defense = false
end
elseif v.components.follower and v.components.follower.leader and not v.peace and inst.components.leader:IsFollower(v) and inst:HasTag("playerghost") then
v.yamche = true 
if v:HasTag("yamche") then
inst.components.talker:Say("Yaaaam.. Dooooo nooooot faaaaaieet..")
v.components.talker:Say("[Avoidance]")
v.peace = true
v.active_hunt = false
v.defense = true
end
elseif v.components.follower and v.components.follower.leader and v.peace and inst.components.leader:IsFollower(v) and inst:HasTag("playerghost") then
inst.components.talker:Say("Oooooooh Ooooh")
v.yamche = true 
if v:HasTag("yamche") then
v.components.talker:Say("[Defensive]")
v.peace = false
v.active_hunt = false
v.defense = false
end
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end end end
AddModRPCHandler("musha","yamche2", yamche2)

local function yamche3(inst)
local x,y,z = inst.Transform:GetWorldPosition()
local ents = TheSim:FindEntities(x,y,z, 25, {"yamcheb"})
for k,v in pairs(ents) do
if not v.removinv then
if v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and not v.pick1 and not inst:HasTag("playerghost") then
inst.components.talker:Say("Yamche, Gathering stuffs.")
v.working_food = true
v.pick1 = true
v.drop = false
v.yamche = true 
if not v.light_on then
v.components.talker:Say("[Gathering Items]\nHungry-rate(x6)")
elseif v.light_on then
v.components.talker:Say("[Light on]+[Gathering Items]\nHungry-rate(x14)")
end
elseif v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and v.pick1 and not inst:HasTag("playerghost") then
inst.components.talker:Say("Stop to gathering")
v.working_food = false
v.pick1 = false
v.drop = true
if not v.light_on then
v.components.talker:Say("[Rest]\nHungry-rate(x1)")
elseif v.light_on then
v.components.talker:Say("[Light on]\nHungry-rate(x8)")
end
elseif not inst.components.leader:IsFollower(v) and not inst:HasTag("playerghost") and inst.components.leader:CountFollowers("yamcheb") == 0 then
inst.components.talker:Say("He need more resting..")
elseif v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and not v.pick1 and inst:HasTag("playerghost") then
inst.components.talker:Say("cooooooooleeecttt...")
v.working_food = true
v.pick1 = true
v.drop = false
v.yamche = true 
if not v.light_on then
v.components.talker:Say("[Gathering Items]\nHungry-rate(x6)")
elseif v.light_on then
v.components.talker:Say("[Light on]+[Gathering Items]\nHungry-rate(x14)")
end
elseif v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and v.pick1 and inst:HasTag("playerghost") then
inst.components.talker:Say("drooooooopeeeee...")
v.working_food = false
v.pick1 = false
v.drop = true
if not v.light_on then
v.components.talker:Say("[Rest]\nHungry-rate(x1)")
elseif v.light_on then
v.components.talker:Say("[Light on]\nHungry-rate(x8)")
end
end 
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end 
end
end

AddModRPCHandler("musha","yamche3", yamche3)

local function ydebug(inst)
local x,y,z = inst.Transform:GetWorldPosition()
local ents = TheSim:FindEntities(x,y,z, 25, {"yamcheb"})
for k,v in pairs(ents) do
v.components.talker:Say("[Drop-All]\nContainer Debug key")
if v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and not v.level1 and v.components.inventory and v.components.container and v.components.inventory then
v.components.container:DropEverything()
v.components.inventory:DropEverything(true)
end
end 
inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
end

AddModRPCHandler("musha","ydebug", ydebug)
------------------------------------------------
-------------comfortable health info
local healthinfoActive = 0
for _, moddir in ipairs(GLOBAL.KnownModIndex:GetModsToLoad()) do
    if GLOBAL.KnownModIndex:GetModInfo(moddir).name == "Health Info" then
		healthinfoActive = 1
   end end 

-------------------------------------------------
--yamche health/hunger info (thx health info)

if healthinfoActive == 0 then
AddClassPostConstruct("components/health_replica", function(self)
self.SetCurrent = function(self, current)
if self.inst.components and self.inst.components.health and self.inst.components.healthinfo_copy and self.inst.components.hungerinfo and self.inst:HasTag("yamcheb") then
---------
local str = self.inst.components.healthinfo_copy.text
if str ~= nil then
			local h = self.inst.components.health
			local mx = math.floor(h.maxhealth-h.minhealth)
			local cur = math.floor(h.currenthealth-h.minhealth)
			local h2 = self.inst.components.hunger
			local mx2 = math.floor(h2.max-h2.hungerrate)
			local cur2 = math.floor(h2.current-h2.hungerrate)
local i,j = string.find(str, " [", nil, true)
if i ~= nil and i > 1 then str = string.sub(str, 1, (i-1)) end
str = "["..math.floor(cur*100/mx).."%/"..math.floor(cur2*100/mx2).."%]"
if self.inst.components.healthinfo_copy and self.inst:HasTag("yamcheb") then
self.inst.components.healthinfo_copy:SetText(str)
end end end
if self.classified ~= nil then
self.classified:SetValue("currenthealth", current)
end end end)
--hover text
AddGlobalClassPostConstruct('widgets/hoverer', 'HoverText', function(self)
self.OnUpdate = function(self)
local using_mouse = self.owner.components and self.owner.components.playercontroller:UsingMouse()
if using_mouse ~= self.shown then
if using_mouse then
self:Show()
else
self:Hide()
end end
if not self.shown then
return
end
local str = nil
if self.isFE == false then
str = self.owner.HUD.controls:GetTooltip() or self.owner.components.playercontroller:GetHoverTextOverride()
else
str = self.owner:GetTooltip()
end
local secondarystr = nil
local lmb = nil
if not str and self.isFE == false then
lmb = self.owner.components.playercontroller:GetLeftMouseAction()
if lmb then
str = lmb:GetActionString()
if lmb.target and lmb.invobject == nil and lmb.target ~= lmb.doer then
local name = lmb.target:GetDisplayName() or (lmb.target.components.named and lmb.target.components.named.name)
if name then
local adjective = lmb.target:GetAdjective()
if adjective then
str = str.. " " .. adjective .. " " .. name
else
str = str.. " " .. name
end
if lmb.target.replica.stackable ~= nil and lmb.target.replica.stackable:IsStack() then
str = str .. " x" .. tostring(lmb.target.replica.stackable:StackSize())
end
if lmb.target.components.inspectable and lmb.target.components.inspectable.recordview and lmb.target.prefab then
GLOBAL.ProfileStatsSet(lmb.target.prefab .. "_seen", true)
end end end
if lmb.target and lmb.target ~= lmb.doer and lmb.target.components and lmb.target.components.healthinfo_copy and lmb.target.components.healthinfo_copy.text ~= '' then
local name = lmb.target:GetDisplayName() or (lmb.target.components.named and lmb.target.components.named.name) or ""
local i,j = string.find(str, " " .. name, nil, true)
if i ~= nil and i > 1 then str = string.sub(str, 1, (i-1)) end
str = str.. " " .. name .. " " .. lmb.target.components.healthinfo_copy.text
end end
local rmb = self.owner.components.playercontroller:GetRightMouseAction()
if rmb then
secondarystr = GLOBAL.STRINGS.RMB .. ": " .. rmb:GetActionString()
end end
if str then
if self.strFrames == nil then self.strFrames = 1 end
if self.str ~= self.lastStr then
--print("new string")
self.lastStr = self.str
self.strFrames = SHOW_DELAY
else
self.strFrames = self.strFrames - 1
if self.strFrames <= 0 then
if lmb and lmb.target and lmb.target:HasTag("player") then
self.text:SetColour(lmb.target.playercolour)
else
self.text:SetColour(1,1,1,1)
end
self.text:SetString(str)
self.text:Show()
end end
else
self.text:Hide()
end
if secondarystr then
YOFFSETUP = -80
YOFFSETDOWN = -50
self.secondarytext:SetString(secondarystr)
self.secondarytext:Show()
else
self.secondarytext:Hide()
end
local changed = (self.str ~= str) or (self.secondarystr ~= secondarystr)
self.str = str
self.secondarystr = secondarystr
if changed then
local pos = TheInput:GetScreenPosition()
self:UpdatePosition(pos.x, pos.y)
end end end)
end

--------------------------------
AddPrefabPostInitAny(function(inst)
	if inst.components.healthinfo_copy == nil and inst:HasTag("yamcheb") then
		inst:AddComponent("healthinfo_copy")
		inst:AddComponent("hungerinfo")
	if inst.components.health and inst.components.hunger then
			str = ""
			local h=inst.components.health
			local mx=math.floor(h.maxhealth-h.minhealth)
			local cur=math.floor(h.currenthealth-h.minhealth)
			local h2 = inst.components.hunger
			local mx2 = math.floor(h2.max-h2.hungerrate)
			local cur2 = math.floor(h2.current-h2.hungerrate)
			str = "["..math.floor(cur*100/mx).."%/"..math.floor(cur2*100/mx2).."%]"
			--str = "\nHealth: "..math.floor(cur*100/mx).."%\nHunger: "..math.floor(cur2*100/mx2).."%"
		inst.components.healthinfo_copy:SetText(str)
		end	end end)

-------------------------------------------------
---------------------------------------------------
  
-- sleep badge
---always on status comfortable
------------- (thx always status)

local AlwaysOnStatus = 0
for _, moddir in ipairs(GLOBAL.KnownModIndex:GetModsToLoad()) do
    if GLOBAL.KnownModIndex:GetModInfo(moddir).name == "Always On Status" then
		AlwaysOnStatus = 1
   end end

local function SleepnTired(inst)   
 if Sleep_Princess == 1 then
   inst.No_Sleep_Princess = true 
 elseif Sleep_Princess == 0 then
   inst.No_Sleep_Princess = false 
 elseif Sleep_Princess == 2 then
   inst.No_Sleep_Princess = false    
   end end
 AddPrefabPostInit("musha", SleepnTired)
 
 if Sleep_Princess == 0 then
 AddClassPostConstruct("widgets/badge", function(self)
local ThePlayer = GLOBAL.ThePlayer
if ThePlayer:HasTag("musha") then
	self:SetScale(.9,.9,.9)
	self.st = self:AddChild(Image("images/status_stamina.xml", "status_stamina.tex"))
	self.st:SetScale(0,.44,0)
	self.st:SetPosition(-.5,-40,0)

	self.num:SetFont(GLOBAL.NUMBERFONT)
	self.num:SetSize(28)
	
	self.num:MoveToFront()
	self.num:Show()

	self.maxnum = self:AddChild(Text(GLOBAL.NUMBERFONT, 33))
    self.maxnum:SetPosition(6, 0, 0)
	self.maxnum:MoveToFront()
    self.maxnum:Hide()
	
	local OldOnGainFocus = self.OnGainFocus
	function self:OnGainFocus()
		OldOnGainFocus(self)
		self.maxnum:Show()
	end

	local OldOnLoseFocus = self.OnLoseFocus
	function self:OnLoseFocus()
		OldOnLoseFocus(self)
		self.maxnum:Hide()
		self.num:Show()
	end
	
if AlwaysOnStatus == 0 then	
	self.num:SetScale(1,1,0)
	self.num:SetPosition(5, 0, 0)
	
	-- for health/hunger/sanity/beaverness
	local OldSetPercent = self.SetPercent
	if OldSetPercent then
		function self:SetPercent(val, max, ...)
			self.maxnum:SetString(tostring(math.ceil(max or 100)))
			OldSetPercent(self, val, max, ...)
		end
	end
	
	-- for moisture
	local OldSetValue = self.SetValue
	if OldSetValue then
		function self:SetValue(val, max, ...)
			self.maxnum:SetString(tostring(math.ceil(max)))
			OldSetValue(self, val, max, ...)
	end end end 
	end
end)

AddClassPostConstruct("widgets/statusdisplays",function(self,inst)
local ThePlayer = GLOBAL.ThePlayer
if ThePlayer:HasTag("musha") and IsServer then
		self.staminab = self:AddChild(Badge("status_stamina", self.owner))
		self.staminab.st:SetScale(0.9, 1.1, 1)
		self.staminab.num:SetScale(0.9, 0.7, 1)
		self.staminab.num:SetPosition(0, -10, 0)
if AlwaysOnStatus == 0 then
	self.staminab:SetPosition(-78, -40, 0)
elseif AlwaysOnStatus == 1 then	
	self.staminab:SetPosition(-78, -46, 0)
	end
	--------
	local function Updatesleep(sleepbadge)
	local player = ThePlayer
	
	local max_stamina = 100
	local min_stamina = 0
	local max_fatigue = 100
	local min_fatigue = 0	
	local max_music = 100
	local min_music = 0	
			local mx=math.floor(max_stamina-min_stamina)
			local cur=math.floor(inst.stamina-min_stamina)
			local mx2=math.floor(max_fatigue-min_fatigue)
			local cur2=math.floor(inst.fatigue-min_fatigue)
			local mxx=math.floor(max_music-min_music)
			local curr=math.floor(inst.music-min_music)
			sleep = ""..math.floor(cur*100/mx).."%"
			sleepy = ""..math.floor(cur2*100/mx2).."%"
			music = ""..math.floor(curr*100/mxx).."%"
			----------
			----------
	
	if sleepbadge == nil then
		sleepbadge = self.staminab.focus
			end
	----		
	if sleepbadge then 
		if IsServer then
			local x,y,z = inst.Transform:GetWorldPosition()
			local ents = TheSim:FindEntities(x,y,z, 25, {"yamcheb"})
			htr = ""
		for k,v in pairs(ents) do
	if v.components.follower and v.components.follower.leader and inst.components.leader:IsFollower(v) and inst.yamche_follow then
			local h=v.components.health
			local mx=math.floor(h.maxhealth-h.minhealth)
			local cur=math.floor(h.currenthealth-h.minhealth)
			local h2 = v.components.hunger
			local mx2 = math.floor(h2.max-h2.hungerrate)
			local cur2 = math.floor(h2.current-h2.hungerrate)
	htr = "Health: "..math.floor(cur*100/mx).."%\nHunger: "..math.floor(cur2*100/mx2).."%"
		self.staminab.num:SetString("[Yamche]\n" .. htr)
		elseif not inst.yamche_follow then
			self.staminab.num:SetString("[Yamche]\n resting.." )
		end end
			
		elseif not IsServer then
			--self.staminab.num:SetString("[badge]\ndoes not support\nthe client player\n(working on)")
			--self.staminab.num:SetString("[Yamche]\n" .. htr)
			end
			--self.staminab.num:SetString("Tired\n" .. sleepy)
			--self.staminab.num:SetString("Fatigue\n" .. (self.fatigue))
		else 
		if IsServer then
			self.staminab.num:SetString("Sleep/Tired\n" .. sleep .."/ " .. sleepy .."\nMusic :".. music)
		elseif not IsServer then
		--self.staminab.num:SetString("Sleep/Tired\nCheck(L)key")
		--self.staminab.num:SetString("Sleep/Tired\n" .. sleep .."/ " .. sleepy .."\nMusic :".. music)
		end	end
	end
	self.Updatesleep = self.inst:DoPeriodicTask(0, function() Updatesleep() end)

	end	
	end)
	end

-------------describe---------------
--tent
STRINGS.NAMES.TENT_MUSHA = "Musha's Tent"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.TENT_MUSHA = "Home sweet home"
STRINGS.RECIPE_TENT_MUSHA = "Leaves Tent"
STRINGS.RECIPE_DESC.TENT_MUSHA = "Tent,Guard,Lantern"


STRINGS.NAMES.MUSHA_EGG = "Phoenix Egg"
STRINGS.NAMES.MUSHA_EGG_CRACKED = "Phoenix Egg"
STRINGS.NAMES.MUSHA_EGGS1 = "Phoenix Egg(LV 2)"
STRINGS.NAMES.MUSHA_EGG_CRACKEDS1 = "Phoenix Egg"
STRINGS.NAMES.MUSHA_EGGS2 = "Phoenix Egg(LV 3)"
STRINGS.NAMES.MUSHA_EGG_CRACKEDS2 = "Phoenix Egg"
STRINGS.NAMES.MUSHA_EGGS3 = "Phoenix Egg(LV 4)"
STRINGS.NAMES.MUSHA_EGG_CRACKEDS3 = "Phoenix Egg"
STRINGS.NAMES.MUSHA_EGG1 = "Phoenix Egg(LV 5)"
STRINGS.NAMES.MUSHA_EGG_CRACKED1 = "Phoenix Egg"
STRINGS.NAMES.MUSHA_EGG2 = "Phoenix Egg(LV 6)"
STRINGS.NAMES.MUSHA_EGG_CRACKED2 = "Phoenix Egg"
STRINGS.NAMES.MUSHA_EGG3 = "Phoenix Egg(LV 7)"
STRINGS.NAMES.MUSHA_EGG_CRACKED3 = "Phoenix Egg"
STRINGS.NAMES.MUSHA_EGG8 = "Phoenix Egg(LV 8)"
STRINGS.NAMES.MUSHA_EGG_CRACKED8 = "Phoenix Egg"
STRINGS.NAMES.MUSHA_EGG = "Phoenix Egg"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG = "Phoenix Egg"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGGS1 = "Phoenix Egg(LV 2)"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGGS2 = "Phoenix Egg(LV 3)"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGGS3 = "Phoenix Egg(LV 4)"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG1 = "Phoenix Egg(LV 5)"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG2 = "Phoenix Egg(LV 6)"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG3 = "Phoenix Egg(LV 7)"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG8 = "Phoenix Egg(LV 8)"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG_CRACKED = "It is not mine. Musha said that is Yamche bud."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG_CRACKEDS1 = "It is not mine. Musha said that is Yamche bud."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG_CRACKEDS2 = "It is not mine. Musha said that is Yamche bud."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG_CRACKEDS3 = "It is not mine. Musha said that is Yamche bud."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG_CRACKED1 = "It is not mine. Musha said that is Yamche bud."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG_CRACKED2 = "It is not mine. Musha said that is Yamche bud."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG_CRACKED3 = "It is not mine. Musha said that is Yamche bud."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_EGG_CRACKED8 = "It is not mine. Musha said that is Yamche bud."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_SMALL = "What a little bird!"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TEEN = "It... Yes. Yamche. Yamche grows really fast."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TEENR1 = "It... Yes. Yamche. Yamche grows really fast."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TEENR2 = "It... Yes. Yamche. Yamche grows really fast."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TEENR3 = "It... Yes. Yamche. Yamche grows really fast."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TEENR4 = "It... Yes. Yamche. Yamche grows really fast."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TEENICE = "It... Yes. Yamche. Yamche grows really fast."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALL = "Yamche eats anything. So glad."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLR1 = "Yamche eats anything. So glad."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLR2 = "Yamche eats anything. So glad."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLR3 = "Yamche eats anything. So glad."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLR4 = "Yamche eats anything. So glad."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRICE = "Yamche eats anything. So glad."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALL2 = "How useful bird it is!"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRR1 = "How useful bird it is!"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRR2 = "How useful bird it is!"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRR3 = "How useful bird it is!"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRR4 = "How useful bird it is!"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRR5 = "How useful bird it is!"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRICE = "How useful bird it is!"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALL3 = "Yamche is not just a bird. It is a friend to me."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRR1 = "Yamche is not just a bird. It is a friend to me."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRR2 = "Yamche is not just a bird. It is a friend to me."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRR3 = "Yamche is not just a bird. It is a friend to me."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRR4 = "Yamche is not just a bird. It is a friend to me."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRR5 = "Yamche is not just a bird. It is a friend to me."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRICE = "Yamche is not just a bird. It is a friend to me."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALL4 = "Yes. I agree. We are family."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRR1 = "Yes. I agree. We are family."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRR2 = "Yes. I agree. We are family."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRR3 = "Yes. I agree. We are family."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRR4 = "Yes. I agree. We are family."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRR5 = "Yes. I agree. We are family."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRR6 = "Yes. I agree. We are family."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRRICE = "Yes. I agree. We are family."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALL5 = "Travel with Together. It is great."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRRR1 = "Travel with Together. It is great."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRRR2 = "Travel with Together. It is great."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRRR3 = "Travel with Together. It is great."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRRR4 = "Travel with Together. It is great."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRRR5 = "Travel with Together. It is great."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRRR6 = "Travel with Together. It is great."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_TALLRRRRRICE = "Travel with Together. It is great."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_RP1 = "Don't starve partner."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_RP2 = "Don't starve partner."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_RP3 = "Don't starve partner."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_RP4 = "Don't starve partner."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_RP5 = "Don't starve partner."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_RP6 = "Don't starve partner."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_RP7 = "Don't starve partner."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHA_RPICE = "Don't starve partner."
STRINGS.NAMES.GHOSTHOUND = "Ghost Hound"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GHOSTHOUND = "Musha summoned a ghost."
STRINGS.NAMES.GHOSTHOUND2 = "Ghost Puppy"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.GHOSTHOUND2 = "Musha summoned a ghost."
STRINGS.NAMES.SHADOWMUSHA = "Musha's Shadow"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.SHADOWMUSHA = "That Shadow looks like a Musha."
STRINGS.NAMES.TENTACLE_FROST = "Frozen tentacle"
STRINGS.RECIPE_DESC.MUSHA_EGG = "Phoenix Egg (order'z')"
STRINGS.RECIPE_DESC.MUSHA_EGGS1 = "Phoenix Egg (Lv.2)"
STRINGS.RECIPE_DESC.MUSHA_EGGS2 = "Phoenix Egg (Lv.3)"
STRINGS.RECIPE_DESC.MUSHA_EGGS3 = "Phoenix Egg (Lv.4)"
STRINGS.RECIPE_DESC.MUSHA_EGG1 = "Phoenix Egg (Lv.5)"
STRINGS.RECIPE_DESC.MUSHA_EGG2 = "Phoenix Egg (Lv.6)"
STRINGS.RECIPE_DESC.MUSHA_EGG3 = "Phoenix Egg (Lv.7)"
STRINGS.RECIPE_DESC.MUSHA_EGG8 = "Phoenix Egg (Lv.Max)"
STRINGS.RECIPE_DESC.REDGEM = "Alchemic to Gem (Blue->Red)"
STRINGS.RECIPE_DESC.BLUEGEM = "Alchemic to Gem (Red->Blue)" 
STRINGS.RECIPE_DESC.NITRE = "Alchemic to Gold (Gold->Nitre)" 
STRINGS.NAMES.NITRE = "Nitre"
STRINGS.NAMES.REDGEM = "Red gem"
STRINGS.NAMES.REDGEM2 = "Red gem"
STRINGS.NAMES.BLUEGEM = "Blue gem" 
STRINGS.NAMES.BLUEGEM2 = "Blue gem" 
STRINGS.NAMES.MUSHASWORD_BASE = "Broken Blade"
STRINGS.NAMES.MUSHASWORD = "Phoenix Blade"
STRINGS.NAMES.MUSHASWORD_FROST = "Frost Phoenix Blade"
STRINGS.RECIPE_DESC.MUSHASWORD_BASE = "Broken Phoenix Blade, Visual Change"
STRINGS.RECIPE_DESC.MUSHASWORD = "Growable Weapon, Visual Change"

STRINGS.RECIPE_DESC.MUSHASWORD_FROST = "Growable Weapon, Visual Change"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHASWORD_BASE = "It is fixable sword by elemental."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHASWORD = "It is fixable flame sword by elemental."
STRINGS.CHARACTERS.GENERIC.DESCRIBE.MUSHASWORD_FROST = "It is fixable frost sword by elemental."
STRINGS.NAMES.MUSHA_FLUTE = "Musha Flute"
STRINGS.RECIPE_DESC.MUSHA_FLUTE = "Healing Music. Repair: Glow dust,Light bulb"
STRINGS.NAMES.GLOWDUST = "Glow dust"
STRINGS.RECIPE_DESC.GLOWDUST = "Dust of light"
STRINGS.NAMES.CRISTAL = "Crystal Candy"
STRINGS.RECIPE_DESC.CRISTAL = "Summon=>bulb-plant,Yamche=>EGG"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.CRISTAL = "Looks like a unusual crystal."
STRINGS.NAMES.EXP = "Energy Essence"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.EXP = "It give some exp to player."
STRINGS.NAMES.EXP1000CHEAT = "Cheat Essence"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.EXP1000CHEAT = "It give tons of exp to player."
STRINGS.RECIPE_DESC.EXP = "Energy essence, Can fix Musha's gear."
STRINGS.RECIPE_DESC.EXP10 = "Adds EXP 10"
STRINGS.RECIPE_DESC.EXP50 = "Adds EXP 50"
STRINGS.RECIPE_DESC.EXP100 = "Adds EXP 100"
STRINGS.NAMES.FROSTHAMMER = "Frost Hammer"
STRINGS.RECIPE_DESC.FROSTHAMMER = "Growable, Fixable, Hammer, Icebox"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.FROSTHAMMER = "Heavy blue hammer."
STRINGS.NAMES.BROKEN_FROSTHAMMER = "Frost Armor"
STRINGS.RECIPE_DESC.BROKEN_FROSTHAMMER = "Growable, Fixable, Ice box, Visual Change"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.BROKEN_FROSTHAMMER =  "It got a multiple function."
STRINGS.NAMES.HAT_MPHOENIX = "Phoenix Helmet"
STRINGS.RECIPE_DESC.HAT_MPHOENIX = "Growable, Detachable mask(Right click)"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HAT_MPHOENIX = "A mediaeval helm."
STRINGS.NAMES.HAT_MPRINCESS = "Puppy Princess Crown"
STRINGS.RECIPE_DESC.HAT_MPRINCESS = "Shield, Sanity regen, Visual Change"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HAT_MPRINCESS = "A golden little crown."
STRINGS.NAMES.HAT_MCROWN = "Puppy Queen Crown"
STRINGS.RECIPE_DESC.HAT_MCROWN = "Shield, Sanity regen, Special option"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HAT_MCROWN = "Looks so nice."
STRINGS.NAMES.HAT_MBUNNY = "Bunny Scout Hat"
STRINGS.RECIPE_DESC.HAT_MBUNNY = "Growable, Detachable Gogle(Right click)"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.HAT_MBUNNY = "Looks like a bunny ears."
STRINGS.NAMES.ARMOR_MUSHAA = "Musha's Armor"
STRINGS.RECIPE_DESC.ARMOR_MUSHAA = "Growable Armor, Backpack"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.ARMOR_MUSHAA = "It is a billow dress."
STRINGS.NAMES.ARMOR_MUSHAB = "Princess Armor"
STRINGS.RECIPE_DESC.ARMOR_MUSHAB = "Growable Armor, Backpack"
STRINGS.CHARACTERS.GENERIC.DESCRIBE.ARMOR_MUSHAB = "It is billow, but safe." 
---------------
STRINGS.PHOENIXS=
{    --random
"Captain\nYamche","Hulk\nYamche","Iron\nYamche","Torr\nYamche","Yamche","Brave\nYamche","Cutie\nYamche","Owl\nYamche","Sunny\nYamche","Moon\nYamche","Seoul\nYamche","Suri\nYamche","Vilja\nYamche","Sunny\nYamche","Sunnyholic","Musha\nYamche","Mellisa\nYamche","Lidia\nYamche ","Battleborn\nYamche ","Grey\nYamche","Sky\nYamche","Lucia\nYamche","Khajit\nYamche","Pig\nYamche","Mjoll\nYamche","Lioness\nYamche","Muiri\nYamche","Ysolda\nYamche","Rayya\nYamche","Falkas\nYamche","Vilkas\nYamche","Aela\nYamche","Huter\nYamche","Huntress\nYamche","Queen\nYamche","Fire\nYamche","Cicero\nYamche","Top\nYamche","Lina\nYamche","Totoro\nYamche","Yu-na\nYamche","Winter\nYamche","White\nYamche","Mellisa\nYamche","Riften\nYamche","Dawnstar\nYamche","Windhelm\nYamche","Pho\nYamche","Sneaky\nYamche","Kiwis\nYamche","Coco\nYamche","Moon\nYamche","Pizza\nYamche","Sugar\nYamche","Orc\nYamche","Elf\nYamche","Knight\nYamche","Lich\nYamche","Azeroth\nYamche","Tauren\nYamche","Troll\nYamche","Thrall\nYamche","Narugar\nYamche","Yancook\nYamche","Tirano\nYamche","Honey\nYamche","Golum\nYamche","Bosom\nYamche","Esmeralda\nYamche","Pluvia\nYamche","Doraemon\nYamche","Dooly\nYamche","Apple\nYamche","IU\nYamche","Gandalf\nYamche","Frodo\nYamche","Sam\nYamche","Regolas\nYamche","Gimli\nYamche","Boromir\nYamche","Wilxon\nYamche","Willo\nYamche","Wolfkong\nYamche","Wenil\nYamche","WX79\nYamche","Wickerbi\nYamche","Woorie\nYamche","Wex\nYamche","Maximus\nYamche","Wigfreedom\nYamche","Webbers\nYamche","Naruto\nYamche","Sasuke\nYamche","Witcher\nYamche","Luka\nYamche","Arong\nYamche","Puppy\nYamche",
 }
-----------------------------difficulty test
--
function giants_1x(inst)
  inst:AddTag("giant1x")
end
function deerclops_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.DEERCLOPS_HEALTH*2)
    inst.components.combat:SetDefaultDamage(TUNING.DEERCLOPS_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(2.75)
  inst:AddTag("giant2x")
end
function bearger_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.BEARGER_HEALTH*2)
    inst.components.combat:SetDefaultDamage(TUNING.DEERCLOPS_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(2.75)
  inst:AddTag("giant2x")
end
function dragonfly_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.DRAGONFLY_HEALTH*2)
    inst.components.combat:SetDefaultDamage(TUNING.DEERCLOPS_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(2.25)
  inst:AddTag("giant2x")
end
function moose_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.MOOSE_HEALTH*2)
    inst.components.combat:SetDefaultDamage(TUNING.DEERCLOPS_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(2.75)
  inst:AddTag("giant2x")
end
function mossling_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.MOSSLING_HEALTH*2)
    inst.components.combat:SetDefaultDamage(TUNING.DEERCLOPS_DAMAGE*1.5)
  inst:AddTag("small_giant2x")
end
function deerclops_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.DEERCLOPS_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.DEERCLOPS_DAMAGE*2.5)
    inst.components.combat:SetAttackPeriod(2.6)
  inst:AddTag("giant3x")
end
function bearger_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.BEARGER_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.BEARGER_DAMAGE*2.5)
    inst.components.combat:SetRange(TUNING.BEARGER_ATTACK_RANGE, TUNING.BEARGER_MELEE_RANGE)
    inst.components.combat:SetAttackPeriod(2.6)
  inst:AddTag("giant3x")
end
function dragonfly_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.DRAGONFLY_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.DRAGONFLY_DAMAGE*2.5)
    inst.components.combat:SetAttackPeriod(2.1)
  inst:AddTag("giant3x")
end
function moose_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.MOOSE_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.MOOSE_DAMAGE*2.5)
    inst.components.combat:SetAttackPeriod(2.6)
   inst:AddTag("giant3x")
end
function mossling_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.MOSSLING_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.MOSSLING_DAMAGE*2.5)
   inst:AddTag("small_giant3x")
end
function deerclops_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.DEERCLOPS_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.DEERCLOPS_DAMAGE*3)
 --   inst.components.combat:SetAreaDamage(TUNING.DEERCLOPS_AOE_RANGE, TUNING.DEERCLOPS_AOE_SCALE)
    inst.components.combat:SetAttackPeriod(2.3)
   inst:AddTag("giant4x")
end
function bearger_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.BEARGER_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.BEARGER_DAMAGE*3)
    inst.components.combat:SetRange(TUNING.BEARGER_ATTACK_RANGE, TUNING.BEARGER_MELEE_RANGE)
 --   inst.components.combat:SetAreaDamage(6, 0.8)
    inst.components.combat:SetAttackPeriod(2.3)
   inst:AddTag("giant4x")
end
function dragonfly_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.DRAGONFLY_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.DRAGONFLY_DAMAGE*3)
   -- inst.components.combat:SetAreaDamage(6, 0.8)
    inst.components.combat:SetAttackPeriod(1.8)
   inst:AddTag("giant4x")
end
function moose_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.MOOSE_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.MOOSE_DAMAGE*3)
    inst.components.combat:SetAttackPeriod(2.3)
    inst:AddTag("giant4x")
end
function mossling_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.MOSSLING_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.MOSSLING_DAMAGE*3)
    inst:AddTag("small_giant4x")
end

------------
function nspider_1x(inst)
    -- inst:AddTag("monster1x")
end
function nspider_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_HEALTH*2)
    inst:AddTag("monster2x")
end
function wspider_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_WARRIOR_HEALTH*2)
    inst:AddTag("monster2x")
end
function qspider_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDERQUEEN_HEALTH*2)
    inst:AddTag("small_giant2x")
end
function chspider_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_HIDER_HEALTH*2)
    inst:AddTag("monster2x")
end
function csspider_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_SPITTER_HEALTH*2)
    inst:AddTag("monster2x")
end
function cdspider_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_WARRIOR_HEALTH*2)
     inst:AddTag("monster2x")
end
function nspider_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(2.75)
     inst:AddTag("monster3x")
end
function wspider_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_WARRIOR_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_WARRIOR_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(3.75 + math.random()*2)
     inst:AddTag("monster3x")
end
function qspider_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDERQUEEN_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDERQUEEN_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(2.75)
     inst:AddTag("small_giant3x")
end
function chspider_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_HIDER_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_HIDER_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(2.75)
     inst:AddTag("monster3x")
end
function csspider_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_SPITTER_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_SPITTER_DAMAGE_MELEE*1.5)
    inst.components.combat:SetAttackPeriod(4.75 + math.random()*2)
     inst:AddTag("monster3x")
end
function cdspider_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_WARRIOR_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_WARRIOR_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(2.75 + math.random()*2)
     inst:AddTag("monster3x")
end
function nspider_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(2.5)
     inst:AddTag("monster4x")
end
function wspider_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_WARRIOR_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_WARRIOR_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(3.5 + math.random()*2)
     inst:AddTag("monster4x")
end
function qspider_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDERQUEEN_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDERQUEEN_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(2.5)
     inst:AddTag("small_giant4x")
end
function chspider_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_HIDER_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_HIDER_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(2.5)
     inst:AddTag("monster4x")
end
function csspider_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_SPITTER_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_SPITTER_DAMAGE_MELEE*2)
    inst.components.combat:SetAttackPeriod(4.5 + math.random()*2)
        inst:AddTag("monster4x")
end
function cdspider_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.SPIDER_WARRIOR_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.SPIDER_WARRIOR_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(3.5 + math.random()*2)
        inst:AddTag("monster4x")
end
------
function hound_1x(inst)
        -- inst:AddTag("monster1x")
end
function nhound_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.HOUND_HEALTH*2)
        inst:AddTag("monster2x")
end
function fhound_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.FIREHOUND_HEALTH*2)
        inst:AddTag("monster2x")
end
function ihound_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.ICEHOUND_HEALTH*2)
        inst:AddTag("monster2x")
end
function warg_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.WARG_HEALTH*2)
        inst:AddTag("small_giant2x")
end

function nhound_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.HOUND_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.HOUND_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(1.75)
        inst:AddTag("monster3x")
end
function fhound_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.FIREHOUND_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.FIREHOUND_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(1.75)
        inst:AddTag("monster3x")
end
function ihound_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.ICEHOUND_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.ICEHOUND_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(1.75)
        inst:AddTag("monster3x")
end
function warg_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.WARG_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.WARG_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(2.75)
        inst:AddTag("small_giant3x")
end

function nhound_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.HOUND_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.HOUND_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(1.5)
        inst:AddTag("monster4x")
end
function fhound_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.FIREHOUND_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.FIREHOUND_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(1.5)
        inst:AddTag("monster4x")
end
function ihound_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.ICEHOUND_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.ICEHOUND_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(1.5)
        inst:AddTag("monster4x")
end
function warg_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.WARG_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.WARG_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(2.5)
        inst:AddTag("small_giant4x")
end
function tentacle_1x(inst)
        -- inst:AddTag("monster1x")
end
function tentacle_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.TENTACLE_HEALTH*2)
    inst.components.combat:SetDefaultDamage(TUNING.TENTACLE_DAMAGE*1.5)
        inst:AddTag("monster2x")
end
function tentacle_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.TENTACLE_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.TENTACLE_DAMAGE*2)
        inst:AddTag("monster3x")
end
function tentacle_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.TENTACLE_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.TENTACLE_DAMAGE*2.5)
        inst:AddTag("monster4x")
end
function rook_1x(inst)
        -- inst:AddTag("monster1x")
end
function rook_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.ROOK_HEALTH*2)
    inst.components.combat:SetDefaultDamage(TUNING.ROOK_DAMAGE*1.5)
        inst:AddTag("monster2x")
end
function rook_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.ROOK_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.ROOK_DAMAGE*2)
        inst:AddTag("monster3x")
end
function rook_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.ROOK_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.ROOK_DAMAGE*3)
        inst:AddTag("monster4x")
end
function minotaur_1x(inst)
        -- inst:AddTag("monster1x")
end
function minotaur_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.MINOTAUR_HEALTH*2)
    inst.components.combat:SetDefaultDamage(TUNING.MINOTAUR_DAMAGE*1.5)
        inst:AddTag("monster2x")
end
function minotaur_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.MINOTAUR_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.MINOTAUR_DAMAGE*2)
        inst:AddTag("monster3x")
end
function minotaur_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.MINOTAUR_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.MINOTAUR_DAMAGE*2.5)
        inst:AddTag("monster4x")
end
function bishop_1x(inst)
        -- inst:AddTag("monster1x")
end
function bishop_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.BISHOP_HEALTH*2)
    inst.components.combat:SetDefaultDamage(TUNING.BISHOP_DAMAGE*1.2)
        inst:AddTag("monster2x")
end
function bishop_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.BISHOP_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.BISHOP_DAMAGE*1.6)
        inst:AddTag("monster3x")
end
function bishop_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.BISHOP_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.BISHOP_DAMAGE*2)
        inst:AddTag("monster4x")
end
function tallbird_1x(inst)
        -- inst:AddTag("monster1x")
end
function tallbird_2x(inst)
    inst.components.health:SetMaxHealth(TUNING.TALLBIRD_HEALTH*2)
        inst:AddTag("monster2x")
end
function tallbird_3x(inst)
    inst.components.health:SetMaxHealth(TUNING.TALLBIRD_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.TALLBIRD_DAMAGE*1.5)
    inst.components.combat:SetAttackPeriod(1.75)
        inst:AddTag("monster3x")
end
function tallbird_4x(inst)
    inst.components.health:SetMaxHealth(TUNING.TALLBIRD_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.TALLBIRD_DAMAGE*2)
    inst.components.combat:SetAttackPeriod(1.5)
        inst:AddTag("monster4x")
end
function leif_1x(inst)
        inst:AddTag("small_giant1x")
    
end
function leif_2x(inst)
        inst:AddTag("small_giant2x")
    inst.components.health:SetMaxHealth(TUNING.LEIF_HEALTH*2)
end
function leif_3x(inst)
        inst:AddTag("small_giant3x")
    inst.components.health:SetMaxHealth(TUNING.LEIF_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.LEIF_DAMAGE*1.5)
end
function leif_4x(inst)
        inst:AddTag("small_giant4x")
    inst.components.health:SetMaxHealth(TUNING.LEIF_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.LEIF_DAMAGE*2)
end
function birchnutdrake_1x(inst)
        -- inst:AddTag("monster1x")
end
function birchnutdrake_2x(inst)
        inst:AddTag("monster2x")
    inst.components.health:SetMaxHealth(100)
end
function birchnutdrake_3x(inst)
        inst:AddTag("monster3x")
    inst.components.health:SetMaxHealth(150)
    inst.components.combat:SetDefaultDamage(10)
end
function birchnutdrake_4x(inst)
        inst:AddTag("monster4x")
    inst.components.health:SetMaxHealth(200)
    inst.components.combat:SetDefaultDamage(15)
end
function merm_1x(inst)
        -- inst:AddTag("monster1x")
   
end
function merm_2x(inst)
        inst:AddTag("monster2x")
    inst.components.health:SetMaxHealth(TUNING.MERM_HEALTH*4)
end
function merm_3x(inst)
        inst:AddTag("monster3x")
    inst.components.health:SetMaxHealth(TUNING.MERM_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.MERM_DAMAGE*1.5)
end
function merm_4x(inst)
        inst:AddTag("monster4x")
    inst.components.health:SetMaxHealth(TUNING.MERM_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.MERM_DAMAGE*2)
end
function bat_1x(inst)
        -- inst:AddTag("monster1x")
    
end
function bat_2x(inst)
        inst:AddTag("monster2x")
    inst.components.health:SetMaxHealth(TUNING.BAT_HEALTH*2)
end
function bat_3x(inst)
        inst:AddTag("monster3x")
    inst.components.health:SetMaxHealth(TUNING.BAT_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.BAT_DAMAGE*1.5)
end
function bat_4x(inst)
        inst:AddTag("monster4x")
    inst.components.health:SetMaxHealth(TUNING.BAT_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.BAT_DAMAGE*2)
end
function monkey_1x(inst)
        -- inst:AddTag("monster1x")
    
end
function monkey_2x(inst)
        inst:AddTag("monster2x")
    inst.components.health:SetMaxHealth(TUNING.MONKEY_HEALTH*2)
end
function monkey_3x(inst)
        inst:AddTag("monster3x")
    inst.components.health:SetMaxHealth(TUNING.MONKEY_HEALTH*4)
end
function monkey_4x(inst)
        inst:AddTag("monster4x")
    inst.components.health:SetMaxHealth(TUNING.MONKEY_HEALTH*6)
end
function walus_1x(inst)
        -- inst:AddTag("monster1x")
    
end
function walus_2x(inst)
        inst:AddTag("monster2x")
    inst.components.health:SetMaxHealth(TUNING.WALRUS_HEALTH*2)
end
function walus_3x(inst)
        inst:AddTag("monster3x")
    inst.components.health:SetMaxHealth(TUNING.WALRUS_HEALTH*4)
    inst.components.locomotor.runspeed = 6
    inst.components.locomotor.walkspeed = 2
end
function walus_4x(inst)
        inst:AddTag("monster4x")
    inst.components.health:SetMaxHealth(TUNING.WALRUS_HEALTH*6)
    inst.components.locomotor.runspeed = 8
    inst.components.locomotor.walkspeed = 2
end
function eyeplant_1x(inst)
end
function eyeplant_2x(inst)
        -- inst:AddTag("monster1x")
    inst.components.health:SetMaxHealth(TUNING.EYEPLANT_HEALTH*2)
end
function eyeplant_3x(inst)
        -- inst:AddTag("monster1x")
    inst.components.health:SetMaxHealth(TUNING.EYEPLANT_HEALTH*4)
    inst.components.combat:SetDefaultDamage(TUNING.EYEPLANT_DAMAGE*1.5)
end
function eyeplant_4x(inst)
        inst:AddTag("monster2x")
    inst.components.health:SetMaxHealth(TUNING.EYEPLANT_HEALTH*6)
    inst.components.combat:SetDefaultDamage(TUNING.EYEPLANT_DAMAGE*2)
end


-------difficult option
if Difficult == "monster1x" then
--[[
AddPrefabPostInit("deerclops", giants_1x)
AddPrefabPostInit("bearger", giants_1x)
AddPrefabPostInit("dragonfly", giants_1x)
AddPrefabPostInit("moose", giants_1x)
AddPrefabPostInit("mossling", giants_1x)
AddPrefabPostInit("spider", nspider_1x)
AddPrefabPostInit("spider_warrior", spider_1x)
AddPrefabPostInit("spiderqueen", spider_1x)
AddPrefabPostInit("spider_hider", spider_1x)
AddPrefabPostInit("spider_spitter", spider_1x)
AddPrefabPostInit("spider_dropper", spider_1x)
AddPrefabPostInit("hound", hound_1x)
AddPrefabPostInit("firehound", hound_1x)
AddPrefabPostInit("icehound", hound_1x)
AddPrefabPostInit("warg", hound_1x)
AddPrefabPostInit("tentacle", tentacle_1x)
AddPrefabPostInit("rook", rook_1x)
AddPrefabPostInit("minotaur", minotaur_1x)
AddPrefabPostInit("bishop", bishop_1x)
AddPrefabPostInit("tallbird", tallbird_1x)
AddPrefabPostInit("leif", leif_1x)
AddPrefabPostInit("merm", merm_1x)
AddPrefabPostInit("birchnutdrake", birchnutdrake_1x)
AddPrefabPostInit("bat", bat_1x)
AddPrefabPostInit("walus", walus_1x)
AddPrefabPostInit("monkey", monkey_1x)
AddPrefabPostInit("eyeplant", eyeplant_1x)]]
end
if Difficult == "monster2x" then
AddPrefabPostInit("deerclops", deerclops_2x)
AddPrefabPostInit("bearger", bearger_2x)
AddPrefabPostInit("dragonfly", dragonfly_2x)
AddPrefabPostInit("moose", moose_2x)
AddPrefabPostInit("mossling", mossling_2x)
AddPrefabPostInit("spider", nspider_2x)
AddPrefabPostInit("spider_warrior", wspider_2x)
AddPrefabPostInit("spiderqueen", qspider_2x)
AddPrefabPostInit("spider_hider", chspider_2x)
AddPrefabPostInit("spider_spitter", csspider_2x)
AddPrefabPostInit("spider_dropper", cdspider_2x)
AddPrefabPostInit("hound", nhound_2x)
AddPrefabPostInit("firehound", fhound_2x)
AddPrefabPostInit("icehound", ihound_2x)
AddPrefabPostInit("warg", warg_2x)
AddPrefabPostInit("tentacle", tentacle_2x)
AddPrefabPostInit("rook", rook_2x)
AddPrefabPostInit("minotaur", minotaur_2x)
AddPrefabPostInit("bishop", bishop_2x)
AddPrefabPostInit("tallbird", tallbird_2x)
AddPrefabPostInit("leif", leif_2x)
AddPrefabPostInit("merm", merm_2x)
AddPrefabPostInit("birchnutdrake", birchnutdrake_2x)
AddPrefabPostInit("bat", bat_2x)
AddPrefabPostInit("walus", walus_2x)
AddPrefabPostInit("monkey", monkey_2x)
AddPrefabPostInit("eyeplant", eyeplant_2x)
end
if Difficult == "monster3x" then
AddPrefabPostInit("deerclops", deerclops_3x)
AddPrefabPostInit("bearger", bearger_3x)
AddPrefabPostInit("dragonfly", dragonfly_3x)
AddPrefabPostInit("moose", moose_3x)
AddPrefabPostInit("mossling", mossling_3x)
AddPrefabPostInit("spider", nspider_3x)
AddPrefabPostInit("spider_warrior", wspider_3x)
AddPrefabPostInit("spiderqueen", qspider_3x)
AddPrefabPostInit("spider_hider", chspider_3x)
AddPrefabPostInit("spider_spitter", csspider_3x)
AddPrefabPostInit("spider_dropper", cdspider_3x)
AddPrefabPostInit("hound", nhound_3x)
AddPrefabPostInit("firehound", fhound_3x)
AddPrefabPostInit("icehound", ihound_3x)
AddPrefabPostInit("warg", warg_3x)
AddPrefabPostInit("tentacle", tentacle_3x)
AddPrefabPostInit("rook", rook_3x)
AddPrefabPostInit("minotaur", minotaur_3x)
AddPrefabPostInit("bishop", bishop_3x)
AddPrefabPostInit("tallbird", tallbird_3x)
AddPrefabPostInit("leif", leif_3x)
AddPrefabPostInit("merm", merm_3x)
AddPrefabPostInit("birchnutdrake", birchnutdrake_3x)
AddPrefabPostInit("bat", bat_3x)
AddPrefabPostInit("walus", walus_3x)
AddPrefabPostInit("monkey", monkey_3x)
AddPrefabPostInit("eyeplant", eyeplant_3x)
end
if Difficult == "monster4x" then
AddPrefabPostInit("deerclops", deerclops_4x)
AddPrefabPostInit("bearger", bearger_4x)
AddPrefabPostInit("dragonfly", dragonfly_4x)
AddPrefabPostInit("moose", moose_4x)
AddPrefabPostInit("mossling", mossling_4x)
AddPrefabPostInit("spider", nspider_4x)
AddPrefabPostInit("spider_warrior", wspider_4x)
AddPrefabPostInit("spiderqueen", qspider_4x)
AddPrefabPostInit("spider_hider", chspider_4x)
AddPrefabPostInit("spider_spitter", csspider_4x)
AddPrefabPostInit("spider_dropper", cdspider_4x)
AddPrefabPostInit("hound", nhound_4x)
AddPrefabPostInit("firehound", fhound_4x)
AddPrefabPostInit("icehound", ihound_4x)
AddPrefabPostInit("warg", warg_4x)
AddPrefabPostInit("tentacle", tentacle_4x)
AddPrefabPostInit("rook", rook_4x)
AddPrefabPostInit("minotaur", minotaur_4x)
AddPrefabPostInit("bishop", bishop_4x)
AddPrefabPostInit("tallbird", tallbird_4x)
AddPrefabPostInit("leif", leif_4x)
AddPrefabPostInit("merm", merm_4x)
AddPrefabPostInit("birchnutdrake", birchnutdrake_4x)
AddPrefabPostInit("bat", bat_4x)
AddPrefabPostInit("walus", walus_4x)
AddPrefabPostInit("monkey", monkey_4x)
AddPrefabPostInit("eyeplant", eyeplant_4x)
end
-------
 
AddModCharacter("musha","FEMALE")
--table.insert(GLOBAL.CHARACTER_GENDERS.FEMALE, "musha")
