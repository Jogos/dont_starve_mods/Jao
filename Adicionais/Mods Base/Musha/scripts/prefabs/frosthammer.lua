local assets=
{
	Asset("ANIM", "anim/frosthammer.zip"),
	Asset("ANIM", "anim/swap_frosthammer.zip"),
	Asset("ATLAS", "images/inventoryimages/frosthammer.xml"),
	Asset("IMAGE", "images/inventoryimages/frosthammer.tex"),
}

local prefabs =
{}

local function levelexp(inst,data)

	local max_exp = 4100
	local exp = math.min(inst.level, max_exp)

if inst.level > 4005 then
--inst.components.talker:Say("-[Frost Hammer]-\n[Grow Points]".. (inst.level))
end
if inst.level >=0 and inst.level <10 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[10]\n[Grow Points]".. (inst.level))
elseif inst.level >=10 and inst.level <30 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[30]\n[Grow Points]".. (inst.level))
elseif inst.level >=30 and inst.level <50 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[50]\n[Grow Points]".. (inst.level))
elseif inst.level >=50 and inst.level <70 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[70]\n[Grow Points]".. (inst.level))
elseif inst.level >=70 and inst.level <90 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[90]\n[Grow Points]".. (inst.level))
elseif inst.level >=90 and inst.level <120 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[120]\n[Grow Points]".. (inst.level))
elseif inst.level >=120 and inst.level <150 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[150]\n[Grow Points]".. (inst.level))
elseif inst.level >=150 and inst.level <180 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[180]\n[Grow Points]".. (inst.level))
elseif inst.level >=180 and inst.level <210 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[210]\n[Grow Points]".. (inst.level))
elseif inst.level >=210 and inst.level <250 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[250]\n[Grow Points]".. (inst.level))
elseif inst.level >=250 and inst.level <350 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[350]\n[Grow Points]".. (inst.level))
elseif inst.level >=350 and inst.level <450 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[450]\n[Grow Points]".. (inst.level))
elseif inst.level >=450 and inst.level <550 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[550]\n[Grow Points]".. (inst.level))
elseif inst.level >=550 and inst.level <650 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[650]\n[Grow Points]".. (inst.level))
elseif inst.level >=650 and inst.level <750 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[750]\n[Grow Points]".. (inst.level))
elseif inst.level >=750 and inst.level <850 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[850]\n[Grow Points]".. (inst.level))
elseif inst.level >=850 and inst.level <950 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[950]\n[Grow Points]".. (inst.level))
elseif inst.level >=950 and inst.level <1050 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[1050]\n[Grow Points]".. (inst.level))
elseif inst.level >=1050 and inst.level <1200 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[1200]\n[Grow Points]".. (inst.level))
elseif inst.level >=1200 and inst.level <1400 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[1500]\n[Grow Points]".. (inst.level))
elseif inst.level >=1400 and inst.level <1600 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[1600]\n[Grow Points]".. (inst.level))
elseif inst.level >=1600 and inst.level <1800 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[1800]\n[Grow Points]".. (inst.level))
elseif inst.level >=1800 and inst.level <2000 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[2000]\n[Grow Points]".. (inst.level))
elseif inst.level >=2000 and inst.level <2200 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[2200]\n[Grow Points]".. (inst.level))
elseif inst.level >=2200 and inst.level <2400 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[2400]\n[Grow Points]".. (inst.level))
elseif inst.level >=2400 and inst.level <2600 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[2600]\n[Grow Points]".. (inst.level))
elseif inst.level >=2600 and inst.level <2800 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[2800]\n[Grow Points]".. (inst.level))
elseif inst.level >=2800 and inst.level <3000 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[3000]\n[Grow Points]".. (inst.level))
elseif inst.level >=3000 and inst.level <4000 then
inst.components.talker:Say("-[Frost Hammer] \nNext:[4000]\n[Grow Points]".. (inst.level))
elseif inst.level >=4000 and inst.level <4005 then
inst.components.talker:Say("-[Frost Hammer] \n[MAX]")
end
end

local function onpreload(inst, data)
	if data then
		if data.level then
		inst.level = data.level
			levelexp(inst)
end
	end
		end
local function onsave(inst, data)
	data.level = inst.level
end

local function Upgradedamage(inst, data)
if inst.components.fueled:IsEmpty() then
inst.broken = true
elseif not inst.components.fueled:IsEmpty() then
inst.broken = false
end
if inst.broken then
   inst.components.weapon:SetDamage(1)
	inst.components.talker:Say("-Broken Frost Hammer \nDamage (1)")
  
elseif not inst.broken then

 if inst.level >=0 and inst.level <10 then
    inst.components.weapon:SetDamage(40)
	inst.components.talker:Say("-Frost Hammer(LV1)\nDamage (40)\nFreeze(10D/20)")
 elseif inst.level >=10 and inst.level <30 then
    inst.components.weapon:SetDamage(41)
	inst.components.talker:Say("-Frost Hammer(LV2)\nDamage (41)\nFreeze(10D/20)")
 elseif inst.level >=30 and inst.level <50 then
    inst.components.weapon:SetDamage(42)
	inst.components.talker:Say("-Frost Hammer(LV3)\nDamage (42)\nFreeze(10D/20)")
 elseif inst.level >=50 and inst.level <70 then
    inst.components.weapon:SetDamage(43)
	inst.components.talker:Say("-Frost Hammer(LV4)]\nDamage (43)\nFreeze(10D/20)")
  elseif inst.level >=70 and inst.level <90 then
    inst.components.weapon:SetDamage(44)
	inst.components.talker:Say("-Frost Hammer(LV5)\nDamage (44)\nFreeze(10D/20)")
 elseif inst.level >=90 and inst.level <120 then
    inst.components.weapon:SetDamage(45)
	inst.components.talker:Say("-Frost Hammer(LV6)\nDamage (45)\nFreeze(10D/20)")
 elseif inst.level >=120 and inst.level <150 then
    inst.components.weapon:SetDamage(46)
	inst.components.talker:Say("-Frost Hammer(LV7)\nDamage (46)\nFreeze(10D/20)")
elseif inst.level >=150 and inst.level <180 then
    inst.components.weapon:SetDamage(47)
	inst.components.talker:Say("-Frost Hammer(LV8)\nDamage (47)\nFreeze(10D/20)")
elseif inst.level >=180 and inst.level <210 then
    inst.components.weapon:SetDamage(48)
	inst.components.talker:Say("-Frost Hammer(LV9)\nDamage (48)\nFreeze(10D/20)")
elseif inst.level >=210 and inst.level <250 then
    inst.components.weapon:SetDamage(49)
	inst.components.talker:Say("-Frost Hammer(LV10)\nDamage (49)\nFreeze(20D/40)")
elseif inst.level >=250 and inst.level <350 then
    inst.components.weapon:SetDamage(50)
	inst.components.talker:Say("-Frost Hammer(LV11)\nDamage (50)\nFreeze(20D/40)")
elseif inst.level >=350 and inst.level <450 then
    inst.components.weapon:SetDamage(51)
	inst.components.talker:Say("-Frost Hammer(LV12)\nDamage (51)\nFreeze(20D/40)")
elseif inst.level >=450 and inst.level <550 then
    inst.components.weapon:SetDamage(52)
	inst.components.talker:Say("-Frost Hammer(LV13)\nDamage (52)\nFreeze(20D/40)")
elseif inst.level >=550 and inst.level <650 then
    inst.components.weapon:SetDamage(53)
	inst.components.talker:Say("-Frost Hammer(LV14)\nDamage (53)\nFreeze(20D/40)")
elseif inst.level >=650 and inst.level <750 then
    inst.components.weapon:SetDamage(54)
	inst.components.talker:Say("-Frost Hammer(LV15)\nDamage (54)\nFreeze(20D/40)")
elseif inst.level >=750 and inst.level <850 then
    inst.components.weapon:SetDamage(55)
	inst.components.talker:Say("-Frost Hammer(LV16)\nDamage (55)\nFreeze(20D/40)")
elseif inst.level >=850 and inst.level <950 then
    inst.components.weapon:SetDamage(56)
	inst.components.talker:Say("-Frost Hammer(LV17)\nDamage (56)\nFreeze(20D/40)")
elseif inst.level >=950 and inst.level <1050 then
    inst.components.weapon:SetDamage(57)
	inst.components.talker:Say("-Frost Hammer(LV18)\nDamage (57)\nFreeze(20D/40)")
elseif inst.level >=1050 and inst.level <1200 then
    inst.components.weapon:SetDamage(58)
	inst.components.talker:Say("-Frost Hammer(LV19)\nDamage (58)\nFreeze(20D/40)")
elseif inst.level >=1200 and inst.level <1400 then
    inst.components.weapon:SetDamage(59)
	inst.components.talker:Say("-Frost Hammer(LV20)\nDamage (59)\nFreeze(30D/60)")
elseif inst.level >=1400 and inst.level <1600 then
    inst.components.weapon:SetDamage(60)
	inst.components.talker:Say("-Frost Hammer(LV21)\nDamage (60)\nFreeze(30D/60)")
elseif inst.level >=1600 and inst.level <1800 then
    inst.components.weapon:SetDamage(61)
	inst.components.talker:Say("-Frost Hammer(LV22)\nDamage (61)\nFreeze(30D/60)")
elseif inst.level >=1800 and inst.level <2000 then
    inst.components.weapon:SetDamage(62)
	inst.components.talker:Say("-Frost Hammer(LV23)\nDamage (62)\nFreeze(30D/60)")
elseif inst.level >=2000 and inst.level <2200 then
    inst.components.weapon:SetDamage(63)
	inst.components.talker:Say("-Frost Hammer(LV24)\nDamage (63)\nFreeze(30D/60)")
elseif inst.level >=2200 and inst.level <2400 then
    inst.components.weapon:SetDamage(64)
	inst.components.talker:Say("-Frost Hammer(LV25)\nDamage (64)\nFreeze(30D/60)")
elseif inst.level >=2400 and inst.level <2600 then
    inst.components.weapon:SetDamage(65)
	inst.components.talker:Say("-Frost Hammer(LV26)\nDamage (65)\nFreeze(30D/60)")
elseif inst.level >=2600 and inst.level <2800 then
    inst.components.weapon:SetDamage(66)
	inst.components.talker:Say("-Frost Hammer(LV27)\nDamage (66)\nFreeze(30D/60)")
elseif inst.level >=2800 and inst.level <3000 then
    inst.components.weapon:SetDamage(67)
	inst.components.talker:Say("-Frost Hammer(LV28)\nDamage (67)\nFreeze(30D/60)")
elseif inst.level >=3000 and inst.level <4000 then
    inst.components.weapon:SetDamage(68)
	inst.components.talker:Say("-Frost Hammer(LV29)\nDamage (68)\nFreeze(30D/60)")
elseif inst.level >=4000 then
    inst.components.weapon:SetDamage(70)
	inst.components.talker:Say("-Frost Hammer(Max30)\nDamage (70)\nFreeze(30D/60)")
end
end
end

local function OnDurability(inst, data)
inst.broken = true
 	Upgradedamage(inst)
end
-------- --------
local function TakeItem(inst, item, data)
local expchance0 = 1
local expchance1 = 0.3
local expchance2 = 0.2
local expchance3 = 0.12
	inst.components.fueled:DoDelta(5000000)
	SpawnPrefab("splash").Transform:SetPosition(inst:GetPosition():Get())
inst.broken = false      
Upgradedamage(inst)
   if math.random() < expchance1 and inst.level <= 4005 then
	inst.level = inst.level + 2
	levelexp(inst)
	inst.components.talker:Say("-[Frost Hammer] \nLucky Points ! +(2)\n[Grow Points]".. (inst.level))
    elseif  math.random() < expchance2 and inst.level <= 4005 then
	inst.level = inst.level + 5
	levelexp(inst)
	inst.components.talker:Say("-[Frost Hammer] \nLucky Points ! +(5)\n[Grow Points]".. (inst.level))
	elseif  math.random() < expchance3 and inst.level <= 4005 then
	inst.level = inst.level + 8
	levelexp(inst)
	inst.components.talker:Say("-[Frost Hammer] \nLucky Points ! +(8)\n[Grow Points]".. (inst.level))
	elseif  math.random() < expchance0 and inst.level <= 4005 then
	inst.level = inst.level + 1
	levelexp(inst)
    end
end
-------- --------
local function OnLoad(inst, data)
    Upgradedamage(inst)
end

local function onattack_FROST(inst, attacker, target)
local player = inst.components.inventoryitem.owner
local freezechance1 = 0.3
local freezechance2 = 0.45
local freezechance3 = 0.6
local expchance = 0.1
local damagedur1 = 0.2
local damagedur2 = 0.5
local damagedur3 = 0.7
local damagedur4 = 1

if math.random() < expchance and not inst.broken and inst.level <= 4000 then
	inst.level = inst.level + 1
	levelexp(inst)
end
    if target and not inst.broken and math.random() < damagedur1 then
inst.components.fueled:DoDelta(-150000)
    elseif target and not inst.broken and math.random() < damagedur2 then
inst.components.fueled:DoDelta(-75000)
    elseif target and not inst.broken and math.random() < damagedur3 then
inst.components.fueled:DoDelta(-35000)
    elseif target and not inst.broken and math.random() < damagedur4 then
inst.components.fueled:DoDelta(-20000)
    elseif target and inst.broken then
SpawnPrefab("splash").Transform:SetPosition(inst:GetPosition():Get())
	inst.components.talker:Say("-Broken Weapon \nDamage (1)")
    end

	if  math.random() < freezechance1 and target.components.freezable and inst.boost and inst.level <250 then
        target.components.freezable:AddColdness(0.25)
        target.components.freezable:SpawnShatterFX()
		target.components.health:DoDelta(-10)
	elseif math.random() < freezechance2 and target.components.freezable and inst.boost and inst.level >=250 and inst.level <1400 then
        target.components.freezable:AddColdness(0.45)
        target.components.freezable:SpawnShatterFX()
		target.components.health:DoDelta(-20)
	elseif math.random() < freezechance3 and target.components.freezable and inst.boost and inst.level >=1400 then
        target.components.freezable:AddColdness(0.65)
        target.components.freezable:SpawnShatterFX()
		target.components.health:DoDelta(-30)		
	
	end
    if target.components.sleeper and target.components.sleeper:IsAsleep() then
        target.components.sleeper:WakeUp()
    end
    if target.components.burnable and target.components.burnable:IsBurning() then
        target.components.burnable:Extinguish()
    end
    if target.components.combat then
        target.components.combat:SuggestTarget(attacker)
    end
end
--------

local function summoning(staff, target, pos)
--local player = inst.components.inventoryitem.owner
    local caster = staff.components.inventoryitem.owner
if caster.components.sanity.current >= 40 then
     local light1 = SpawnPrefab("splash")
    local monster = SpawnPrefab("tentacle_frost")
 
     light1.Transform:SetPosition(pos.x, pos.y, pos.z)
    monster.Transform:SetPosition(pos.x, pos.y, pos.z)
  
caster.components.sanity:DoDelta(-25)
    elseif caster.components.sanity.current < 40 then
	local fail1 = SpawnPrefab("statue_transition")
    local fail2 = SpawnPrefab("statue_transition_2")

    fail1.Transform:SetPosition(pos.x, pos.y, pos.z)
    fail2.Transform:SetPosition(pos.x, pos.y, pos.z)
    end
end

local function yellow_reticuletargetfn()
    return Vector3(ThePlayer.entity:LocalToWorldSpace(5, 0, 0))
end

local function sanity_cost(inst, owner)
    if owner.components.sanity and inst.cost_on then
        owner.components.sanity:DoDelta(-1,false)
		inst.SoundEmitter:PlaySound("dontstarve/common/gem_shatter")
    end
	
end

local function Release_Frost(inst,owner, data)
	inst.boost = false
	inst.components.talker:Say("-[Release Frost]\nSpeed Down(50)\nSummon monster\nCooling Body heat")
	inst.components.equippable.walkspeedmult = 0.5
     inst:AddComponent("heater")
    inst.components.heater:SetThermics(false, true)
    inst.components.heater.equippedheat = -10
	inst.SoundEmitter:PlaySound("dontstarve/common/gem_shatter")
	 	     inst:AddComponent("reticule")
    inst.components.reticule.targetfn = yellow_reticuletargetfn
    inst.components.reticule.ease = true
    inst:AddComponent("spellcaster")
    inst.components.spellcaster:SetSpellFn(summoning)
    inst.components.spellcaster.canuseonpoint = true
 
    MakeHauntableLaunch(inst)
    AddHauntableCustomReaction(inst, function(inst, haunter)
        if math.random() <= TUNING.HAUNT_CHANCE_RARE then
            local pos = Vector3(inst.Transform:GetWorldPosition())
            local start_angle = math.random()*2*PI
            local offset = FindWalkableOffset(pos, start_angle, math.random(3,12), 60, false, true)
            local pt = pos + offset
            createlight(inst, nil, pt)
            inst.components.hauntable.hauntvalue = TUNING.HAUNT_LARGE
            return true
        end
        return false
    end, true, false, true)
	 end
	 
local function Preserved_Frost(inst, data)
	inst.boost = true
	inst.components.talker:Say("-[Preserved Frost]\nSpeed Down(30)\nFreeze Hit")
    inst.components.equippable.walkspeedmult = 0.7
       inst:RemoveComponent("spellcaster")
      inst:RemoveComponent("heater")
	  end 
	  
	
local function OnOpen(inst)
  inst.SoundEmitter:PlaySound("dontstarve/common/gem_shatter")
	Release_Frost(inst)
	inst.boost = false
	inst.cost_on = true
		end 

local function OnClose(inst) 
  inst.SoundEmitter:PlaySound("dontstarve/common/fireOut")
	Preserved_Frost(inst)
	inst.boost = true
	inst.cost_on = false
    end
 
local function OnDropped(inst,data)
    end
local function OnPutInInventory(inst)
    Upgradedamage(inst)
    end 
	
local function onequip(inst, owner) 
 owner.frost = true
    Upgradedamage(inst)
   inst.task2 = inst:DoPeriodicTask(3, function() sanity_cost(inst, owner) end)
    owner.AnimState:OverrideSymbol("swap_object", "swap_frosthammer", "swap_umbrella")
    owner.AnimState:Show("ARM_carry") 
    owner.AnimState:Hide("ARM_normal") 
    --[[if inst.components.container ~= nil then
        inst.components.container:Open(owner)
    end]]
end

local function onunequip(inst, owner) 
owner.frost = false
inst.cost_on = false
if inst.task2 then inst.task2:Cancel() inst.task2 = nil end
    Upgradedamage(inst)
    inst:RemoveComponent("spellcaster")
    if inst.task then inst.task:Cancel() inst.task = nil end
     owner.AnimState:Hide("ARM_carry") 
    owner.AnimState:Show("ARM_normal") 
    if inst.components.container ~= nil then
        inst.components.container:Close(owner)
    end
end
 
--[[
local slotpos_2 = {}

for y = 0, 1 do
table.insert(slotpos_2, Vector3(-126, -y*75 + 114 ,-126 +75, -y*75 + 114 ))

	--table.insert(slotpos_2, Vector3(-162, -y*75 + 114 ,0))
	--table.insert(slotpos_2, Vector3( -162 +75, -y*75 + 114 ,0))

end]]
--[[
local function itemtest(inst, item, slot)
	if not item:HasTag("tstonehammer") then
			return true
end
end]]

local function fn()
local inst = CreateEntity()
	
	inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

	MakeInventoryPhysics(inst)  
    inst.AnimState:SetBank("umbrella")
    inst.AnimState:SetBuild("frosthammer")
    inst.AnimState:PlayAnimation("idle")
	
    inst.entity:AddMiniMapEntity()
	inst.MiniMapEntity:SetIcon( "frosthammer.tex" )
	
   inst:AddComponent("talker")
    inst.components.talker.fontsize = 20
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.colour = Vector3(0.7, 0.85, 1, 1)
    inst.components.talker.offset = Vector3(200,-250,0)
    inst.components.talker.symbol = "swap_object"

    inst:AddComponent("tool")
    inst.components.tool:SetAction(ACTIONS.HAMMER)
    inst.OnLoad = OnLoad
	    inst:AddComponent("waterproofer")
    inst.components.waterproofer.effectiveness = 0

     inst:AddComponent("weapon")
    inst.components.weapon:SetOnAttack(Upgradedamage)
	inst.components.weapon:SetOnAttack(onattack_FROST)
    inst.components.weapon:SetRange(1.55)
	
    if not TheWorld.ismastersim then
		inst:DoTaskInTime(0, function()	inst.replica.container:WidgetSetup("frostsmall") end)
		return inst
	end
inst.entity:SetPristine()
	-------
    
    inst:AddComponent("inspectable")
    inst:AddComponent("inventoryitem")
  --inst.components.inventoryitem:SetOnDroppedFn(OnDropped)
    inst.components.inventoryitem:SetOnPutInInventoryFn(OnPutInInventory)
    	inst.components.inventoryitem.atlasname = "images/inventoryimages/frosthammer.xml"
 
   ---------------------  
   inst:AddTag("fridge")
  	inst:AddTag("lowcool")
         inst:AddTag("backpack")
	       inst:AddTag("waterproofer")
     ----------------------
     inst:AddComponent("container")
     inst.components.container:WidgetSetup("frostsmall")
     inst.components.container.onopenfn = OnOpen
     inst.components.container.onclosefn = OnClose

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip( onequip )
    inst.components.equippable:SetOnUnequip( onunequip )

------------------------

        inst:AddComponent("fueled")
       inst.components.fueled.fueltype = "CHEMICAL"
       inst.components.fueled:InitializeFuelLevel(30000000)
		
       inst.components.fueled:SetDepletedFn(OnDurability)
        inst.components.fueled.ontakefuelfn = TakeItem
        inst.components.fueled.accepting = true
inst.components.fueled:StopConsuming()        
inst.boost = true 

	inst.level = 0
inst:ListenForEvent("levelup", levelexp)
	inst.OnSave = onsave
	inst.OnPreLoad = onpreload
	inst.components.equippable.walkspeedmult = 0.8
    inst.components.equippable.runspeedmult = 0.8
	
    MakeHauntableLaunch(inst)
 
    return inst
end


return Prefab( "common/inventory/frosthammer", fn, assets, prefabs) 
