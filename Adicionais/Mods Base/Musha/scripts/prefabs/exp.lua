local assets=
{
	Asset("ANIM", "anim/exp.zip"),
  Asset("ATLAS", "images/inventoryimages/exp.xml"),
  Asset("IMAGE", "images/inventoryimages/exp.tex"),
}

local function item_oneaten(inst, eater)
if eater:HasTag("musha") then
eater.stamina = eater.stamina + 10
eater.level = eater.level + 100
eater.music = eater.music + 100
eater.fatigue = eater.fatigue - 10
end end

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    MakeInventoryPhysics(inst)
 	inst.entity:AddNetwork()

    inst.entity:AddSoundEmitter()
    
    inst.AnimState:SetBank("bulb")
    inst.AnimState:SetBuild("exp")
    inst.AnimState:PlayAnimation("idle")
    inst.AnimState:SetMultColour(1, 1, 1, 0.3)

    local light = inst.entity:AddLight()
    light:SetFalloff(0.3)
    light:SetIntensity(.3)
    light:SetRadius(0.3)
    light:SetColour(120/255, 120/255, 150/255)
    light:Enable(true)
    inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
	
	  --     inst.entity:AddMiniMapEntity()
	--inst.MiniMapEntity:SetIcon( "exp.tex" )
	
	inst.entity:SetPristine() 
          	if not TheWorld.ismastersim then
   return inst
end	

    inst:AddComponent("tradable")
	
	    inst:AddComponent("edible")
    inst.components.edible.foodtype = FOODTYPE.VEGGIE
    inst.components.edible.healthvalue = 30
    inst.components.edible.hungervalue = 5
    inst.components.edible.sanityvalue = 30
    inst.components.edible:SetOnEatenFn(item_oneaten)


    inst:AddComponent("stackable")
	inst.components.stackable.maxsize = TUNING.STACK_SIZE_SMALLITEM

    inst:AddComponent("fuel")
    inst.components.fuel.fuelvalue = 40000000
    inst.components.fuel.fueltype = "CHEMICAL"

    inst:AddComponent("inspectable")

    inst:AddComponent("bait")
    inst:AddTag("molebait")
    
    inst:AddComponent("inventoryitem")
inst.components.inventoryitem.atlasname = "images/inventoryimages/exp.xml"
 

    return inst
end

return Prefab( "common/inventory/exp", fn, assets) 

