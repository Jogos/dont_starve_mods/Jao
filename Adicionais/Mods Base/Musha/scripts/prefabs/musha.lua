local MakePlayerCharacter = require "prefabs/player_common"
--local easing = require("easing")
local assets = {

        Asset( "ANIM", "anim/player_basic.zip" ),
        Asset( "ANIM", "anim/player_idles_shiver.zip" ),
        Asset( "ANIM", "anim/player_actions.zip" ),
        Asset( "ANIM", "anim/player_actions_axe.zip" ),
        Asset( "ANIM", "anim/player_actions_pickaxe.zip" ),
        Asset( "ANIM", "anim/player_actions_shovel.zip" ),
        Asset( "ANIM", "anim/player_actions_blowdart.zip" ),
        Asset( "ANIM", "anim/player_actions_eat.zip" ),
        Asset( "ANIM", "anim/player_actions_item.zip" ),
        Asset( "ANIM", "anim/player_actions_uniqueitem.zip" ),
        Asset( "ANIM", "anim/player_actions_bugnet.zip" ),
        Asset( "ANIM", "anim/player_actions_fishing.zip" ),
        Asset( "ANIM", "anim/player_actions_boomerang.zip" ),
        Asset( "ANIM", "anim/player_bush_hat.zip" ),
        Asset( "ANIM", "anim/player_attacks.zip" ),
        Asset( "ANIM", "anim/player_idles.zip" ),
        Asset( "ANIM", "anim/player_rebirth.zip" ),
        Asset( "ANIM", "anim/player_jump.zip" ),
        Asset( "ANIM", "anim/player_amulet_resurrect.zip" ),
        Asset( "ANIM", "anim/player_teleport.zip" ),
        Asset( "ANIM", "anim/wilson_fx.zip" ),
        Asset( "ANIM", "anim/player_one_man_band.zip" ),
        Asset( "ANIM", "anim/shadow_hands.zip" ),
        Asset( "SOUND", "sound/sfx.fsb" ),
        Asset( "SOUND", "sound/wilson.fsb" ),
        Asset( "ANIM", "anim/beard.zip" ),

        Asset( "ANIM", "anim/musha_battle.zip" ),
        Asset( "ANIM", "anim/musha.zip" ),
        Asset( "ANIM", "anim/musha_normal.zip" ),
	Asset( "ANIM", "anim/musha_hunger.zip" ),
	Asset( "ANIM", "anim/ghost_musha_build.zip" ),

}
local prefabs = {}

local start_inv = 
{
	--"mushasword_frost",
	--"mushasword",
   --"broken_frosthammer",
    --"frosthammer",
	--"armor_mushaa",	
	--"armor_mushab",
    -- "hat_mprincess",
--	"mushasword_base",
--	"musha_flute",
	"musha_egg",
	--[["musha_egg1",
	"musha_egg2",
	"musha_egg3",
	"musha_eggs1",
	"musha_eggs2",
	"musha_eggs3",
	"musha_egg8",	]]
 --   "musha_small",

	"glowdust",
	"glowdust",
	"glowdust",
--	"cristal",
	"hat_mprincess"
}

			
local function stamina_sleep(inst,data)
local max_stamina = 100
local stamina = math.min(inst.stamina, max_stamina)
 end
local function fatigue_sleep(inst,data)
local max_fatigue = 100
local fatigue = math.min(inst.fatigue, max_fatigue)
 end
local function fullcharged_music(inst)
local max_music = 100
local music = math.min(inst.music, max_music)

end 
-----------------------------

local function consume_stamina(inst, data)
if not inst.No_Sleep_Princess then
if not inst.sleep_on and not inst.tiny_sleep and not inst:HasTag("playerghost") then
    if TheWorld.state.isday and inst.stamina >0 then
 inst.stamina = inst.stamina - 0.005
elseif TheWorld.state.isdusk and inst.stamina >0 then
 inst.stamina = inst.stamina - 0.01
elseif TheWorld.state.isnight and inst.stamina >0 then
 inst.stamina = inst.stamina - 0.02
end end
if not (inst.tiny_sleep or inst.sleep_on) and inst.fatigue <100 and (inst.sg:HasStateTag("attack") or inst.sg:HasStateTag("moving")) and not inst:HasTag("playerghost") then
inst.fatigue = inst.fatigue + 0.1
end
if not (inst.tiny_sleep or inst.sleep_on) and inst.fatigue <100 and ( inst.sg:HasStateTag("chopping") or inst.sg:HasStateTag("mining") or  inst.sg:HasStateTag("hammering") or inst.sg:HasStateTag("digging") or inst.sg:HasStateTag("netting") or inst.sg:HasStateTag("fishing") or inst.sg:HasStateTag("read")) and not inst:HasTag("playerghost") then
inst.fatigue = inst.fatigue + 0.15
if inst.sneak_pang then		
		inst.components.talker:Say("Unhide!")	
		inst.components.colourtweener:StartTween({1,1,1,1}, 0)
			local fx = SpawnPrefab("statue_transition_2")
      fx.entity:SetParent(inst.entity)
	  fx.Transform:SetScale(1.2, 1.2, 1.2)
      fx.Transform:SetPosition(0, 0, 0)
		inst.components.sanity:DoDelta(80)
	inst.sneak_pang = false		
	inst:RemoveTag("notarget")
		end
end 
if not (inst.tiny_sleep or inst.sleep_on) and inst.fatigue >0 and not (inst.sg:HasStateTag("moving") or inst.sg:HasStateTag("attack") or inst.sg:HasStateTag("chopping") or inst.sg:HasStateTag("mining") or  inst.sg:HasStateTag("hammering") or inst.sg:HasStateTag("digging") or inst.sg:HasStateTag("netting") or inst.sg:HasStateTag("fishing")) and not inst:HasTag("playerghost") then
inst.fatigue = inst.fatigue - 0.15
end
if inst.stamina > 0 then
if inst.fatigue >=10 and inst.fatigue <20 and not inst.sleep_on and not inst.tiny_sleep then
inst.stamina = inst.stamina - 0.01
elseif inst.fatigue >=20 and inst.fatigue <30 and not inst.sleep_on and not inst.tiny_sleep then
inst.stamina = inst.stamina - 0.04
elseif inst.fatigue >=30 and inst.fatigue <40 and not inst.sleep_on and not inst.tiny_sleep then
inst.stamina = inst.stamina - 0.08
elseif inst.fatigue >=40 and inst.fatigue <50 and not inst.sleep_on and not inst.tiny_sleep then
inst.stamina = inst.stamina - 0.12
elseif inst.fatigue >=50 and inst.fatigue <60 and not inst.sleep_on and not inst.tiny_sleep then
inst.stamina = inst.stamina - 0.16
elseif inst.fatigue >=60 and inst.fatigue <70 and not inst.sleep_on and not inst.tiny_sleep then
inst.stamina = inst.stamina - 0.2
elseif inst.fatigue >=70 and inst.fatigue <80 and not inst.sleep_on and not inst.tiny_sleep then
inst.stamina = inst.stamina - 0.24
elseif inst.fatigue >=80 and inst.fatigue <90 and not inst.sleep_on and not inst.tiny_sleep then
inst.stamina = inst.stamina - 0.28
elseif inst.fatigue >=90 and not inst.sleep_on and not inst.tiny_sleep then
inst.stamina = inst.stamina - 0.32
end 
end
if inst.tiny_sleep then
inst.components.health:StartRegen(1, 30)
end
if inst.tiny_sleep and inst.stamina < 100 then
inst.stamina = inst.stamina + 0.25
end
if inst.tiny_sleep and inst.fatigue > 0 then
inst.fatigue = inst.fatigue - 0.8
end end 
end

local function check_music(inst)
local chrged_time = 2
local chrging_timer = 0
	inst.time_perfomance = inst:DoPeriodicTask(0.1,function(inst)
		chrging_timer = chrging_timer + 1
		if chrging_timer > chrged_time and inst.music < 100 then
		inst.music = inst.music + 1
			fullcharged_music(inst)
		end
		if inst.time_perfomance and (chrging_timer > chrged_time)  then
			inst.time_perfomance:Cancel()
			inst.time_perfomance = nil
		end	end)
	end

local function sleep_test(inst, data)
    local x, y, z = inst.Transform:GetWorldPosition() 
    local ents = TheSim:FindEntities(x, y, z, 4, { "fire" })
    for i, v in ipairs(ents) do
        if v.components.burnable:IsBurning() then
		inst.warm_on = true
    elseif v.components.burnable:Extinguish() then
		inst.warm_on = false
        end
     end
if inst.No_Sleep_Princess then 
inst.stamina = inst.stamina * 0 + 100
inst.fatigue = inst.fatigue * 0
end	 
	 if not inst.No_Sleep_Princess then
--[[if (inst.sleep_on or inst.tiny_sleep) and inst.components.health:IsHurt() or inst.components.hunger.current == 0 then
inst.sleep_on = false
inst.sleepcheck = false
inst.sg.statemem.iswaking = true
inst.sg:GoToState("wakeup")
end]]
if inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")  then
inst.sleep_on = true
inst.sleepcheck = true
end
if inst.sg:HasStateTag("moving") then
inst.sleep_on = false
inst.tiny_sleep = false
end
if inst.sleep_on then 
inst.sleepcheck = true
inst.switch = false
inst.active_valkyrie = false
inst.components.health:StartRegen(1, 15)
if inst.AnimState:AnimDone() then
inst.sg:AddStateTag("sleeping")
inst.sg:AddStateTag("busy")
--inst.components.sleeper:IsAsleep()
inst.AnimState:PlayAnimation("bedroll_sleep_loop", true)
end
end
if not inst.sleep_on and inst.sleepcheck then
inst.sleepcheck = false
end 
--[[
if inst.components.sleeper:WakeUp() then
inst.sleep_on = false
inst.sleepcheck = false
inst.tiny_sleep = false
end]]
    local x, y, z = inst.Transform:GetWorldPosition() 
    local ents = TheSim:FindEntities(x, y, z, 15, { "monster" })
    for i, v in ipairs(ents) do
	if not (v:AddTag("tree") or v:AddTag("birchnut") or v:AddTag("deciduoustree")) then
    if v.components.health and not v.components.health:IsDead() then
		inst.sleep_no = true
    elseif v.components.health and v.components.health:IsDead() then
		inst.sleep_no = false
		end
	        end end
     
--perfomance
--old 
--[[
if inst.sleep_on and inst.stamina <= 75 and not inst.charging_music then
inst.charging_music = true
end
if inst.charging_music and inst.stamina >=100 then
inst.switlight = true inst.music_check = true inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" ) 
inst.charging_music = false
end ]]
---------new
if (inst.sleep_on or inst.tiny_sleep) then
inst.charging_music = true
elseif not (inst.sleep_on or inst.tiny_sleep) then
inst.charging_music = false
end end
end

 local function onsleepheal(inst)
 if inst.warm_on then
    if inst.sleep_on then
        inst.components.health:DoDelta(0.5)
		  inst.components.sanity:DoDelta(0.5)
		
				end
end
end

local function on_fatigue(inst, data)
if not (inst.sleep_on or inst.tiny_sleep) then
inst.components.talker.colour = Vector3(1, 1, 1, 1)
inst.components.talker.fontsize = 28
end
if not inst.No_Sleep_Princess then
if inst.sleep_no then
scheduler:ExecuteInTime(8, function() inst.sleep_no = false end) 
end
if inst.warm_on then
scheduler:ExecuteInTime(8, function() inst.warm_on = false end) 
end
if inst.sleep_on and inst.stamina < 100 then
inst.stamina = inst.stamina + 1 
end
if inst.sleep_on and inst.fatigue > 0 then
inst.fatigue = inst.fatigue - 2
end

if inst.strength == "valkyrie" and not inst.sleep_on and not inst.tiny_sleep and inst.fatigue < 100 then
inst.fatigue = inst.fatigue + 0.5
elseif inst.strength == "berserk" and not inst.sleep_on and not inst.tiny_sleep and inst.fatigue < 100 then
inst.fatigue = inst.fatigue + 0.1
end
if inst.sneak_pang and inst.fatigue < 100 then 
inst.fatigue = inst.fatigue + 0.8
end
if inst.on_sparkshield and inst.stamina < 100 then
inst.fatigue = inst.fatigue + 1
end
if not (inst.sleep_on or inst.tiny_sleep) then
--inst.components.sleeper:WakeUp()
end

--dizzy
local dizzy = 0.1
if inst.stamina <= 30 and not inst.dizzy and math.random() < dizzy and not inst.sg:HasStateTag("attack") and not inst:HasTag("playerghost") then
	inst.dizzy = true
if math.random() < dizzy then
inst.components.talker:Say("feel so dizzy..")
elseif math.random() < dizzy then
inst.components.talker:Say("Musha so tired..")
elseif math.random() < dizzy then
inst.components.talker:Say("Musha fell asleep forever..")
elseif math.random() < dizzy then
inst.components.talker:Say("Musha, almost died..")
end
	inst:DoTaskInTime( 0, function() 
scheduler:ExecuteInTime(1, function() inst.sg:GoToState("knockout") inst.tiny_sleep = true end)
inst.sg:AddStateTag("busy")
	inst:DoTaskInTime( 150, function() inst.dizzy = false end) end)
 end end
end


-- passive skills
-- critical hit
local function on_Critical_1(inst, data)
local hitcriticalchance1 = 0.15
local other = data.target
    if math.random() < hitcriticalchance1 and inst.components.hunger.current > 30 then
    SpawnPrefab("explode_small").Transform:SetPosition(other:GetPosition():Get())
	 inst.SoundEmitter:PlaySound("dontstarve/tentacle/tentacle_emerge")
	other.components.health:DoDelta(-30)
			inst:RemoveEventCallback("onhitother", on_Critical_1) end end
local function on_Critical_2(inst, data)
local hitcriticalchance2 = 0.18
local other = data.target
    if math.random() < hitcriticalchance2 and inst.components.hunger.current > 30 then
    SpawnPrefab("explode_small").Transform:SetPosition(other:GetPosition():Get())
	other.components.health:DoDelta(-34)
			inst:RemoveEventCallback("onhitother", on_Critical_2) end end 
local function on_Critical_3(inst, data)
local hitcriticalchance3 = 0.22
local other = data.target
    if math.random() < hitcriticalchance3 and inst.components.hunger.current > 30 then
    SpawnPrefab("explode_small").Transform:SetPosition(other:GetPosition():Get())
	other.components.health:DoDelta(-38)
			inst:RemoveEventCallback("onhitother", on_Critical_3) end end 
local function on_Critical_4(inst, data)	
local hitcriticalchance4 = 0.26		
local other = data.target
    if math.random() < hitcriticalchance4 and inst.components.hunger.current > 30 then
    SpawnPrefab("explode_small").Transform:SetPosition(other:GetPosition():Get())
	other.components.health:DoDelta(-42)
			inst:RemoveEventCallback("onhitother", on_Critical_4) end end 
local function on_Critical_5(inst, data)	
local hitcriticalchance5 = 0.3		
local other = data.target
    if math.random() < hitcriticalchance5 and inst.components.hunger.current > 30 then
    SpawnPrefab("explode_small").Transform:SetPosition(other:GetPosition():Get())
	other.components.health:DoDelta(-46)
			inst:RemoveEventCallback("onhitother", on_Critical_5) end end 
local function on_Critical_6(inst, data)	
local hitcriticalchance6 = 0.34		
local other = data.target
    if math.random() < hitcriticalchance6 and inst.components.hunger.current > 30 then
    SpawnPrefab("explode_small").Transform:SetPosition(other:GetPosition():Get())
	other.components.health:DoDelta(-50)
			inst:RemoveEventCallback("onhitother", on_Critical_6) end end
local function on_Critical_7(inst, data)		
local hitcriticalchance7 = 0.38	
local other = data.target
    if math.random() < hitcriticalchance7 and inst.components.hunger.current > 30 then
    SpawnPrefab("explode_small").Transform:SetPosition(other:GetPosition():Get())
	other.components.health:DoDelta(-55)
			inst:RemoveEventCallback("onhitother", on_Critical_7) end end			
	local function Critical_level_1(inst)
   	inst:ListenForEvent("onhitother", on_Critical_1) end
	local function Critical_level_2(inst)
   	inst:ListenForEvent("onhitother", on_Critical_2) end
	local function Critical_level_3(inst)
   	inst:ListenForEvent("onhitother", on_Critical_3) end
	local function Critical_level_4(inst)
   	inst:ListenForEvent("onhitother", on_Critical_4) end
	local function Critical_level_5(inst)
   	inst:ListenForEvent("onhitother", on_Critical_5) end
	local function Critical_level_6(inst)
   	inst:ListenForEvent("onhitother", on_Critical_6) end
	local function Critical_level_7(inst)
   	inst:ListenForEvent("onhitother", on_Critical_7) end


	--Electric Shield (sanity shield)
local function on_Lshield_1(inst, attacked)
   local Lshield1 = 0.2
   if math.random() < Lshield1 and inst.components.sanity.current >= 1 and inst.components.health:GetPercent() < .95 and not inst.components.health:IsDead() then
	SpawnPrefab("shock_fx").Transform:SetPosition(inst:GetPosition():Get())
	inst.SoundEmitter:PlaySound("dontstarve/common/lightningrod")
        inst.components.health:DoDelta(5)
        inst.components.sanity:DoDelta(-1)
		inst:RemoveEventCallback("attacked", on_Lshield_1)
	end end
local function on_Lshield_2(inst, attacked)
   local Lshield2 = 0.24
   if math.random() < Lshield2 and inst.components.sanity.current >= 1 and inst.components.health:GetPercent() < .96 and not inst.components.health:IsDead() then
	SpawnPrefab("shock_fx").Transform:SetPosition(inst:GetPosition():Get())
	inst.SoundEmitter:PlaySound("dontstarve/common/lightningrod")
        inst.components.health:DoDelta(7)
        inst.components.sanity:DoDelta(-1)
		inst:RemoveEventCallback("attacked", on_Lshield_2)
	end end
local function on_Lshield_3(inst, attacked)
   local Lshield3 = 0.28
   if math.random() < Lshield3 and inst.components.sanity.current >= 1 and inst.components.health:GetPercent() < .97 and not inst.components.health:IsDead() then
	SpawnPrefab("shock_fx").Transform:SetPosition(inst:GetPosition():Get())
	inst.SoundEmitter:PlaySound("dontstarve/common/lightningrod")
        inst.components.health:DoDelta(9)
        inst.components.sanity:DoDelta(-1)
		inst:RemoveEventCallback("attacked", on_Lshield_3)
	end end
local function on_Lshield_4(inst, attacked)
   local Lshield4 = 0.32
   if math.random() < Lshield4 and inst.components.sanity.current >= 1 and inst.components.health:GetPercent() < .98 and not inst.components.health:IsDead() then
	SpawnPrefab("shock_fx").Transform:SetPosition(inst:GetPosition():Get())
	inst.SoundEmitter:PlaySound("dontstarve/common/lightningrod")
        inst.components.health:DoDelta(12)
        inst.components.sanity:DoDelta(-1)
		inst:RemoveEventCallback("attacked", on_Lshield_4)
	end end	
local function on_Lshield_5(inst, attacked)
   local Lshield5 = 0.36
   if math.random() < Lshield5 and inst.components.sanity.current >= 1 and inst.components.health:GetPercent() < .98 and not inst.components.health:IsDead() then
	SpawnPrefab("shock_fx").Transform:SetPosition(inst:GetPosition():Get())
	inst.SoundEmitter:PlaySound("dontstarve/common/lightningrod")
        inst.components.health:DoDelta(15)
        inst.components.sanity:DoDelta(-1)
		inst:RemoveEventCallback("attacked", on_Lshield_5)
	end end		
    local function Lshield_level_1(inst)
    inst:ListenForEvent("attacked", on_Lshield_1)
	end
    local function Lshield_level_2(inst)
    inst:ListenForEvent("attacked", on_Lshield_2)
	end
    local function Lshield_level_3(inst)
    inst:ListenForEvent("attacked", on_Lshield_3)
	end
    local function Lshield_level_4(inst)
    inst:ListenForEvent("attacked", on_Lshield_4)
	end
	local function Lshield_level_5(inst)
    inst:ListenForEvent("attacked", on_Lshield_5)
	end	

	--auto shadow lightning strike 
local function on_hitLightnings_1(inst, data)
inst.vl1 = false
	local other = data.target
	if other and other.components.health and not other:HasTag("wall") and not other:HasTag("structure")  then
			SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
        	SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
			other.components.health:DoDelta(-5)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_1)
	elseif other:HasTag("wall") or other:HasTag("structure") then
            SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
			SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
	        SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.health:DoDelta(-5)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_1)
	end end
local function on_hitLightnings_2(inst, data)
inst.vl2 = false
	local other = data.target
	if other and other.components.health and not other:HasTag("wall") and not other:HasTag("structure")  then
			SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
        	SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
			other.components.health:DoDelta(-10)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_2)
	elseif other:HasTag("wall") or other:HasTag("structure") then
            SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
			SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
	        SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.health:DoDelta(-10)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_2)
	end end
local function on_hitLightnings_3(inst, data)
inst.vl3 = false
	local other = data.target
	if other and other.components.health and not other:HasTag("wall") and not other:HasTag("structure")  then
			SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
        	SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
			other.components.health:DoDelta(-15)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_3)
	elseif other:HasTag("wall") or other:HasTag("structure") then
            SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
			SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
	        SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.health:DoDelta(-15)
 inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_3)
	end end
local function on_hitLightnings_4(inst, data)
	local other = data.target
	inst.vl4 = false
	if other and other.components.health and not other:HasTag("wall") and not other:HasTag("structure")  then
			SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
        	SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
			other.components.health:DoDelta(-20)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_4)
	elseif other:HasTag("wall") or other:HasTag("structure") then
            SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
			SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
	        SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.health:DoDelta(-20)
 inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_4)
	end 
	end
local function on_hitLightnings_5(inst, data)
	local other = data.target
	inst.vl5 = false
	if other and other.components.health and not other:HasTag("wall") and not other:HasTag("structure")  then
			SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
        	SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
			other.components.health:DoDelta(-25)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_5)
	elseif other:HasTag("wall") or other:HasTag("structure") then
            SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
			SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
	        SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.health:DoDelta(-25)
 inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_5)
	end 
	end
local function on_hitLightnings_6(inst, data)
	local other = data.target
	inst.vl6 = false
	if other and other.components.health and not other:HasTag("wall") and not other:HasTag("structure")  then
			SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
        	SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
			other.components.health:DoDelta(-30)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_6)
	elseif other:HasTag("wall") or other:HasTag("structure") then
            SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
			SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
	        SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.health:DoDelta(-30)
 inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_6)
	end 
	end
local function on_hitLightnings_7(inst, data)
	local other = data.target
	inst.vl7 = false
	if other and other.components.health and not other:HasTag("wall") and not other:HasTag("structure")  then
			SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
        	SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
			other.components.health:DoDelta(-35)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_7)
	elseif other:HasTag("wall") or other:HasTag("structure") then
            SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
			SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
	        SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.health:DoDelta(-35)
 inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_7)
	end 
	end
local function on_hitLightnings_8(inst, data)
	local other = data.target
	inst.vl8 = false
	if other and other.components.health and not other:HasTag("wall") and not other:HasTag("structure")  then
			SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
        	SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
			other.components.health:DoDelta(-40)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_8)
	elseif other:HasTag("wall") or other:HasTag("structure") then
            SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
			SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
	        SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.health:DoDelta(-40)
 inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_8)
	end 
	end
	
local function on_hitLightnings_9(inst, data)
	local other = data.target
	inst.vl8 = false
	if other and other.components.health and not other:HasTag("wall") and not other:HasTag("structure")  then
			SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
        	SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
			other.components.health:DoDelta(-45)
inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_9)
	elseif other:HasTag("wall") or other:HasTag("structure") then
            SpawnPrefab("lightning").Transform:SetPosition(other:GetPosition():Get())
			SpawnPrefab("shock_fx").Transform:SetPosition(other:GetPosition():Get())
	        SpawnPrefab("collapse_small").Transform:SetPosition(other:GetPosition():Get())
		other.components.health:DoDelta(-45)
 inst.components.combat:SetRange(2)
inst.AnimState:SetBloomEffectHandle( "" )
inst:RemoveEventCallback("onhitother", on_hitLightnings_9)
	end 
	end
	
  local function Lightnings_level_1(inst)
  local randomL = 0.05
if inst.active_valkyrie and inst.stamina > 0 and not inst.on_hitLightnings and math.random() < randomL then
	inst.on_hitLightnings = true inst.vl1 = true
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
	inst:DoTaskInTime( 0, function() 
	inst:ListenForEvent("onhitother", on_hitLightnings_1)
	inst.components.combat:SetRange(8)  
	inst:DoTaskInTime( 12, function() inst.on_hitLightnings = false end) end)
 end end
  local function Lightnings_level_2(inst)
  local randomL = 0.05
if inst.active_valkyrie and inst.stamina > 0 and not inst.on_hitLightnings and math.random() < randomL then
	inst.on_hitLightnings = true inst.vl2 = true
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
	inst:DoTaskInTime( 0, function() 
	inst:ListenForEvent("onhitother", on_hitLightnings_2)
	inst.components.combat:SetRange(9)  
	inst:DoTaskInTime( 11, function() inst.on_hitLightnings = false end) end)
 end end
  local function Lightnings_level_3(inst)
  local randomL = 0.1
if inst.active_valkyrie and inst.stamina > 0 and not inst.on_hitLightnings and math.random() < randomL then
	inst.on_hitLightnings = true inst.vl3 = true
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
	inst:DoTaskInTime( 0, function() 
	inst:ListenForEvent("onhitother", on_hitLightnings_3)
	inst.components.combat:SetRange(10) 
	inst:DoTaskInTime( 10, function() inst.on_hitLightnings = false end) end)
 end end
  local function Lightnings_level_4(inst)
  local randomL = 0.15
if inst.active_valkyrie and inst.stamina > 0 and not inst.on_hitLightnings and math.random() < randomL then
	inst.on_hitLightnings = true inst.vl4 = true
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
	inst:DoTaskInTime( 0, function() 
	inst:ListenForEvent("onhitother", on_hitLightnings_4)
	inst.components.combat:SetRange(11) 
	inst:DoTaskInTime( 9, function() inst.on_hitLightnings = false end) end)
 end end
 local function Lightnings_level_5(inst)
 local randomL = 0.1
if inst.active_valkyrie and inst.stamina > 0 and not inst.on_hitLightnings and math.random() < randomL then
	inst.on_hitLightnings = true inst.vl5 = true
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
	inst:DoTaskInTime( 0, function() 
	inst:ListenForEvent("onhitother", on_hitLightnings_5)
	inst.components.combat:SetRange(12) 
	inst:DoTaskInTime( 8, function() inst.on_hitLightnings = false end) end)
 end end
 local function Lightnings_level_6(inst)
 local randomL = 0.2
if inst.active_valkyrie and inst.stamina > 0 and not inst.on_hitLightnings and math.random() < randomL then
	inst.on_hitLightnings = true inst.vl6 = true
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())	
	inst:DoTaskInTime( 0, function() 
	inst:ListenForEvent("onhitother", on_hitLightnings_6)
	inst.components.combat:SetRange(13) 
	inst:DoTaskInTime( 7, function() inst.on_hitLightnings = false end) end)
 end end
 local function Lightnings_level_7(inst)
 local randomL = 0.25
if inst.active_valkyrie and inst.stamina > 0 and not inst.on_hitLightnings and math.random() < randomL then
	inst.on_hitLightnings = true inst.vl7 = true
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
	inst:DoTaskInTime( 0, function() 
	inst:ListenForEvent("onhitother", on_hitLightnings_7)
	inst.components.combat:SetRange(14)  
	inst:DoTaskInTime( 6, function() inst.on_hitLightnings = false end) end)
 end end
 local function Lightnings_level_8(inst)
 local randomL = 0.3
if inst.active_valkyrie and inst.stamina > 0 and not inst.on_hitLightnings and math.random() < randomL then
	inst.on_hitLightnings = true inst.vl8 = true
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
	inst:DoTaskInTime( 0, function() 
	inst:ListenForEvent("onhitother", on_hitLightnings_8)
	inst.components.combat:SetRange(15) 
	inst:DoTaskInTime( 6, function() inst.on_hitLightnings = false end) end)
 end end
 local function Lightnings_level_9(inst)
 local randomL = 0.3
if inst.active_valkyrie and inst.stamina > 0 and not inst.on_hitLightnings and math.random() < randomL then
	inst.on_hitLightnings = true inst.vl8 = true
	inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
inst.SoundEmitter:PlaySound("dontstarve/maxwell/shadowmax_appear")
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
	inst:DoTaskInTime( 0, function() 
	inst:ListenForEvent("onhitother", on_hitLightnings_9)
	inst.components.combat:SetRange(15) 
	inst:DoTaskInTime( 5, function() inst.on_hitLightnings = false end) end)
 end end
--adds on shield

local function Sparkshield_heal(inst, attacked, data) 
if inst.level < 430  then
inst.components.health:DoDelta(2)
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
elseif inst.level >= 430 then
inst.components.health:DoDelta(3)
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
elseif inst.level >= 1880 then
inst.components.health:DoDelta(4)
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
elseif inst.level >= 7000 then
inst.components.health:DoDelta(5)
SpawnPrefab("sparks").Transform:SetPosition(inst:GetPosition():Get())
end end

	local function on_sparkshield_1( attacked, data)
if data.attacker.components.burnable and data.attacker.components.health and not data.attacker:HasTag("thorny") and not data.attacker:HasTag("gruef") then
            data.attacker.components.health:DoDelta(-4)
SpawnPrefab("sparks").Transform:SetPosition(data.attacker:GetPosition():Get())
end end
	local function on_sparkshield_2( attacked, data)
if data.attacker.components.burnable and data.attacker.components.health and not data.attacker:HasTag("thorny") and not data.attacker:HasTag("gruef") then
            data.attacker.components.health:DoDelta(-6)
SpawnPrefab("sparks").Transform:SetPosition(data.attacker:GetPosition():Get())
end end
	local function on_sparkshield_3( attacked, data)
if data.attacker.components.burnable and data.attacker.components.health and not data.attacker:HasTag("thorny") and not data.attacker:HasTag("gruef") then
            data.attacker.components.health:DoDelta(-8)
SpawnPrefab("sparks").Transform:SetPosition(data.attacker:GetPosition():Get())
end end
	local function on_sparkshield_4( attacked, data)
if data.attacker.components.burnable and data.attacker.components.health and not data.attacker:HasTag("thorny") and not data.attacker:HasTag("gruef") then
            data.attacker.components.health:DoDelta(-10)
SpawnPrefab("sparks").Transform:SetPosition(data.attacker:GetPosition():Get())
end end
		
	local function active_sparkshield(inst)
	if inst.level < 430 and inst.on_sparkshield then	
		inst:ListenForEvent("attacked", on_sparkshield_1)
		inst:ListenForEvent("attacked", Sparkshield_heal)
	elseif inst.level >= 430 and inst.level < 1880 and inst.on_sparkshield then	
		inst:ListenForEvent("attacked", on_sparkshield_2)
		inst:ListenForEvent("attacked", Sparkshield_heal)
	elseif inst.level >= 1880 and inst.level < 7000 and inst.on_sparkshield then	
		inst:ListenForEvent("attacked", on_sparkshield_3)
		inst:ListenForEvent("attacked", Sparkshield_heal)
	elseif inst.level >= 7000 and inst.on_sparkshield then	
		inst:ListenForEvent("attacked", on_sparkshield_4)
		inst:ListenForEvent("attacked", Sparkshield_heal)
	elseif not inst.on_sparkshield then	
		inst:RemoveEventCallback("attacked", Sparkshield_heal)
		inst:RemoveEventCallback("attacked", on_sparkshield_1)
		inst:RemoveEventCallback("attacked", on_sparkshield_2)
		inst:RemoveEventCallback("attacked", on_sparkshield_3)
		inst:RemoveEventCallback("attacked", on_sparkshield_4)
			end

if inst.charging_music and inst.music < 100 then
check_music(inst)
end
if inst.charging_music and inst.music >= 100 and (inst.sleep_on or inst.tiny_sleep) then	
inst.music_check = true inst.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" ) 
elseif inst.music < 100 then
inst.music_check = false 
end end
	-- valkyrie armor 

		------------------    
	local function On_frameshield_1(attacked, data) -- 
        if data.attacker.components.burnable and data.attacker.components.health and data.attacker.components.burnable and not data.attacker:HasTag("thorny") and not data.attacker:HasTag("gruef") and not data.attacker.burn then
            data.attacker.components.health:DoDelta(-3)
            data.attacker.components.burnable:Ignite()
		end
    if data.attacker.components.burnable and data.attacker.components.burnable:IsBurning() then
        data.attacker.components.burnable:Extinguish()
        end 
		 data.attacker.burn = true
		end
	local function On_frameshield_2(attacked, data)  --
        if data.attacker.components.burnable and data.attacker.components.health and data.attacker.components.burnable and not data.attacker:HasTag("thorny") and not data.attacker:HasTag("gruef") and not data.attacker.burn then
            data.attacker.components.health:DoDelta(-6)
            data.attacker.components.burnable:Ignite()
		end
    if data.attacker.components.burnable and data.attacker.components.burnable:IsBurning() then
        data.attacker.components.burnable:Extinguish()
        end 
		 data.attacker.burn = true
		end
	local function On_frameshield_3(attacked, data)  --
        if data.attacker.components.burnable and data.attacker.components.health and data.attacker.components.burnable and not data.attacker:HasTag("thorny") and not data.attacker:HasTag("gruef") and not data.attacker.burn then
            data.attacker.components.health:DoDelta(-9)
            data.attacker.components.burnable:Ignite()
		end
    if data.attacker.components.burnable and data.attacker.components.burnable:IsBurning() then
        data.attacker.components.burnable:Extinguish()
        end
		 data.attacker.burn = true
		end
	local function On_frameshield_4(attacked, data)  --
        if data.attacker.components.burnable and data.attacker.components.health and data.attacker.components.burnable and not data.attacker:HasTag("thorny") and not data.attacker:HasTag("gruef") and not data.attacker.burn then
            data.attacker.components.health:DoDelta(-12)
            data.attacker.components.burnable:Ignite()
		end
    if data.attacker.components.burnable and data.attacker.components.burnable:IsBurning() then
        data.attacker.components.burnable:Extinguish()
        end
		 data.attacker.burn = true
		end
		
	local function Off_frameshield(inst)
			inst:RemoveEventCallback("attacked", On_frameshield_1)
			inst:RemoveEventCallback("attacked", On_frameshield_2)
			inst:RemoveEventCallback("attacked", On_frameshield_3)
			inst:RemoveEventCallback("attacked", On_frameshield_4)
			end
		
--active flame shield and armor		
	   local function frameshield_1(inst)
	inst.valkyrie_armor_1 =true
	end	
	   local function frameshield_2(inst)
	inst.valkyrie_armor_1 =false
		inst.valkyrie_armor_2 =true
	end 
	   local function frameshield_3(inst)
	inst.valkyrie_armor_2 =false
			inst.valkyrie_armor_3 =true
	end	
	   local function frameshield_4(inst)
	inst.valkyrie_armor_3 =false
		inst.valkyrie_armor_4 =true
	end	

local function flameshield_active(inst)	
	if inst.valkyrie_turn and inst.valkyrie_armor_1 then
		inst:ListenForEvent("attacked", On_frameshield_1) end
	if inst.valkyrie_turn and inst.valkyrie_armor_2 then		
		inst:ListenForEvent("attacked", On_frameshield_2) end
	if inst.valkyrie_turn and inst.valkyrie_armor_3 then			
		inst:ListenForEvent("attacked", On_frameshield_3) end	
	if inst.valkyrie_turn and inst.valkyrie_armor_4 then	
		inst:ListenForEvent("attacked", On_frameshield_4) end
	
	end
	
	
	local Lightningo = .15
	
	
    local function on_Valkyrie_all(inst, attacked, data)
	
		if inst.on_Valkyrie_1 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then	
inst.valkyrie_turn = true 

	
	elseif inst.on_Valkyrie_2 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		scheduler:ExecuteInTime(7, function() Lightnings_level_1(inst) end)
				
inst.valkyrie_turn = true 

	elseif inst.on_Valkyrie_3 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 12)

		scheduler:ExecuteInTime(7, function() Lightnings_level_1(inst) end)
		frameshield_1(inst)
		
inst.valkyrie_turn = true 

	elseif inst.on_Valkyrie_4 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 12)

		scheduler:ExecuteInTime(6, function() Lightnings_level_2(inst) end)
		frameshield_1(inst)
	
inst.valkyrie_turn = true 


	elseif inst.on_Valkyrie_5 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 6)

		scheduler:ExecuteInTime(6, function() Lightnings_level_2(inst) end)
		frameshield_2(inst)
		
inst.valkyrie_turn = true 
				 


	elseif inst.on_Valkyrie_6 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 6)

		scheduler:ExecuteInTime(5, function() Lightnings_level_3(inst) end)
		frameshield_2(inst)
	
inst.valkyrie_turn = true 


	elseif inst.on_Valkyrie_7 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 6)

				scheduler:ExecuteInTime(5, function() Lightnings_level_3(inst) end)
		frameshield_3(inst)
	
inst.valkyrie_turn = true 

	elseif inst.on_Valkyrie_8 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 6)

		scheduler:ExecuteInTime(5, function() Lightnings_level_4(inst) end)
		frameshield_3(inst)
	
inst.valkyrie_turn = true 

	elseif inst.on_Valkyrie_9 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 6)

		scheduler:ExecuteInTime(5, function() Lightnings_level_4(inst) end)
		frameshield_4(inst)
		
inst.valkyrie_turn = true 

	elseif inst.on_Valkyrie_10 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 6)

		scheduler:ExecuteInTime(5, function() Lightnings_level_5(inst) end)
		frameshield_4(inst)
		
inst.valkyrie_turn = true 

	elseif inst.on_Valkyrie_11 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 6)

		scheduler:ExecuteInTime(4, function() Lightnings_level_6(inst) end)
		frameshield_4(inst)
	
inst.valkyrie_turn = true 

	elseif inst.on_Valkyrie_12 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 6)

		scheduler:ExecuteInTime(4, function() Lightnings_level_7(inst) end)
		frameshield_4(inst)
	
inst.valkyrie_turn = true 

	elseif inst.on_Valkyrie_13 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then
 
		inst.components.health:StartRegen(1, 6)

		scheduler:ExecuteInTime(4, function() Lightnings_level_8(inst) end)
		frameshield_4(inst)
	
inst.valkyrie_turn = true 

	elseif inst.on_Valkyrie_14 and inst.active_valkyrie and inst.stamina > 0 and not inst.components.health:IsDead() then

		inst.components.health:StartRegen(1, 6)

		scheduler:ExecuteInTime(3, function() Lightnings_level_9(inst) end)
		frameshield_4(inst)

inst.valkyrie_turn = true 

	end 
if not inst.active_valkyrie then	
inst:RemoveEventCallback("onhitother", on_hitLightnings_1)
inst:RemoveEventCallback("onhitother", on_hitLightnings_2)
inst:RemoveEventCallback("onhitother", on_hitLightnings_3)
inst:RemoveEventCallback("onhitother", on_hitLightnings_4)
inst:RemoveEventCallback("onhitother", on_hitLightnings_5)
inst:RemoveEventCallback("onhitother", on_hitLightnings_6)
inst:RemoveEventCallback("onhitother", on_hitLightnings_7)
inst:RemoveEventCallback("onhitother", on_hitLightnings_8)
inst:RemoveEventCallback("onhitother", on_hitLightnings_9)
inst.switch = false
inst.vl1 = false inst.vl2 = false inst.vl3 = false inst.vl4 = false 
inst.vl5 = false inst.vl6 = false inst.vl7 = false inst.vl8 = false 
--inst.vl_remove = false
end	
	if inst.stamina <= 0 then
	inst.active_valkyrie = false	
	inst.valkyrie_turn = false
	end end 
				 
		local function Valkyrie_level_1(inst)
	inst.on_Valkyrie_1 = true
		end 
		local function Valkyrie_level_2(inst)
	inst.on_Valkyrie_2 = true inst.on_Valkyrie_1 = false
		end 	
		local function Valkyrie_level_3(inst)
	inst.on_Valkyrie_3 = true inst.on_Valkyrie_2 = false
		end 	
		   local function Valkyrie_level_4(inst)
	inst.on_Valkyrie_4 = true inst.on_Valkyrie_3 = false
		end 	
		   local function Valkyrie_level_5(inst)
	inst.on_Valkyrie_5 = true inst.on_Valkyrie_4 = false
		end 	
		   local function Valkyrie_level_6(inst)
	inst.on_Valkyrie_6 = true inst.on_Valkyrie_5 = false
		end 	
		   local function Valkyrie_level_7(inst)
	inst.on_Valkyrie_7 = true inst.on_Valkyrie_6 = false
		end 	
		   local function Valkyrie_level_8(inst)
	inst.on_Valkyrie_8 = true  inst.on_Valkyrie_7 = false
		end 	
		   local function Valkyrie_level_9(inst)
	inst.on_Valkyrie_9 = true  inst.on_Valkyrie_8 = false
		end 	
		   local function Valkyrie_level_10(inst)
	inst.on_Valkyrie_10 = true  inst.on_Valkyrie_9 = false
		end 	
		   local function Valkyrie_level_11(inst)
	inst.on_Valkyrie_11 = true  inst.on_Valkyrie_10 = false
		end 	
		   local function Valkyrie_level_12(inst)
	inst.on_Valkyrie_12 = true  inst.on_Valkyrie_11 = false
		end 	
		   local function Valkyrie_level_13(inst)
	inst.on_Valkyrie_13 = true  inst.on_Valkyrie_12 = false
		end 	
		   local function Valkyrie_level_14(inst)
	inst.on_Valkyrie_14 = true  inst.on_Valkyrie_13 = false
		end 	
	
	---berserk passive skill
local function On_freeze_1(attacked, data)
        if data and data.attacker and data.attacker.components.freezable then
            data.attacker.components.freezable:AddColdness(0.2)
            data.attacker.components.freezable:SpawnShatterFX()
        end 
    end
local function On_freeze_2(attacked, data)
        if data and data.attacker and data.attacker.components.freezable then
            data.attacker.components.freezable:AddColdness(0.4)
            data.attacker.components.freezable:SpawnShatterFX()
        end 
    end
	
--berserk
local function OnHitfreeze(inst, data)
local hitfreeze = .1
	 local other = data.target
     if other and math.random() < hitfreeze and other.components.freezable then
        other.components.freezable:AddColdness(0.1)
       -- other.components.freezable:SpawnShatterFX()
    end
    if other.components.burnable and other.components.burnable:IsBurning() then
        other.components.burnable:Extinguish()
end
end
	
local function berserk_hit(inst, data)
    local other = data.target
	SpawnPrefab("small_puff").Transform:SetPosition(other:GetPosition():Get())
	end
		
	function on_berserk_ability_1(inst, data)
	if inst.components.hunger.current < 30 then
			--inst.components.health:SetAbsorptionAmount(0.15)
			inst:ListenForEvent("attacked", On_freeze_1)
			--inst.components.combat:SetAreaDamage(2.5, .5)
			--inst:ListenForEvent("onhitother", berserk_hit)
			inst.berserk_armor_1 = true
			inst.components.health:StartRegen(1, 20)
		end end
	function on_berserk_ability_2(inst, data)
	if inst.components.hunger.current < 30 then
			--inst.components.health:SetAbsorptionAmount(0.3)
			inst:ListenForEvent("attacked", On_freeze_2)
				inst.components.health:StartRegen(1, 15)		
			--inst.components.combat:SetAreaDamage(2.5, .5)
			--inst:ListenForEvent("onhitother", berserk_hit)
			inst.berserk_armor_1 = false
			inst.berserk_armor_2 = true
		end end
	function on_berserk_ability_3(inst, data)
	if inst.components.hunger.current < 30 then
			--inst.components.health:SetAbsorptionAmount(0.45)
			inst:ListenForEvent("attacked", On_freeze_2)
			inst.components.combat:SetAreaDamage(2, .5)
			inst.components.health:StartRegen(1, 10)
			inst.berserk_armor_1 = false
			inst.berserk_armor_2 = false
			inst.berserk_armor_3 = true
			inst:ListenForEvent("onhitother", berserk_hit)
		end end
	function berserk_abil_1(inst, data)
	if inst.components.hunger.current < 30 then
	inst:ListenForEvent("hungerdelta", on_berserk_ability_1)
		end end
	function berserk_abil_2(inst, data)
	if inst.components.hunger.current < 30 then
	inst:ListenForEvent("hungerdelta", on_berserk_ability_2)
		end end
	function berserk_abil_3(inst, data)
	if inst.components.hunger.current < 30 then
	inst:ListenForEvent("hungerdelta", on_berserk_ability_3)
		end end		
    ---force berserk
	   local function force_berserk(inst)
	   if inst.berserk_on then
		inst.components.hunger:SetPercent(.1)
		end end


local function levelexp(inst,data)

	local max_exp = 99999999999999997000
	local level = math.min(inst.level, max_exp)

	local health_percent = inst.components.health:GetPercent()
	local sanity_percent = inst.components.sanity:GetPercent()

if inst.level <7000 then
inst.components.talker:Say("[EXP] \n".. (inst.level))
end
--stat 
	--level 1
if inst.level <5 then
	inst.components.health.maxhealth = math.ceil (80)
	inst.components.sanity.max = math.ceil (80)
	--level 2
elseif inst.level >=5 and inst.level <10  then
	inst.components.health.maxhealth = math.ceil (85)
	inst.components.sanity.max = math.ceil (85)
	--level 3
elseif inst.level >=10 and inst.level <30  then
	inst.components.health.maxhealth = math.ceil (90)
	inst.components.sanity.max = math.ceil (90)
	--level 4
elseif inst.level >=30 and inst.level <50  then
	inst.components.health.maxhealth = math.ceil (95)
	inst.components.sanity.max = math.ceil (95)
	--level 5
elseif inst.level >=50 and inst.level <80  then
	inst.components.health.maxhealth = math.ceil (100)
	inst.components.sanity.max = math.ceil (100)
	--level 6
elseif inst.level >=80 and inst.level <125  then
	inst.components.health.maxhealth = math.ceil (105)
	inst.components.sanity.max = math.ceil (105)
	--level 7
elseif inst.level >=125 and inst.level <200  then
	inst.components.health.maxhealth = math.ceil (110)
	inst.components.sanity.max = math.ceil (110)
	--level 8
elseif inst.level >=200 and inst.level <340  then
	inst.components.health.maxhealth = math.ceil (115)
	inst.components.sanity.max = math.ceil (115)
	--level 9
elseif inst.level >=340 and inst.level <430  then
	inst.components.health.maxhealth = math.ceil (120)
	inst.components.sanity.max = math.ceil (120)
	--level 10
elseif inst.level >=430 and inst.level <530  then
	inst.components.health.maxhealth = math.ceil (125)
	inst.components.sanity.max = math.ceil (125)
	--level 11
elseif inst.level >=530 and inst.level <640  then
	inst.components.health.maxhealth = math.ceil (130)
	inst.components.sanity.max = math.ceil (130)
	--level 12
elseif inst.level >=640 and inst.level <760  then
	inst.components.health.maxhealth = math.ceil (135)
	inst.components.sanity.max = math.ceil (135)
	--level 13
elseif inst.level >=760 and inst.level <890  then
	inst.components.health.maxhealth = math.ceil (140)
	inst.components.sanity.max = math.ceil (140)
	--level 14
elseif inst.level >=890 and inst.level <1030  then
	inst.components.health.maxhealth = math.ceil (145)
	inst.components.sanity.max = math.ceil (145)
	--level 15
elseif inst.level >=1030 and inst.level <1180  then
	inst.components.health.maxhealth = math.ceil (150)
	inst.components.sanity.max = math.ceil (150)
	--level 16
elseif inst.level >=1180 and inst.level <1340  then
	inst.components.health.maxhealth = math.ceil (155)
	inst.components.sanity.max = math.ceil (155)
	--level 17
elseif inst.level >=1340 and inst.level <1510  then
	inst.components.health.maxhealth = math.ceil (160)
	inst.components.sanity.max = math.ceil (160)
	--level 18
elseif inst.level >=1510 and inst.level <1690  then
	inst.components.health.maxhealth = math.ceil (165)
	inst.components.sanity.max = math.ceil (165)
	--level 19
elseif inst.level >=1690 and inst.level <1880  then
	inst.components.health.maxhealth = math.ceil (170)
	inst.components.sanity.max = math.ceil (170)
	--level 20
elseif inst.level >=1880 and inst.level <2080  then
	inst.components.health.maxhealth = math.ceil (175)
	inst.components.sanity.max = math.ceil (175)
	--level 21
elseif inst.level >=2080 and inst.level <2290  then
	inst.components.health.maxhealth = math.ceil (180)
	inst.components.sanity.max = math.ceil (180)
	--level 22
elseif inst.level >=2290 and inst.level <2500  then
	inst.components.health.maxhealth = math.ceil (185)
	inst.components.sanity.max = math.ceil (185)
	--level 23
elseif inst.level >=2500 and inst.level <2850  then
	inst.components.health.maxhealth = math.ceil (190)
	inst.components.sanity.max = math.ceil (190)
	--level 24
elseif inst.level >=2850 and inst.level <3200  then
	inst.components.health.maxhealth = math.ceil (195)
	inst.components.sanity.max = math.ceil (195)
	--level 25
elseif inst.level >=3200 and inst.level <3700  then
	inst.components.health.maxhealth = math.ceil (200)
	inst.components.sanity.max = math.ceil (200)
	--level 26
elseif inst.level >=3700 and inst.level <4200  then
	inst.components.health.maxhealth = math.ceil (205)
	inst.components.sanity.max = math.ceil (205)
	--level 27
elseif inst.level >=4200 and inst.level <4700  then
	inst.components.health.maxhealth = math.ceil (210)
	inst.components.sanity.max = math.ceil (210)
	--level 28
elseif inst.level >=4700 and inst.level <5500 then
	inst.components.health.maxhealth = math.ceil (215)
	inst.components.sanity.max = math.ceil (215)
	--level 29
elseif inst.level >=5500 and inst.level <7000 then
	inst.components.health.maxhealth = math.ceil (220)
	inst.components.sanity.max = math.ceil (220)
	-- level 30
elseif inst.level >=7000  then
	inst.components.health.maxhealth = math.ceil (225)
	inst.components.sanity.max = math.ceil (220 + level* 0.005)
	inst.components.combat.damagemultiplier = 1.1
end

	inst.components.health:SetPercent(health_percent)
	inst.components.sanity:SetPercent(sanity_percent)

----level and skill
--[[test]] 
--[[
if inst.level >= 0 then
Critical_level_3(inst)  --max 7 (damage )
Lshield_level_3(inst)  --max 4 (defense heal + reflect)
Valkyrie_level_4(inst)	--max 14 (prototype1 + Lightning 8 + Armor 4 + Duration 1)
berserk_abil_3(inst)  --max3 (armor + AOS attack)]]


			if inst.level >0 and inst.level <= 4 then

			elseif inst.level >4 and inst.level <= 5 then
	inst.components.talker:Say("Level 2\n Unlock: [Ligntnig Shield]\nLV 1/5")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
	Lshield_level_1(inst)
			elseif inst.level >5 and inst.level <= 9 then
--inst.components.talker:Say("Next :EXP[10]\n[EXP] + ".. (inst.level))
	Lshield_level_1(inst)
		elseif inst.level >9 and inst.level <= 10 then
	inst.components.talker:Say("Level 3\n Unlock:[Valkyrie Power Lightning]: 1/4 \n[Valkyrie]: 1/2")	
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_1(inst)
Valkyrie_level_1(inst)
			elseif inst.level >10 and inst.level <= 29 then
--inst.components.talker:Say("Next :EXP[30]\n[EXP] + ".. (inst.level))
Lshield_level_1(inst)
Valkyrie_level_1(inst)
		elseif inst.level >29 and inst.level <= 30 then
		inst.components.talker:Say("Level 4\n Unlock:[Critical Hit]\nLV 1/7")	
	inst.components.talker:Say("Level 4\n Unlock:[Valkyrie Power Lightning]: 1/4 \n[Valkyrie]: 1/2")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_1(inst)
Critical_level_1(inst)
Valkyrie_level_1(inst)
			elseif inst.level > 30 and inst.level <= 49  then
--inst.components.talker:Say("Next :EXP[50]\n[EXP] + ".. (inst.level))
Lshield_level_1(inst)
Critical_level_1(inst)
Valkyrie_level_1(inst)
		elseif inst.level >49 and inst.level <= 50 then
	inst.components.talker:Say("Level 5\n Unlock: [Valkyrie Lightning]\nLV 1/8")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_1(inst)
Critical_level_1(inst)
Valkyrie_level_2(inst)
			elseif inst.level >50 and inst.level <=79  then
--inst.components.talker:Say("Next :EXP[80]\n[EXP] + ".. (inst.level))
Lshield_level_1(inst)
Critical_level_1(inst)
Valkyrie_level_2(inst)
		elseif inst.level >79 and inst.level <= 80 then
	inst.components.talker:Say("Level 6\n Unlock : [Electric Shield]\nLV 2/5")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_2(inst)
Critical_level_1(inst)
Valkyrie_level_2(inst)
		elseif inst.level >80 and inst.level <=124  then
Lshield_level_2(inst)
Critical_level_1(inst)
Valkyrie_level_2(inst)		
		elseif inst.level >124 and inst.level <= 125 then
	inst.components.talker:Say("Level 7\n Unlock:[Berserk]-[Berserk Armor]\nLV 1/2")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_2(inst)
Valkyrie_level_2(inst)
berserk_abil_1(inst)
			elseif inst.level >125 and inst.level <=199  then
--inst.components.talker:Say("Next :EXP[200]\n[EXP] + ".. (inst.level))
Lshield_level_2(inst)
Valkyrie_level_2(inst)
berserk_abil_1(inst)
		elseif inst.level >199 and inst.level <= 200 then
	inst.components.talker:Say("Level 8\n Unlock : [Valkyrie Armor]\nLV 1/4")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_2(inst)
Valkyrie_level_3(inst)
berserk_abil_1(inst)
			elseif inst.level >200 and inst.level <=339  then
--inst.components.talker:Say("Next :EXP[340]\n[EXP] + ".. (inst.level))
Lshield_level_2(inst)
Valkyrie_level_3(inst)
berserk_abil_1(inst)
		elseif inst.level >339 and inst.level <= 340 then
	inst.components.talker:Say("Level 9\n Unlock : [Valkyrie Lightning Strike]\nLV 2/8")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_2(inst)
Valkyrie_level_4(inst)
berserk_abil_1(inst)
			elseif inst.level >340 and inst.level <=429  then
--inst.components.talker:Say("Next :EXP[430]\n[EXP] + ".. (inst.level))
Lshield_level_2(inst)
Valkyrie_level_4(inst)
berserk_abil_1(inst)
		elseif inst.level >429 and inst.level <= 430 then
	inst.components.talker:Say("Level 10\n Unlock :[Electric Shield]\nLV 3/5 \nUpgrade all [Active Skills]")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_3(inst)
Valkyrie_level_4(inst)
berserk_abil_1(inst)
			elseif inst.level >430 and inst.level <=529  then
--inst.components.talker:Say("Next :EXP[530]\n[EXP] + ".. (inst.level))
Lshield_level_3(inst)
Valkyrie_level_4(inst)
berserk_abil_1(inst)
		elseif inst.level >529 and inst.level <= 530 then
	inst.components.talker:Say("Level 11\n Unlock: [Critical Hit]\nLV 2/7")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_3(inst)
Critical_level_2(inst)
Valkyrie_level_4(inst)
berserk_abil_1(inst)
			elseif inst.level >530 and inst.level <=639  then
--inst.components.talker:Say("Next :EXP[640]\n[EXP] + ".. (inst.level))
Lshield_level_3(inst)
Critical_level_2(inst)
Valkyrie_level_4(inst)
berserk_abil_1(inst)
		elseif inst.level >639 and inst.level <= 640 then
	inst.components.talker:Say("Level 12\n [Critical Hit]\nLV 3/7")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_3(inst)
Critical_level_3(inst)
Valkyrie_level_4(inst)
berserk_abil_1(inst)
			elseif inst.level >640 and inst.level <=759  then
--inst.components.talker:Say("Next :EXP[760]\n[EXP] + ".. (inst.level))
Lshield_level_3(inst)
Critical_level_3(inst)
Valkyrie_level_4(inst)
berserk_abil_1(inst)
		elseif inst.level >759 and inst.level <= 760 then
inst.components.talker:Say("Level 13\n Unlock: [Valkyrie Armor]\nLV 2/4")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_3(inst)
Critical_level_3(inst)
Valkyrie_level_5(inst)
berserk_abil_1(inst)
			elseif inst.level >760 and inst.level <=889  then
--inst.components.talker:Say("Next :EXP[890]\n[EXP] + ".. (inst.level))
Lshield_level_3(inst)
Critical_level_3(inst)
Valkyrie_level_5(inst)
berserk_abil_1(inst)
		elseif inst.level >889 and inst.level <= 890 then
	inst.components.talker:Say("Level 14\n Unlock: [Valkyrie Ligntnig]\nLV 3/8")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_3(inst)
Critical_level_3(inst)
Valkyrie_level_6(inst)
berserk_abil_1(inst)
			elseif inst.level >890 and inst.level <=1029  then
--inst.components.talker:Say("Next :EXP[1030]\n[EXP] + ".. (inst.level))
Lshield_level_3(inst)
Critical_level_3(inst)
Valkyrie_level_6(inst)
berserk_abil_1(inst)
		elseif inst.level >1029 and inst.level <= 1030 then
	inst.components.talker:Say("Level 15\n Unlock: [Ligntnig Shield]\nLV 4/5")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_4(inst)
Critical_level_3(inst)
Valkyrie_level_6(inst)
berserk_abil_1(inst)
			elseif inst.level >1030 and inst.level <=1179  then
--inst.components.talker:Say("Next :EXP[1180]\n[EXP] + ".. (inst.level))
Lshield_level_4(inst)
Critical_level_3(inst)
Valkyrie_level_6(inst)
berserk_abil_1(inst)
		elseif inst.level >1179 and inst.level <= 1180 then
inst.components.talker:Say("Level 16\n Unlock: [Critical Hit]\nLV 4/7")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_4(inst)
Critical_level_4(inst)
Valkyrie_level_6(inst)
berserk_abil_1(inst)
			elseif inst.level >1180 and inst.level <=1339  then
--inst.components.talker:Say("Next :EXP[1340]\n[EXP] + ".. (inst.level))
Lshield_level_4(inst)
Critical_level_4(inst)
Valkyrie_level_6(inst)
berserk_abil_1(inst)
		elseif inst.level >1339 and inst.level <= 1340 then
inst.components.talker:Say("Level 17\n Unlock : [Valkyrie Armor]\nLV 3/4")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_4(inst)
Critical_level_4(inst)
Valkyrie_level_7(inst)
berserk_abil_1(inst)
			elseif inst.level >1340 and inst.level <=1509  then
--inst.components.talker:Say("Next :EXP[1510]\n[EXP] + ".. (inst.level))
Lshield_level_4(inst)
Critical_level_4(inst)
Valkyrie_level_7(inst)
berserk_abil_1(inst)
		elseif inst.level >1509 and inst.level <= 1510 then
	inst.components.talker:Say("Level 18\n Unlock : [Valkyrie Lightning Strike]\nLV 4/8")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_4(inst)
Critical_level_4(inst)
Valkyrie_level_8(inst)
berserk_abil_1(inst)
			elseif inst.level >1510 and inst.level <=1689  then
--inst.components.talker:Say("Next :EXP[1690]\n[EXP] + ".. (inst.level))
Lshield_level_4(inst)
Critical_level_4(inst)
Valkyrie_level_8(inst)
berserk_abil_1(inst)
		elseif inst.level >1689 and inst.level <= 1690 then
inst.components.talker:Say("Level 19\n Unlock : [Berserk Armor]\nLV 2/2")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_4(inst)
Critical_level_4(inst)
Valkyrie_level_8(inst)
berserk_abil_2(inst)
			elseif inst.level >1690 and inst.level <=1879  then
--inst.components.talker:Say("Next :EXP[1880]\n[EXP] + ".. (inst.level))
Lshield_level_4(inst)
Critical_level_4(inst)
Valkyrie_level_8(inst)
berserk_abil_2(inst)
		elseif inst.level >1879 and inst.level <= 1880 then
inst.components.talker:Say("Level 20\n Unlock : Unlock : [Critical Hit]\nLV 5/7\nUpgrade all [Active Skills]")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_4(inst)
Critical_level_5(inst)
Valkyrie_level_8(inst)
berserk_abil_2(inst)
			elseif inst.level >1880 and inst.level <=2079  then
--inst.components.talker:Say("Next :EXP[2080]\n[EXP] + ".. (inst.level))
Lshield_level_4(inst)
Critical_level_5(inst)
Valkyrie_level_8(inst)
berserk_abil_2(inst)
		elseif inst.level >2079 and inst.level <= 2080 then
	inst.components.talker:Say("Level 21\n Unlock : [Electric Shield]\nLV 5/5")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_5(inst)
Valkyrie_level_8(inst)
berserk_abil_2(inst)
			elseif inst.level >2080 and inst.level <=2289  then
--inst.components.talker:Say("Next :EXP[2289]\n[EXP] + ".. (inst.level))
Lshield_level_5(inst)
Critical_level_5(inst)
Valkyrie_level_8(inst)
berserk_abil_2(inst)
		elseif inst.level >2189 and inst.level <= 2290 then
	inst.components.talker:Say("Level 22\n Unlock : [Valkyrie Armor]\nLV 4/4")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_5(inst)
Valkyrie_level_9(inst)
berserk_abil_2(inst)
			elseif inst.level >2290 and inst.level <=2499  then
--inst.components.talker:Say("Next :EXP[2500]\n[EXP] + ".. (inst.level))
Lshield_level_5(inst)
Critical_level_5(inst)
Valkyrie_level_9(inst)
berserk_abil_2(inst)
		elseif inst.level >2499 and inst.level <= 2500 then
	inst.components.talker:Say("Level 23\n Unlock : [Valkyrie Lightning Strike]\nLV 5/8")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_5(inst)
Valkyrie_level_10(inst)
berserk_abil_2(inst)
			elseif inst.level >2500 and inst.level <=2849  then
--inst.components.talker:Say("Next :EXP[2850]\n[EXP] + ".. (inst.level))
Lshield_level_5(inst)
Critical_level_5(inst)
Valkyrie_level_10(inst)
berserk_abil_2(inst)
		elseif inst.level >2849 and inst.level <= 2850 then
	inst.components.talker:Say("Level 24\n Unlock : [Berserk Master]\nLV 2/2")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_5(inst)
Valkyrie_level_10(inst)
berserk_abil_3(inst)
			elseif inst.level >2850 and inst.level <=3199  then
--inst.components.talker:Say("Next :EXP[3200]\n[EXP] + ".. (inst.level))
Lshield_level_5(inst)
Critical_level_5(inst)
Valkyrie_level_10(inst)
berserk_abil_3(inst)
		elseif inst.level >3199 and inst.level <= 3200 then
inst.components.talker:Say("Level 25\n Unlock : [Critical Hit]\nLV 6/7")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_6(inst)
Valkyrie_level_10(inst)
berserk_abil_3(inst)
			elseif inst.level >3200 and inst.level <=3699  then
--inst.components.talker:Say("Next :EXP[3700]\n[EXP] + ".. (inst.level))
Lshield_level_5(inst)
Critical_level_6(inst)
Valkyrie_level_10(inst)
berserk_abil_3(inst)
		elseif inst.level >3699 and inst.level <= 3700 then
inst.components.talker:Say("Level 26\n Unlock : [Valkyrie Lightning Strike]\nLV 6/8")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_6(inst)
Valkyrie_level_11(inst)
berserk_abil_3(inst)
			elseif inst.level >3700 and inst.level <=4199  then
--inst.components.talker:Say("Next :EXP[4200]\n[EXP] + ".. (inst.level))
Lshield_level_5(inst)
Critical_level_6(inst)
Valkyrie_level_11(inst)
berserk_abil_3(inst)
		elseif inst.level >4199 and inst.level <= 4200 then
inst.components.talker:Say("Level 27\n Unlock : [Critical]\nLV 7/7")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_7(inst)
Valkyrie_level_11(inst)
berserk_abil_3(inst)
			elseif inst.level >4200 and inst.level <=4699  then
--inst.components.talker:Say("Next :EXP[4700]\n[EXP] + ".. (inst.level))
Lshield_level_5(inst)
Critical_level_7(inst)
Valkyrie_level_11(inst)
berserk_abil_3(inst)
		elseif inst.level >4699 and inst.level <= 4700 then
inst.components.talker:Say("Level 28\n Unlock : [Valkyrie Lightning Strike]\nLV 7/8")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_7(inst)
Valkyrie_level_12(inst)
berserk_abil_3(inst)
			elseif inst.level >4700 and inst.level <=5499  then
--inst.components.talker:Say("Next :EXP[5500]\n[EXP] + ".. (inst.level))
Lshield_level_5(inst)
Critical_level_7(inst)
Valkyrie_level_12(inst)
berserk_abil_3(inst)
		elseif inst.level >5499 and inst.level <= 5500 then
	inst.components.talker:Say("Level 29\n Unlock : [Valkyrie Lightning Strike]\nLV 4/4")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_7(inst)
Valkyrie_level_13(inst)
berserk_abil_3(inst)
			elseif inst.level >5500 and inst.level <=6999  then
--inst.components.talker:Say("Next :EXP[7000]\n[EXP] + ".. (inst.level))
Lshield_level_5(inst)
Critical_level_7(inst)
Valkyrie_level_13(inst)
berserk_abil_3(inst)
		elseif inst.level >6999 and inst.level <= 7000 then
inst.components.talker:Say("Level 30\n Unlock : [Double Damage]\n Unlock : [Valkyrie Master] \nLV 2/2\nUpgrade all [Active Skills]")
inst.SoundEmitter:PlaySound("dontstarve/common/cookingpot_finish", "snd")
inst.SoundEmitter:PlaySound("dontstarve/wilson/equip_item_gold")
Lshield_level_5(inst)
Critical_level_7(inst)
Valkyrie_level_14(inst)
berserk_abil_3(inst)
		elseif inst.level > 7000 then
		Lshield_level_5(inst)
Critical_level_7(inst)
Valkyrie_level_14(inst)
berserk_abil_3(inst)
	end
end

local tynychance = 0.06
local smallllchance = 0.15
local smallchance = 0.2
local normalchance = 0.3
local largechance = 0.7
local foodnormalchance = 0.4
local foodgoodchance = 0.6
local foodlargechance = 0.8

local function spawnspirit(inst, x, y, z, scale)
    local fx = SpawnPrefab("wathgrithr_spirit")
    fx.Transform:SetPosition(x, y, z)
    fx.Transform:SetScale(scale, scale, scale)
end

local smallScale = 0.5
local medScale = 0.7
local largeScale = 1.1
local function onkilll(inst, data)
	local victim = data.victim
	   if not (victim:HasTag("prey") or
            victim:HasTag("veggie") or
            victim:HasTag("eyeplant") or
            victim:HasTag("insect") or			
            victim:HasTag("structure")) then
    local delta = victim.components.combat.defaultdamage * 0.25
		   if math.random() < largechance then
					inst.level = inst.level + 1
 					--if victim:HasTag("monster1x") then
					--inst.level = inst.level + 1
					if victim:HasTag("monster2x") then
					inst.level = inst.level + 1
						if math.random() < 0.09 then
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
					end	
					elseif victim:HasTag("monster3x") then
					inst.level = inst.level + 2
						if math.random() < 0.12 then
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
					end
					elseif victim:HasTag("monster4x") then
					inst.level = inst.level + 3
						if math.random() < 0.15 then
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
					end
						end
											   				
 		--inst.components.health:DoDelta(delta, false, "battleborn")
		inst.components.sanity:DoDelta(delta)
			levelexp(inst)					
            local time = victim.components.health.destroytime or 2
            local x, y, z = victim.Transform:GetWorldPosition()
            local scale = (victim:HasTag("smallcreature") and smallScale)
                        or (victim:HasTag("largecreature") and largeScale)
                        or medScale
					inst:DoTaskInTime(time, spawnspirit, x, y, z, scale)
				 	   if not inst.berserk and math.random() < 0.05 then
					   --inst.components.talker:Say("Come to me! Ghost puppy")
					     scheduler:ExecuteInTime(1, function() SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get()) SpawnPrefab("ghosthound2").Transform:SetPosition(victim:GetPosition():Get()) end) end
					end					
				end
		  if (victim:HasTag("prey") or victim:HasTag("insect") or victim:HasTag("frog")) then
    local delta = victim.components.combat.defaultdamage * 0.1
		   if math.random() < smallllchance then
    				inst.level = inst.level + 1
 			inst.components.sanity:DoDelta(delta)
			levelexp(inst)					
            local time = victim.components.health.destroytime or 2
            local x, y, z = victim.Transform:GetWorldPosition()
            local scale = (victim:HasTag("smallcreature") and smallScale)
                        or (victim:HasTag("largecreature") and largeScale)
                        or medScale
					inst:DoTaskInTime(time, spawnspirit, x, y, z, scale)
					end end 	
		 if (victim:HasTag("epic") or victim:HasTag("deerclops") or 
			 victim:HasTag("bearger") or victim:HasTag("moose") or 
							  victim:HasTag("dragonfly")  ) then
			local delta = victim.components.combat.defaultdamage * 0.1
		   			inst.components.sanity:DoDelta(delta)
					if victim:HasTag("small_giant1x") then
					inst.level = inst.level + 5
					inst.components.talker:Say("Kill Epic Monster\n [EXP] + 5 ")

					elseif victim:HasTag("small_giant2x") then
					inst.level = inst.level + 10
					inst.components.talker:Say("Kill Epic Monster\n [EXP] + 10 ")
											if math.random() < 0.2 then
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
											elseif math.random() < 0.02 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("purplegem").Transform:SetPosition(victim:GetPosition():Get())
					end
					elseif victim:HasTag("small_giant3x") then
					inst.level = inst.level + 15
					inst.components.talker:Say("Kill Epic Monster\n [EXP] + 15 ")
											if math.random() < 0.3 then
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
											elseif math.random() < 0.05 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("purplegem").Transform:SetPosition(victim:GetPosition():Get())
					end
					elseif victim:HasTag("small_giant4x") then
					inst.level = inst.level + 20
					inst.components.talker:Say("Kill Epic Monster\n [EXP] + 20 ")
											if math.random() < 0.4 then
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
											elseif math.random() < 0.1 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("amulet").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("purplegem").Transform:SetPosition(victim:GetPosition():Get())
					end
					elseif victim:HasTag("giant1x") then
					inst.level = inst.level + 10
					inst.components.talker:Say("Kill Giant\n [EXP] + 10 ")
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
						if math.random() < 0.2 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("amulet").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())

					end
					elseif victim:HasTag("giant2x") then
					inst.level = inst.level + 20
					inst.components.talker:Say("Kill Giant\n [EXP] + 20 ")
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("redgem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
						if math.random() < 0.5 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("bluegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
						elseif math.random() < 0.15 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("amulet").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("purplegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
						elseif math.random() < 0.05 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("mandrake").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
					end
					elseif victim:HasTag("giant3x") then
					inst.level = inst.level + 30
					inst.components.talker:Say("Kill Giant\n [EXP] + 30 ")
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("redgem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("bluegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
						if math.random() < 0.5 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("amulet").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("purplegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
						elseif math.random() < 0.3 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("greengem").Transform:SetPosition(victim:GetPosition():Get())		
					SpawnPrefab("purplegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
						elseif math.random() < 0.2 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("mandrake").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
					end
					elseif victim:HasTag("giant4x") then
					inst.level = inst.level + 40
					inst.components.talker:Say("Kill Giant\n [EXP] + 40 ")
					SpawnPrefab("statue_transition_2").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("redgem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("bluegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("purplegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("amulet").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
					if math.random() < 0.7 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("amulet").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("purplegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("greengem").Transform:SetPosition(victim:GetPosition():Get())	
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())					
						elseif math.random() < 0.6 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("purplegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("yellowgem").Transform:SetPosition(victim:GetPosition():Get())	
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
						elseif math.random() < 0.5 then
					SpawnPrefab("glowdust").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("orangegem").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("mandrake").Transform:SetPosition(victim:GetPosition():Get())
					SpawnPrefab("goldnugget").Transform:SetPosition(victim:GetPosition():Get())
					end
					end
    				levelexp(inst)			
					
            local time = victim.components.health.destroytime or 2
            local x, y, z = victim.Transform:GetWorldPosition()
            local scale = (victim:HasTag("smallcreature") and smallScale)
                        or (victim:HasTag("largecreature") and largeScale)
                        or medScale
					inst:DoTaskInTime(time, spawnspirit, x, y, z, scale)
					end 
					end 	
				

		
local function expeat(inst, food)

if food.components.edible.hungervalue >= 150 and food.components.edible.healthvalue >= 100 then 
if food.components.edible then
	inst.level = inst.level + 25
	levelexp(inst)
	inst.components.talker:Say("Yam Yam !!\n [EXP] + 25 ")
end
elseif food.components.edible.hungervalue > 140 or food.components.edible.healthvalue > 35 then 
if food.components.edible and math.random() < foodlargechance then
	inst.level = inst.level + 1
	levelexp(inst)
end
elseif food.components.edible.hungervalue > 55 or food.components.edible.healthvalue > 29 or food.components.edible.sanityvalue > 19 then 
if food.components.edible and math.random() < foodgoodchance then
	inst.level = inst.level + 1
	levelexp(inst)
end
elseif food.components.edible.hungervalue > 24 or food.components.edible.healthvalue > 19 or food.components.edible.sanityvalue > 14 then 
if food.components.edible and math.random() < foodnormalchance then
	inst.level = inst.level + 1
	levelexp(inst)
end
elseif food.components.edible.hungervalue > 9 or food.components.edible.healthvalue > 9 or food.components.edible.sanityvalue > 9 then 
if food.components.edible and math.random() < tynychance then
	inst.level = inst.level + 1
	levelexp(inst)
end
end
--force bersek
 if string.find(tostring(food),"monsterm") then
 		 	force_berserk(inst)
	end
end

local function in_fire(inst)
    local x, y, z = inst.Transform:GetWorldPosition() 
    local delta = 0
    local max_rad = 10
    local ents = TheSim:FindEntities(x, y, z, max_rad, { "fire" })
    for i, v in ipairs(ents) do
        if v.components.burnable ~= nil and v.components.burnable:IsBurning() then
            local rad = v.components.burnable:GetLargestLightRadius() or 1
            local sz = TUNING.SANITYAURA_TINY * math.min(max_rad, rad) / max_rad
            local distsq = inst:GetDistanceSqToInst(v) - 9
            -- shift the value so that a distance of 3 is the minimum
            delta = delta + sz / math.max(1, distsq)
        end
 
    end
    return delta
end

  local function onsanitydelta(inst, data)
  
  --sanity change
  if TheWorld.state.isday then
  if inst.strength == "full" then
 inst.components.sanity.dapperness = (TUNING.DAPPERNESS_MED)
 elseif inst.strength == "normal" then
 inst.components.sanity.dapperness = (0) 
		end 
    elseif TheWorld.state.isdusk then
  if inst.strength == "full" then
 inst.components.sanity.dapperness = (0)
  elseif inst.strength == "normal" then
 inst.components.sanity.dapperness = (-TUNING.DAPPERNESS_SMALL) 
		end 
    elseif TheWorld.state.isnight then
  if inst.strength == "full" then
 inst.components.sanity.dapperness =  (-TUNING.DAPPERNESS_SMALL)
 elseif inst.strength == "normal" then
  inst.components.sanity.dapperness = (-TUNING.DAPPERNESS_LARGE) 		
   end end
  
    inst.components.temperature:SetModifier("sanity",
        (data.newpercent < TUNING.WILLOW_CHILL_END and TUNING.WILLOW_SANITY_CHILLING) or
        (data.newpercent < TUNING.WILLOW_CHILL_START 
        and easing.outQuad(data.newpercent - TUNING.WILLOW_CHILL_END, 
        TUNING.WILLOW_SANITY_CHILLING, -TUNING.WILLOW_SANITY_CHILLING, 
        TUNING.WILLOW_CHILL_START - TUNING.WILLOW_CHILL_END)) 
        or 0)
end

local function phasechange(inst, data)
if inst.sg:HasStateTag("nomorph") or
          inst:HasTag("playerghost") or
          inst.components.health:IsDead() then
          return
  end  
--phase change
if inst.level >= 10 then
inst.valkyrie_on = true
end
if inst.level >= 640 then 
inst.berserk_on = true end
if inst.active_valkyrie and inst.stamina >= 30 then
    inst.strength = "valkyrie"
elseif inst.components.hunger.current >= 160 and inst.stamina >= 50 and not inst.active_valkyrie then
    inst.strength = "full" 
elseif inst.components.hunger.current >= 160 and inst.stamina < 50 and not inst.active_valkyrie then
    inst.strength = "normal" 	
elseif inst.components.hunger.current < 160 and inst.components.hunger.current >= 30 and not inst.berserk and not inst.active_valkyrie then
  inst.strength = "normal" 
elseif inst.components.hunger.current < 30 and inst.berserk_on and inst.stamina < 30 then
  inst.strength = "normal" 
elseif inst.components.hunger.current < 30 and inst.berserk_on and inst.stamina >= 30 then
  inst.strength = "berserk" 
end
--
--sanity change
   if TheWorld.state.isday then
  if inst.strength == "full" then
  if inst.warm_on then
 inst.components.sanity.dapperness = (TUNING.DAPPERNESS_LARGE)
 elseif not inst.warm_on then
 inst.components.sanity.dapperness = (TUNING.DAPPERNESS_MED)
	end
	if inst.No_Sleep_Princess then
inst.components.combat:SetAttackPeriod(0.55)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.95 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.95 )	
inst.components.health:StartRegen(0, 0)
	end
 elseif inst.strength == "normal" then
   if inst.warm_on then
 inst.components.sanity.dapperness = (TUNING.DAPPERNESS_TINY)
 elseif not inst.warm_on then
 inst.components.sanity.dapperness = (0) 
	end	
		if inst.No_Sleep_Princess then
inst.components.combat:SetAttackPeriod(0.1)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.15 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.15 )	
inst.components.health:StartRegen(0, 0)
	end end 
    elseif TheWorld.state.isdusk then
  if inst.strength == "full" then
    if inst.warm_on then
 inst.components.sanity.dapperness = (TUNING.DAPPERNESS_TINY)
 elseif not inst.warm_on then
 inst.components.sanity.dapperness = (0) 
	end
		if inst.No_Sleep_Princess then
inst.components.combat:SetAttackPeriod(0.6)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.9 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.9 )	
inst.components.health:StartRegen(0, 0)
	end
  elseif inst.strength == "normal" then
     if inst.warm_on then
 inst.components.sanity.dapperness = (0)
 elseif not inst.warm_on then
 inst.components.sanity.dapperness = (-TUNING.DAPPERNESS_SMALL) 
	end 
		if inst.No_Sleep_Princess then
inst.components.combat:SetAttackPeriod(0.15)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.1 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.1 )	
inst.components.health:StartRegen(0, 0)
	end end 
    elseif TheWorld.state.isnight then
  if inst.strength == "full" then
     if inst.warm_on then
 inst.components.sanity.dapperness = (0)
 elseif not inst.warm_on then
 inst.components.sanity.dapperness = (-TUNING.DAPPERNESS_SMALL) 
	end
		if inst.No_Sleep_Princess then
inst.components.combat:SetAttackPeriod(0.65)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.8 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.8 )	
inst.components.health:StartRegen(0, 0)
	end
 elseif inst.strength == "normal" then
      if inst.warm_on then
 inst.components.sanity.dapperness = (-TUNING.DAPPERNESS_SMALL)
 elseif not inst.warm_on then
 inst.components.sanity.dapperness = (-TUNING.DAPPERNESS_MED) 
	end 
		if inst.No_Sleep_Princess then
inst.components.combat:SetAttackPeriod(0.2)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.95 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.95 )	
inst.components.health:StartRegen(0, 0)
	end
	end end
  
	if inst.strength == "full" then
			inst.soundsname = "willow"
			inst.components.combat:SetAreaDamage(0, 0)
			--inst.components.health:StartRegen(0, 0)
			inst:RemoveEventCallback("onhitother", berserk_hit)
			inst:RemoveEventCallback("attacked", On_freeze_1)		
			inst:RemoveEventCallback("attacked", On_freeze_2)			
			inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * 1.4)
			inst.components.health.fire_damage_scale = .75
			inst.berserk = false
			inst.valkyrie = false
			inst:RemoveEventCallback("attacked", On_frameshield_1)
			inst:RemoveEventCallback("attacked", On_frameshield_2)
			inst:RemoveEventCallback("attacked", On_frameshield_3)
			inst:RemoveEventCallback("attacked", On_frameshield_4)
			inst.valkyrie_turn = false
			if not inst.visual_hold then
			inst.AnimState:SetBuild("musha")
			end end
	if inst.strength == "normal" then
			inst.soundsname = "willow"
			inst.components.combat:SetAreaDamage(0, 0)
			--inst.components.health:StartRegen(0, 0)
			inst:RemoveEventCallback("onhitother", berserk_hit)
			inst:RemoveEventCallback("attacked", On_freeze_1)		
			inst:RemoveEventCallback("attacked", On_freeze_2)	
			inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE)
			inst.components.health.fire_damage_scale = .75
			inst.berserk = false
			inst.valkyrie = false
			inst:RemoveEventCallback("attacked", On_frameshield_1)
			inst:RemoveEventCallback("attacked", On_frameshield_2)
			inst:RemoveEventCallback("attacked", On_frameshield_3)
			inst:RemoveEventCallback("attacked", On_frameshield_4)
			inst.valkyrie_turn = false			
			if not inst.visual_hold then
			inst.AnimState:SetBuild("musha_normal")
			end	end		
	if inst.strength == "valkyrie" then
			inst.soundsname = "willow"
			inst.components.combat:SetAreaDamage(0, 0)
			inst.components.sanity.dapperness = (0)
			inst:RemoveEventCallback("onhitother", berserk_hit)
			inst:RemoveEventCallback("attacked", On_freeze_1)		
			inst:RemoveEventCallback("attacked", On_freeze_2)					
			inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * 2)
			inst.components.health.fire_damage_scale = 0
			inst.berserk = false
			inst.valkyrie = true
inst.components.combat:SetAttackPeriod(0.005)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.3 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.3 )		
 
			if not inst.visual_hold then
			inst.AnimState:SetBuild("musha_battle")
			end	end		
	if inst.strength == "berserk" then
			inst.soundsname = "wendy"
			--inst.components.sanity.dapperness = (TUNING.DAPPERNESS_LARGE*-6)
		inst.components.sanity.dapperness = (-TUNING.DAPPERNESS_LARGE)
			inst.components.hunger:SetRate(TUNING.WILSON_HUNGER_RATE * 0.5)
			inst.components.health.fire_damage_scale = 0
			inst.berserk = true
			inst.valkyrie = false
			inst.active_valkyrie = false
			inst:RemoveEventCallback("attacked", On_frameshield_1)
			inst:RemoveEventCallback("attacked", On_frameshield_2)
			inst:RemoveEventCallback("attacked", On_frameshield_3)
			inst:RemoveEventCallback("attacked", On_frameshield_4)
			inst.valkyrie_turn = false
			inst.components.combat:SetAttackPeriod(0.001)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.4 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.4 )	
 
			if not inst.visual_hold then
			inst.AnimState:SetBuild("musha_hunger")
  end end
  
  if not inst.activec0 and inst.berserk and inst.berserk_armor_1 and not inst.music_armor then
	inst.components.health:SetAbsorptionAmount(0.4)
elseif not inst.activec0 and inst.berserk and inst.berserk_armor_2 and not inst.music_armor then
	inst.components.health:SetAbsorptionAmount(0.45)	
elseif not inst.activec0 and inst.berserk and inst.berserk_armor_3 and not inst.music_armor then
	inst.components.health:SetAbsorptionAmount(0.5)
	
end	
if not inst.activec0 and inst.valkyrie and inst.valkyrie_armor_1 and not inst.music_armor then
	inst.components.health:SetAbsorptionAmount(0.06)
elseif not inst.activec0 and inst.valkyrie and inst.valkyrie_armor_2 and not inst.music_armor then
	inst.components.health:SetAbsorptionAmount(0.12)	
elseif not inst.activec0 and inst.valkyrie and inst.valkyrie_armor_3 and not inst.music_armor then
	inst.components.health:SetAbsorptionAmount(0.18)	
elseif not inst.activec0 and inst.valkyrie and inst.valkyrie_armor_4 and not inst.music_armor then
	inst.components.health:SetAbsorptionAmount(0.24)
end
  	if not inst.activec0 and not inst.berserk and not inst.valkyrie and not inst.music_armor then
	inst.components.health:SetAbsorptionAmount(0)
	end
	if inst.music_armor then
	inst.components.health:SetAbsorptionAmount(1)
  
  end 
  
  --sleepy
if not (inst.valkyrie or inst.berserk) and not inst.sleep_on and not inst.tiny_sleep and not inst.No_Sleep_Princess then
if inst.stamina >= 95 then
inst.components.combat:SetAttackPeriod(0.005)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.25 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.25 )	
	inst.components.health:StartRegen(1, 20)
elseif inst.stamina < 95 and inst.stamina >= 90 then
inst.components.combat:SetAttackPeriod(0.01)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.2 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.2 )	
	inst.components.health:StartRegen(1, 25)
elseif inst.stamina < 90 and inst.stamina >= 85 then
inst.components.combat:SetAttackPeriod(0.05)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.15 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.15 )	
	inst.components.health:StartRegen(1, 30)
elseif inst.stamina < 85 and inst.stamina >= 80 then
inst.components.combat:SetAttackPeriod(0.1)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.1 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.1 )	
inst.components.health:StartRegen(1, 40)
elseif inst.stamina < 80 and inst.stamina >= 75 then
inst.components.combat:SetAttackPeriod(0.15)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.05 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.05 )	
inst.components.health:StartRegen(1, 50)
elseif inst.stamina < 75 and inst.stamina >= 70 then
inst.components.combat:SetAttackPeriod(0.2)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 1.0 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 1.0 )	
inst.components.health:StartRegen(1, 60)
elseif inst.stamina < 70 and inst.stamina >= 65 then
inst.components.combat:SetAttackPeriod(0.25)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.98 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.98 )	
inst.components.health:StartRegen(0, 0)
elseif inst.stamina < 60 and inst.stamina >= 55 then
inst.components.combat:SetAttackPeriod(0.3)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.96 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.96 )	
inst.components.health:StartRegen(0, 0)
elseif inst.stamina < 55 and inst.stamina >= 50 then
inst.components.combat:SetAttackPeriod(0.35)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.94 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.94 )	
inst.components.health:StartRegen(0, 0)
elseif inst.stamina < 50 and inst.stamina >= 45 then
inst.components.combat:SetAttackPeriod(0.4)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.92 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.92 )	
inst.components.health:StartRegen(0, 0)
elseif inst.stamina < 40 and inst.stamina >= 35 then
inst.components.combat:SetAttackPeriod(0.45)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.9 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.9)	
 inst.components.health:StartRegen(0, 0)
elseif inst.stamina < 35 and inst.stamina >= 30 then
inst.components.combat:SetAttackPeriod(0.5)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.85 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.85 )	
 inst.components.health:StartRegen(0, 0)
elseif inst.stamina < 30 and inst.stamina >= 25 then
inst.components.combat:SetAttackPeriod(0.55)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.8 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.8 )	
 inst.components.health:StartRegen(-1, 60)
elseif inst.stamina < 25 and inst.stamina >= 20 then
inst.components.combat:SetAttackPeriod(0.6)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.75 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.75 )	
 inst.components.health:StartRegen(-1, 55)
elseif inst.stamina < 20 and inst.stamina >= 15 then
inst.components.combat:SetAttackPeriod(0.65)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.7 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.7 )	
 inst.components.health:StartRegen(-1, 50)
elseif inst.stamina < 15 and inst.stamina >= 10 then
inst.components.combat:SetAttackPeriod(0.7)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.65 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.65 )	
 inst.components.health:StartRegen(-1, 45)
elseif inst.stamina < 10 and inst.stamina >= 5 then
inst.components.combat:SetAttackPeriod(0.75)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.6 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.6 )	
 inst.components.health:StartRegen(-1, 40)
elseif inst.stamina < 5 and inst.stamina > 0 then
inst.components.combat:SetAttackPeriod(0.8)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED * 0.55 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.55 )	
 inst.components.health:StartRegen(-1, 35)
elseif inst.stamina <= 0 then
inst.components.combat:SetAttackPeriod(0.9)
inst.components.locomotor.walkspeed = (TUNING.WILSON_WALK_SPEED *0.5 )
inst.components.locomotor.runspeed = (TUNING.WILSON_RUN_SPEED * 0.5 )	
 inst.components.health:StartRegen(-1, 30)	 
------
end end
  end 

local function onpreload(inst, data)
	if data then
				if data.music then
			inst.music = data.music
			fullcharged_music(inst)
			end
				if data.stamina then
			inst.stamina = data.stamina
			stamina_sleep(inst)
			end
				if data.fatigue then
			inst.fatigue = data.fatigue
			fatigue_sleep(inst)
			end
			if data.level then
			inst.level = data.level
			levelexp(inst)
			if data.health and data.health.health then inst.components.health.currenthealth = data.health.health end
			if data.sanity and data.sanity.current then inst.components.sanity.current = data.sanity.current end
			inst.components.health:DoDelta(0)
			inst.components.sanity:DoDelta(0)
		end
	end
end  
 
local function onsave(inst, data)
	data.level = inst.level
	data.stamina = inst.stamina
	data.fatigue = inst.fatigue
	data.music = inst.music
 	end
	
local function ondeath(inst)
    inst.components.health.numrevives = 0
end

local function onnewstate(inst)
    if inst._wasnomorph ~= inst.sg:HasStateTag("nomorph") then
        inst._wasnomorph = not inst._wasnomorph
        if not inst._wasnomorph then
            phasechange(inst)
        end
    end
end

local function onbecamehuman(inst)
    if inst._wasnomorph == nil then
        inst.strength = "normal"
        inst._wasnomorph = inst.sg:HasStateTag("nomorph")
        inst.talksoundoverride = nil
        inst.hurtsoundoverride = nil
        inst:ListenForEvent("hungerdelta", phasechange)
        inst:ListenForEvent("newstate", onnewstate)
        phasechange(inst, nil, true)
    end
end

local function onbecameghost(inst)
    if inst._wasnomorph ~= nil then
        inst.strength = "normal"
        inst._wasnomorph = nil
        inst.talksoundoverride = nil
        inst.hurtsoundoverride = nil
        inst:RemoveEventCallback("hungerdelta", phasechange)
        inst:RemoveEventCallback("newstate", onnewstate)
    end
end


local function onload(inst)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)
    inst:ListenForEvent("ms_becameghost", onbecameghost)

    if inst:HasTag("playerghost") then
        onbecameghost(inst)
    else
        onbecamehuman(inst)
    end
end
--------------------------------------------------------------
 
	---------------------------------------------------------------------------------
	
-- This initializes for both clients and the host
local common_postinit = function(inst,data) 

	inst.soundsname = "willow"
	inst.MiniMapEntity:SetIcon( "musha_mapicon.tex" )
	inst:AddTag("bookbuilder")
	inst:AddTag("musha")
	
    inst.entity:AddLight()
	inst.Light:SetRadius(2.5)
    inst.Light:SetFalloff(.8)
    inst.Light:SetIntensity(.8)
    inst.Light:SetColour(150/255,150/255,150/255)
	inst:AddComponent("keyhandler")
    inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY, "INFO")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY5, "INFO2")
    inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY2, "Lightning_a")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY3, "on_shield_act")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY4, "buff")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY6, "yamche")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY7, "visual_hold")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY8, "yamche2")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY9, "yamche3")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY10, "ydebug")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY11, "shadows")
	inst.components.keyhandler:AddActionListener("musha", TUNING.MUSHA.KEY12, "sleeping")
	
	 inst:AddComponent("sanityaura")
	-- inst:AddComponent("sleeper")
	inst:AddComponent("colourtweener")
	inst.components.colourtweener:StartTween({1,1,1,1}, 0)
	inst:ListenForEvent("killed", onkilll)
	inst:ListenForEvent("death", ondeath)
	--inst.components.talker.colour = Vector3(1, 1, 1, 1)
			inst.stamina = 100 
			inst.fatigue = 0
			inst.music = 0
end

local master_postinit = function(inst)

	inst.level = 0
	
	inst.components.eater:SetOnEatFn(expeat)
	inst:ListenForEvent("levelup", levelexp)
	
	inst.Transform:SetScale(0.95,0.95,0.95)

	inst.components.health:SetMaxHealth(80)
	inst.components.hunger:SetMax(200)
	inst.components.sanity:SetMax(80)
	
--[[	inst.components.sanity.custom_rate_fn = in_fire
    inst.components.sanity.rate_modifier = TUNING.WILLOW_SANITY_MODIFIER
    inst:ListenForEvent("sanitydelta", onsanitydelta)]]
    --inst.components.combat.damagemultiplier = TUNING.WENDY_DAMAGE_MULT
	inst.components.combat.damagemultiplier = .55
	--inst.components.combat.playerdamagepercent = 0
	inst.components.temperature.hurtrate = 0.5
-----------

    inst._wasnomorph = nil
    inst.talksoundoverride = nil
    inst.hurtsoundoverride = nil

	inst.components.eater.strongstomach = true
	
	--inst:AddComponent("sleeper")
	inst.components.talker.fontsize = 28
	inst:AddComponent("reader")
    inst.components.eater.stale_hunger = TUNING.WICKERBOTTOM_STALE_FOOD_HUNGER
    inst.components.eater.stale_health = TUNING.WICKERBOTTOM_STALE_FOOD_HEALTH
    inst.components.eater.spoiled_hunger = TUNING.WICKERBOTTOM_SPOILED_FOOD_HUNGER
    inst.components.eater.spoiled_health = TUNING.WICKERBOTTOM_SPOILED_FOOD_HEALTH
	inst.components.builder.science_bonus = 1
	
inst.sleep_on = false
inst.tiny_sleep = false
phasechange(inst)	
--[[
inst:WatchWorldState("daytime", function(inst) phasechange(inst) end , TheWorld)
inst:WatchWorldState("dusktime", function(inst) phasechange(inst) end , TheWorld)
inst:WatchWorldState("nighttime", function(inst) phasechange(inst) end , TheWorld)
]]
	
	inst:ListenForEvent("sleep", stamina_sleep)
	inst:ListenForEvent("sleepy", fatigue_sleep)
inst.sleep_test = inst:DoPeriodicTask(0, sleep_test)
inst.consume_sleep1 = inst:DoPeriodicTask(1, consume_stamina)
inst.on_fatigue = inst:DoPeriodicTask(1, on_fatigue)
inst.sleepheal = inst:DoPeriodicTask(6,onsleepheal)
inst:DoPeriodicTask(1, function()
			if (inst.sleep_on or inst.tiny_sleep) and not inst.sg:HasStateTag("tent") then
	local max_stamina = 100
	local min_stamina = 0
	local max_fatigue = 100
	local min_fatigue = 0	
	local max_music = 100
	local min_music = 0	
			local mx=math.floor(max_stamina-min_stamina)
			local cur=math.floor(inst.stamina-min_stamina)
			local mx2=math.floor(max_fatigue-min_fatigue)
			local cur2=math.floor(inst.fatigue-min_fatigue)
			local mxx=math.floor(max_music-min_music)
			local curr=math.floor(inst.music-min_music)
			local sleep = ""..math.floor(cur*100/mx).."%"
			local sleepy = ""..math.floor(cur2*100/mx2).."%"
			local music = ""..math.floor(curr*100/mxx).."%"
inst.components.talker:StopIgnoringAll("sleeping")	
    inst.components.talker.fontsize = 22
    inst.components.talker.colour = Vector3(0.75, 0.85, 1, 1)
inst.components.talker:Say("[Sleep / Tired]\n"..(sleep).." / "..(sleepy).."\n[Music]: "..(music))
	end	end)
inst.Valkyrie_all = inst:DoPeriodicTask(0, on_Valkyrie_all)
inst.phasechange = inst:DoPeriodicTask(0, phasechange)
--inst.active_sparkshield = inst:DoPeriodicTask(0, function() active_sparkshield(inst) end)
--inst:ListenForEvent("hungerdelta", phasechange)
inst:ListenForEvent("hungerdelta", active_sparkshield)
inst:ListenForEvent("hungerdelta", flameshield_active)

	inst.OnLoad = onload
    inst.OnNewSpawn = onload
	
	inst.OnSave = onsave
	inst.OnPreLoad = onpreload

    return inst
end

return MakePlayerCharacter("musha", prefabs, assets, common_postinit, master_postinit, start_inv)

	
	
	
	