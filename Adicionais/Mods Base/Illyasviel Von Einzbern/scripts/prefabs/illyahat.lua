local assets =
{ 
    Asset("ANIM", "anim/illyahat.zip"),
    Asset("ANIM", "anim/illyahat_swap.zip"), 

    Asset("ATLAS", "images/inventoryimages/illyahat.xml"),
    Asset("IMAGE", "images/inventoryimages/illyahat.tex"),
}

local prefabs = 
{
}

local function OnEquip(inst, owner) 
    owner.AnimState:OverrideSymbol("swap_hat", "illyahat_swap", "swap_hat")

    owner.AnimState:Show("HAT")
    owner.AnimState:Show("HAT_HAIR")
    owner.AnimState:Hide("HAIR_NOHAT")
    owner.AnimState:Hide("HAIR")

    if owner:HasTag("player") then
        owner.AnimState:Hide("HEAD")
        owner.AnimState:Show("HEAD_HAT")
    end
end

local function OnUnequip(inst, owner) 
    owner.AnimState:Hide("HAT")
    owner.AnimState:Hide("HAT_HAIR")
    owner.AnimState:Show("HAIR_NOHAT")
    owner.AnimState:Show("HAIR")

    if owner:HasTag("player") then
        owner.AnimState:Show("HEAD")
        owner.AnimState:Hide("HEAD_HAT")
    end
end

local function fn()

    local inst = CreateEntity()
    
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("illyahat")
    inst.AnimState:SetBuild("illyahat")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("hat")

    if not TheWorld.ismastersim then
        return inst
    end

    inst.entity:SetPristine()

    inst:AddComponent("inspectable")

    inst:AddComponent("tradable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "illyahat"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/illyahat.xml"
	
	if not inst.components.characterspecific then
    inst:AddComponent("characterspecific")
end
 
	inst.components.characterspecific:SetOwner("illya")
	inst.components.characterspecific:SetStorable(true)
	inst.components.characterspecific:SetComment("Doesn't belong to me.") 
    
    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.HEAD
    inst.components.equippable:SetOnEquip(OnEquip)
    inst.components.equippable:SetOnUnequip(OnUnequip)
	inst.components.inventoryitem.keepondeath = true
	
	inst.components.equippable.dapperness = TUNING.DAPPERNESS_SMALL
	
    MakeHauntableLaunch(inst)

    return inst
end

return  Prefab("common/inventory/illyahat", fn, assets, prefabs)