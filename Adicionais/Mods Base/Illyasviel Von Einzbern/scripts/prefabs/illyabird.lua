
local assets=
{
    Asset("ANIM", "anim/illyabird.zip"),
    Asset("ANIM", "anim/swap_illyabird.zip"),
 
    Asset("ATLAS", "images/inventoryimages/illyabird.xml"),
    Asset("IMAGE", "images/inventoryimages/illyabird.tex"),
}

local function fn()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    MakeInventoryPhysics(inst)

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
	
	inst.AnimState:SetBank("illyabird")
    inst.AnimState:SetBuild("illyabird")
    inst.AnimState:PlayAnimation("idle")
	
	if not TheWorld.ismastersim then
        return inst
    end
 
    local function OnEquip(inst, owner)
        owner.AnimState:OverrideSymbol("swap_object", "swap_illyabird", "swap_illyabird")
        owner.AnimState:Show("ARM_carry")
        owner.AnimState:Hide("ARM_normal")
    end
 
    local function OnUnequip(inst, owner)
        owner.AnimState:Hide("ARM_carry")
        owner.AnimState:Show("ARM_normal")
    end
 
    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.keepondeath = true
    inst.components.inventoryitem.imagename = "illyabird"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/illyabird.xml"
     
    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip( OnEquip )
    inst.components.equippable:SetOnUnequip( OnUnequip )
	inst.components.inventoryitem.keepondeath = true
		
	if not inst.components.characterspecific then
    inst:AddComponent("characterspecific")
end
 
	inst.components.characterspecific:SetOwner("illya")
	inst.components.characterspecific:SetStorable(true)
	inst.components.characterspecific:SetComment("I don't feel safe near this...") 
	
	inst:AddComponent("inspectable")
			
	inst:AddTag("shadow")
 	inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(10)
    inst.components.weapon:SetRange(8, 10)
	inst.components.weapon:SetProjectile("illya_projectile")

    return inst
	
end
	
return  Prefab("common/inventory/illyabird", fn, assets)