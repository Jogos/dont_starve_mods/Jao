local assets =
{
	Asset("ANIM", "anim/grail.zip"),
    Asset("ATLAS", "images/inventoryimages/grail.xml"),
	Asset("IMAGE", "images/inventoryimages/grail.tex"),
}

local prefabs = 
{
}

-- Still does nothing. Dunno what it should do...
local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddMiniMapEntity()
    MakeInventoryPhysics(inst)

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    
	inst:AddTag("grail")
	
    inst.MiniMapEntity:SetIcon("grail.tex")
    inst.MiniMapEntity:SetPriority(5)
	
    if not TheWorld.ismastersim then
        return inst
    end
		
	inst.entity:SetPristine() 
    
    inst.AnimState:SetBank("grail")
    inst.AnimState:SetBuild("grail")
    inst.AnimState:PlayAnimation("idle", false)
	
    MakeHauntableLaunch(inst)
    inst:AddComponent("inspectable")
	
if not inst.components.characterspecific then
    inst:AddComponent("characterspecific")
end
 
	inst.components.characterspecific:SetOwner("illya")
	inst.components.characterspecific:SetStorable(true)
	inst.components.characterspecific:SetComment("I don't feel safe near this...") 
			
    inst:AddComponent("inventoryitem")
	inst.components.inventoryitem.imagename = "grail"
	inst.components.inventoryitem.atlasname = "images/inventoryimages/grail.xml"
	
    return inst
end

return Prefab( "common/inventory/grail", fn, assets) 