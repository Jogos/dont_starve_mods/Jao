
local MakePlayerCharacter = require "prefabs/player_common"


local assets = {
        Asset( "ANIM", "anim/illya.zip" ),
        Asset( "ANIM", "anim/ghost_illya_build.zip" ),
}
local prefabs = {
}

-- Custom starting items
local start_inv = {
	"illyahat",
	"grail",
	"illyabird",
}

-- When the character is revived from human
local function onbecamehuman(inst)
	-- Set speed when loading or reviving from ghost (optional)
	inst.components.locomotor.walkspeed = 6
	inst.components.locomotor.runspeed = 8.5
end

-- When loading or spawning the character
local function onload(inst)
    inst:ListenForEvent("ms_respawnedfromghost", onbecamehuman)

    if not inst:HasTag("playerghost") then
        onbecamehuman(inst)
    end
end


-- This initializes for both the server and client. Tags can be added here.
local common_postinit = function(inst) 
	-- Minimap icon
	inst.MiniMapEntity:SetIcon( "illya.tex" )
	
	inst:AddTag("illya_builder")
end

-- This initializes for the server only. Components are added here.
local master_postinit = function(inst)
	-- choose which sounds this character will play
	inst.soundsname = "willow"
	
	-- Uncomment if "wathgrithr"(Wigfrid) or "webber" voice is used
    --inst.talker_path_override = "dontstarve_DLC001/characters/"
	
	-- Stats	
	inst.components.health:SetMaxHealth(50)
	inst.components.hunger:SetMax(150)
	inst.components.sanity:SetMax(300)
	inst.components.temperature.mintemp = 20
	
	inst.components.health.SetPenalty = function(self, penalty)
    self.penalty = math.clamp(penalty, 0, 0)
	end
	
	inst.components.locomotor.walkspeed = 6
	inst.components.locomotor.runspeed = 8.5
	
    inst.components.combat.damagemultiplier = 0.75
		
	-- Hunger rate (optional)
	inst.components.hunger.hungerrate = 1 * TUNING.WILSON_HUNGER_RATE
	
	inst:ListenForEvent("equip", function()
		inst.AnimState:ClearOverrideSymbol("swap_body")
	end)
		
	inst.Transform:SetScale(0.7, 0.7, 0.7)
		
	inst.OnLoad = onload
    inst.OnNewSpawn = onload
end

return MakePlayerCharacter("illya", prefabs, assets, common_postinit, master_postinit, start_inv)
