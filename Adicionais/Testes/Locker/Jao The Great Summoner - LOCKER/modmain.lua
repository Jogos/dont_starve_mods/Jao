-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		 --
-- Any unauthorized use will be reported to the DMCA. 				 --
-- To use any file or sprite ask my permission.					 --
--										 --
-- Author: Paulo Victor de Oliveira Leal					 --
-- Contact: ciclopiano@gmail.com						 --
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------


-- Esta funcao de biblioteca nos permite usar um arquivo em um local especificado. 
-- Permite usar para chamar variaveis globais de ambiente sem inicializa-los em nossos arquivos.
modimport("libs/env.lua")

-- Acao de inicializacao.
use "data/actions/init"

-- Componente de inicializacao.
use "data/components/init"

-- Scripts necessarios
PrefabFiles = {
    "jao", "jaostaff", "sourceofmagic",
    "summonskip", "summonrhino", "summonchop", "summonjill", "summonshelly",
    "summonstonerhino", "summonstonechop", "summonstonejill", "summonstoneshelly",
    "summonstone", --"summonstonerhinospawner", "summonstonespawner",
}

-- Arquivos de importacao de imagens e animacao
Assets = {
    Asset( "IMAGE", "images/saveslot_portraits/jao.tex" ),
    Asset( "ATLAS", "images/saveslot_portraits/jao.xml" ),
    
    Asset( "IMAGE", "images/selectscreen_portraits/jao.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/jao.xml" ),
    
    Asset( "IMAGE", "images/selectscreen_portraits/jao_silho.tex" ),
    Asset( "ATLAS", "images/selectscreen_portraits/jao_silho.xml" ),
    
    Asset( "IMAGE", "bigportraits/jao.tex" ),
    Asset( "ATLAS", "bigportraits/jao.xml" ),
    
    Asset( "IMAGE", "images/map_icons/jao.tex" ),
    Asset( "ATLAS", "images/map_icons/jao.xml" ),
    
    Asset( "IMAGE", "images/avatars/avatar_jao.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_jao.xml" ),
    
    Asset( "IMAGE", "images/avatars/avatar_ghost_jao.tex" ),
    Asset( "ATLAS", "images/avatars/avatar_ghost_jao.xml" ),
    
    Asset("ATLAS",  "images/hud/magictab.xml"),
    Asset("IMAGE",  "images/hud/magictab.tex"),
    
    Asset( "IMAGE", "images/inventoryimages/sourceofmagic.tex" ),
    Asset( "ATLAS", "images/inventoryimages/sourceofmagic.xml" ),
    
    Asset( "IMAGE", "images/inventoryimages/summonstone.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summonstone.xml" ),
    
    Asset( "IMAGE", "images/inventoryimages/summonstonerhino.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summonstonerhino.xml" ),
    
    Asset( "IMAGE", "images/inventoryimages/summonstonechop.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summonstonechop.xml" ),
    
    Asset( "IMAGE", "images/inventoryimages/summonstonejill.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summonstonejill.xml" ),
    
    Asset( "IMAGE", "images/inventoryimages/summonstoneshelly.tex" ),
    Asset( "ATLAS", "images/inventoryimages/summonstoneshelly.xml" ),
    
     Asset("ANIM", "anim/ui_largechest_5x5.zip"),
    
}

-- Variaveis globais
local require = GLOBAL.require
local STRINGS = GLOBAL.STRINGS
local SITCOMMAND = GLOBAL.Action(4, true, true, 10, false, false, nil)
local SITCOMMAND_CANCEL = GLOBAL.Action(4, true, true, 10, false, false, nil)

-- Codigo da aba de receitas
local recipe_tab = AddRecipeTab(
"Invocations and Spells", -- Nome da aba
999, -- Posicao na lista de abas (999 e a ultima)
"images/hud/magictab.xml", -- Imagem da aba
"magictab.tex", -- (.tex) da imagem
"jaobuilder" -- Tag necessaria no personagem para visualizar essa aba
)

-- Receita da gema roxa
local purplegem_recipe = AddRecipe("purplegem", 
{Ingredient("goldnugget",5), Ingredient("tentaclespots", 3)}, 
recipe_tab, TECH.NONE, 
nil, nil, nil, 3)

-- Receita da gema 
local redgem_recipe = AddRecipe("redgem",
{Ingredient("goldnugget",5), Ingredient("mosquitosack", 3)},
recipe_tab, TECH.NONE,
nil, nil, nil, 3)

-- Receita da gema 
local bluegem_recipe = AddRecipe("bluegem",        
{Ingredient("goldnugget",5), Ingredient("ice", 3)},    
recipe_tab, TECH.NONE,
nil, nil, nil, 3)

-- Receitas dos tomos de feiticos
AddRecipe("book_birds",     {Ingredient("papyrus", 2),      Ingredient("bird_egg", 2)},                          recipe_tab, TECH.NONE)
AddRecipe("book_gardening", {Ingredient("papyrus", 2),      Ingredient("seeds", 1),      Ingredient("poop", 1)}, recipe_tab, TECH.NONE)
AddRecipe("book_sleep",     {Ingredient("papyrus", 2),      Ingredient("nightmarefuel", 2)},                     recipe_tab, TECH.NONE)
AddRecipe("book_brimstone", {Ingredient("papyrus", 2),      Ingredient("redgem", 1)},                            recipe_tab, TECH.NONE)
AddRecipe("book_tentacles", {Ingredient("papyrus", 2),      Ingredient("tentaclespots", 1)},                     recipe_tab, TECH.NONE)							

-- Receita para invoca o Shelly
local summonshelly_recipe = AddRecipe("summonshelly",
{GLOBAL.Ingredient("summonstoneshelly", 1, "images/inventoryimages/summonstoneshelly.xml")},
recipe_tab, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/summonshelly.xml", "summonshelly.tex")
summonshelly_recipe.tagneeded = false
summonshelly_recipe.builder_tag ="jaobuilder"
summonshelly_recipe.atlas = resolvefilepath("images/inventoryimages/summonshelly.xml")

-- Receita para invoca o Jill
local summonjill_recipe = AddRecipe("summonjill",
{GLOBAL.Ingredient("summonstonejill", 1, "images/inventoryimages/summonstonejill.xml")},
recipe_tab, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/summonjill.xml", "summonjill.tex")
summonjill_recipe.tagneeded = false
summonjill_recipe.builder_tag ="jaobuilder"
summonjill_recipe.atlas = resolvefilepath("images/inventoryimages/summonjill.xml")

-- Receita para invoca o Chop
local summonchop_recipe = AddRecipe("summonchop",
{GLOBAL.Ingredient("summonstonechop", 1, "images/inventoryimages/summonstonechop.xml")},
recipe_tab, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/summonchop.xml", "summonchop.tex")
summonchop_recipe.tagneeded = false
summonchop_recipe.builder_tag ="jaobuilder"
summonchop_recipe.atlas = resolvefilepath("images/inventoryimages/summonchop.xml")

-- Receita para invoca o Skip
local summonskip_recipe = AddRecipe("summonskip",
{GLOBAL.Ingredient("summonstone", 1, "images/inventoryimages/summonstone.xml")},
recipe_tab, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/summonskip.xml", "summonskip.tex")
summonskip_recipe.tagneeded = false
summonskip_recipe.builder_tag ="jaobuilder"
summonskip_recipe.atlas = resolvefilepath("images/inventoryimages/summonskip.xml")

-- Receita para invocar o Rhino
local summonrhino_recipe = AddRecipe("summonrhino",
{GLOBAL.Ingredient("summonstonerhino", 1, "images/inventoryimages/summonstonerhino.xml")},
recipe_tab, TECH.NONE,
nil, nil, nil, nil, nil,
"images/inventoryimages/summonrhino.xml", "summonrhino.tex")
summonrhino_recipe.tagneeded = false
summonrhino_recipe.builder_tag = "jaobuilder"
summonrhino_recipe.atlas = resolvefilepath("images/inventoryimages/summonrhino.xml")

-- Dados do persongem
STRINGS.CHARACTER_TITLES.jao = "The Great Summoner"
STRINGS.CHARACTER_NAMES.jao = "Jao"
STRINGS.CHARACTER_DESCRIPTIONS.jao = "*Teleport\n*Range Attack\n*Summons"
STRINGS.CHARACTER_QUOTES.jao = "\"Die asshole!\""

-- Dados das gemas
GLOBAL.STRINGS.RECIPE_DESC.PURPLEGEM = "Purple gem for magic items."
GLOBAL.STRINGS.RECIPE_DESC.REDGEM = "Red gem for magic items."
GLOBAL.STRINGS.RECIPE_DESC.BLUEGEM = "Blue gem for magic items."

-- Dados do cajado
GLOBAL.STRINGS.NAMES.JAOSTAFF = "Jao's Staff"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.JAOSTAFF = "Teleportation source, light and fire Jão"

-- Dados do item
GLOBAL.STRINGS.NAMES.SUMMONSTONESHELLY = "Summon Stone SHELLY"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONSTONESHELLY = "It allow invokes Shelly."

-- Dados do item
GLOBAL.STRINGS.NAMES.SUMMONSTONEJILL = "Summon Stone Jill"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONSTONEJILL = "It allow invokes Jill."

-- Dados do item
GLOBAL.STRINGS.NAMES.SUMMONSTONECHOP = "Summon Stone Chop"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONSTONECHOP = "It allow invokes Chop."

-- Dados do item
GLOBAL.STRINGS.NAMES.SUMMONSTONERHINO = "Summon Stone Rhino"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONSTONERHINO = "It allow invokes Rhino."

-- Dados do item
GLOBAL.STRINGS.NAMES.SUMMONSTONE = "Summon Stone"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONSTONE = "It allow invokes any invocation be combined with the right item."

-- Dados do item
GLOBAL.STRINGS.NAMES.SOURCEOFMAGIC = "The Source of Magic"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SOURCEOFMAGIC = "Grants power of invocation of John!"

-- Dados do Shelly
GLOBAL.STRINGS.NAMES.SUMMONSHELLY = "Shelly"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONSHELLY = "Your flying chest."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONSHELLY = "I can only have saved that item here..."

-- Dados do Jill
GLOBAL.STRINGS.NAMES.SUMMONJILL = "Jill"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONJILL = "High speed riding."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONJILL = "Their hair is beautifull today, Jill..."

-- Dados do Chop
GLOBAL.STRINGS.NAMES.SUMMONCHOP = "Chop"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONCHOP = "Great strength but very slowly."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONCHOP = "Even a stone is faster than that! Let's Chop accelerates!..."

-- Dados do Skip
GLOBAL.STRINGS.NAMES.SUMMONSKIP = "Skip"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONSKIP = "Great defense and aura of sanity."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONSKIP = "It's just a bunch of rocks..."

--Dados do Rhino
GLOBAL.STRINGS.NAMES.SUMMONRHINO = "Rhino"
GLOBAL.STRINGS.RECIPE_DESC.SUMMONRHINO = "Very strong, but quickly loses control."
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.SUMMONRHINO = "He is smarter than a door?!"

-- Icones do mapa de cada objeto
AddMinimapAtlas("images/inventoryimages/chop.xml")
AddMinimapAtlas("images/inventoryimages/rocky.xml")
AddMinimapAtlas("images/inventoryimages/rhino.xml")
AddMinimapAtlas("images/inventoryimages/jill.xml")
AddMinimapAtlas("images/inventoryimages/shelly.xml")
AddMinimapAtlas("images/inventoryimages/sourceofmagic_atlas.xml")
AddMinimapAtlas("images/inventoryimages/summonstone_map.xml")
AddMinimapAtlas("images/inventoryimages/summonstonerhino_map.xml")
AddMinimapAtlas("images/inventoryimages/summonstonechop_map.xml")
AddMinimapAtlas("images/inventoryimages/summonstonejill_map.xml")
AddMinimapAtlas("images/inventoryimages/summonstoneshelly_map.xml")

-- Funcoes de comando dos pets
AddReplicableComponent("followersitcommand")

-- Esperar
SITCOMMAND.id = "SITCOMMAND"
SITCOMMAND.str = "Wait"
SITCOMMAND.fn = function(act)
    local targ = act.target
    if targ and targ.components.followersitcommand and not targ.components.rideable then
            act.doer.components.locomotor:Stop()
            act.doer.components.talker:Say("Hold on")
            targ.components.followersitcommand:SetStaying(true)
            targ.components.followersitcommand:RememberSitPos("currentstaylocation", GLOBAL.Point(targ.Transform:GetWorldPosition())) 
            return true
    end
end
AddAction(SITCOMMAND)

-- Chamar
SITCOMMAND_CANCEL.id = "SITCOMMAND_CANCEL"
SITCOMMAND_CANCEL.str = "Call"
SITCOMMAND_CANCEL.fn = function(act)
    local targ = act.target
    if targ and targ.components.followersitcommand and not targ.components.rideable then
            act.doer.components.locomotor:Stop()
            act.doer.components.talker:Say("Hey, I need help!")
            targ.components.followersitcommand:SetStaying(false)
            return true
    end
end
AddAction(SITCOMMAND_CANCEL)

-- Animacao
AddComponentAction("SCENE", "followersitcommand", function(inst, doer, actions, rightclick)
    if rightclick and inst.replica.followersitcommand and not inst.components.rideable then
        if not inst.replica.followersitcommand:IsCurrentlyStaying() then
            table.insert(actions, GLOBAL.ACTIONS.SITCOMMAND)
        else
            table.insert(actions, GLOBAL.ACTIONS.SITCOMMAND_CANCEL)
        end
    end
end)

-- Animacao
AddComponentAction("SCENE", "followersitcommand", function(inst, doer, actions, rightclick)
    if rightclick and inst.replica.followersitcommand and not inst.components.rideable then
        if not inst.replica.followersitcommand:IsCurrentlyStaying() then
            table.insert(actions, GLOBAL.ACTIONS.SITCOMMAND)
        else
            table.insert(actions, GLOBAL.ACTIONS.SITCOMMAND_CANCEL)
        end
    end
end)

-- Habilitar troca com os pets
function HF_addtradablecomponenttoprefab(inst)
    if not inst.components.tradable then
        inst:AddComponent("tradable")
    end
end

-- Itens trocaveis
AddPrefabPostInit("umbrella", HF_addtradablecomponenttoprefab)
AddPrefabPostInit("torch", HF_addtradablecomponenttoprefab)


-----------------------------------------------------------
local containers = require "containers"
local params = {}

local OVERRIDE_WIDGETSETUP = false
local containers_widgetsetup_base = containers.widgetsetup

function containers.widgetsetup(container, prefab)
    local t = params[prefab or container.inst.prefab]
    if t ~= nil then
        for k, v in pairs(t) do
            container[k] = v
        end
        container:SetNumSlot(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
        if OVERRIDE_WIDGETSETUP then
            containers_widgetsetup_base(container, prefab)
        end
    else
        containers_widgetsetup_base(container, prefab)
    end
end

local function makeChest()
    local container =
    {
        widget =
        {
            slotpos = {},
            animbank = "ui_largechest_5x5",
            animbuild = "ui_largechest_5x5",
            pos = GLOBAL.Vector3(0, 200, 0),
            side_align_tip = 160,
        },
        type = "chest",
    }

    for y = 3, -1, -1 do
        for x = -1, 3 do
            table.insert(container.widget.slotpos, GLOBAL.Vector3(80 * x - 80 * 2 + 80, 80 * y - 80 * 2 + 80, 0))
        end
    end

    return container
end

params.largechest = makeChest()

for k, v in pairs(params) do
    containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, v.widget.slotpos ~= nil and #v.widget.slotpos or 0)
end

GLOBAL.LARGECHEST_CONFIG_SYNC = false

local containers_widgetsetup_custom = containers.widgetsetup
local MAXITEMSLOTS = containers.MAXITEMSLOTS

AddPrefabPostInit("world_network", function(inst)
    inst:AddComponent("largechest_configsync")
end)

AddPrefabPostInit("world_network", function(inst)
    if containers.widgetsetup ~= containers_widgetsetup_custom then
        OVERRIDE_WIDGETSETUP = true
        local containers_widgetsetup_base2 = containers.widgetsetup
        function containers.widgetsetup(container, prefab)
            containers_widgetsetup_base2(container, prefab)
            if container.type == "largechest" then
                container.type = "chest"
            end
        end
    end
    if containers.MAXITEMSLOTS < MAXITEMSLOTS then
        containers.MAXITEMSLOTS = MAXITEMSLOTS
    end
end)


-----------------------------------------------------------

-- Interpretacao da chuva para os pets
AddComponentPostInit("moisture", function(self)
    local old = self.GetMoistureRate
    self.GetMoistureRate = function(self)
        local oldvalue = old(self)
        local x, y, z = self.inst.Transform:GetWorldPosition()
        local ents = GLOBAL.TheSim:FindEntities(x, y, z, 4, {'sheltercarrier'})
        for k, v in pairs(ents) do 
            if v.components.inventory:GetEquippedItem(GLOBAL.EQUIPSLOTS.HANDS) and 
                v.components.inventory:GetEquippedItem(GLOBAL.EQUIPSLOTS.HANDS).prefab == "umbrella" then
                return 0
            end
            if v.components.inventory:GetEquippedItem(GLOBAL.EQUIPSLOTS.HANDS) and 
                v.components.inventory:GetEquippedItem(GLOBAL.EQUIPSLOTS.HANDS).prefab == "grass_umbrella" then
                return oldvalue * 0.5
            end
        end
        return oldvalue
    end
end)


-- container.widgetsetup, because people suck
local magical_chest =
{
	widget =
	{
		slotpos = {},
		animbank = "ui_chest_3x3",
		animbuild = "ui_chest_3x3",
		pos = GLOBAL.Vector3(0, 200, 0),
		side_align_tip = 160,
	},
	type = "chest",
}

for y = 2, 0, -1 do
	for x = 0, 2 do
		table.insert(magical_chest.widget.slotpos, GLOBAL.Vector3(80 * x - 80 * 2 + 80, 80 * y - 80 * 2 + 80, 0))
	end
end

local containers = GLOBAL.require("containers")
containers.MAXITEMSLOTS = math.max(containers.MAXITEMSLOTS, magical_chest.widget.slotpos ~= nil and #magical_chest.widget.slotpos or 0)

local _widgetsetup = containers.widgetsetup
function containers.widgetsetup(container, prefab, data)
	local pref = prefab or container.inst.prefab
	if pref == "summonshelly" then
		for k, v in pairs(magical_chest) do
			container[k] = v
		end
		container:SetNumSlots(container.widget.slotpos ~= nil and #container.widget.slotpos or 0)
	else
		return _widgetsetup(container, prefab, data)
	end
end


-- Arquivo de falas do personagem
STRINGS.CHARACTERS.JAO = require "speech_jao"

-- Nome no jogo
STRINGS.NAMES.JAO = "Jao"

-- Falas genericas
STRINGS.CHARACTERS.GENERIC.DESCRIBE.JAO = 
{
    GENERIC = "It's Jao!",
    ATTACKER = "This Jão seems wise....",
    MURDERER = "Murderer!",
    REVIVER = "Jão, friend of lost souls.",
    GHOST = "Jão could use a heart.",
}

-- Genero do personagem (male, female, or robot)
table.insert(GLOBAL.CHARACTER_GENDERS.MALE, "jao")

AddMinimapAtlas("images/map_icons/jao.xml")

-- Inicio
AddModCharacter("jao")