
local function server_trigger_resync(inst, self)
	self.opt_difficulty:set(LARGECHEST_OPT_DIFFICULTY)
	self.opt_set:set(true)
end

local function client_trigger_resync(inst, self)
	if not LARGECHEST_CONFIG_SYNC and self.opt_set:value() then
		LARGECHEST_CONFIG_SYNC = true
		LARGECHEST_OPT_DIFFICULTY = self.opt_difficulty:value()
		LARGECHEST_REGISTER_RECIPES()
	end
end

return Class(function(self, inst)
	self.inst = inst
	self.opt_difficulty = net_ushortint(inst.GUID, "largechest_opt_difficulty")
	self.opt_set = net_bool(inst.GUID, "largechest_opt_set")
	if TheWorld.ismastersim then
		inst:DoTaskInTime(0, server_trigger_resync, self)
	else
		inst.OnEntityReplicated = function(inst) 
			inst:DoPeriodicTask(1, client_trigger_resync, nil, self)
		end
	end
end)
