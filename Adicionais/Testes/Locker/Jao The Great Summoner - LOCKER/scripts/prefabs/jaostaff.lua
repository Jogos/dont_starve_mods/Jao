-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		 --
-- Any unauthorized use will be reported to the DMCA. 				 --
-- To use any file or sprite ask my permission.					 --
--										 --
-- Author: Paulo Victor de Oliveira Leal					 --
-- Contact: ciclopiano@gmail.com						 --
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------


-- Animacoes e imagens necessarias
local assets=
{
    Asset("ANIM", "anim/jaostaff.zip"),
    Asset("ANIM", "anim/swap_jaostaff.zip"),
    
    Asset("SOUND", "sound/common.fsb"),
    
    Asset("ATLAS", "images/inventoryimages/jaostaff.xml"),
    Asset("IMAGE", "images/inventoryimages/jaostaff.tex"),
}

-- Scripts necessarios
local prefabs =
{
    "torchfire",
}
-----------------------------------------
-- @param inst
-- @param attacker
-- @param target
-- @return

local teste = false
local SPAWN_DIST = 0

local function gerar_sd( )

    local val = math.random(15,35)
    --teste = false
    --while teste == false do      
     --   if (val >= 2 and val <= 30) then
     --       teste = true
    --    end
    --    val = math.random()
    --end
    return val
end

local function GetSpawnPoint(pt)
    local theta = math.random() * 2 * PI
    SPAWN_DIST = gerar_sd( )
    local radius = SPAWN_DIST
    local offset = FindWalkableOffset(pt, theta, radius, 12, true)
    return offset ~= nil and (pt + offset) or nil
end

local function SpawnItem(inst, pos)
    local pt = inst:GetPosition()
    
    local spawn_pt = GetSpawnPoint(pt)
    local summonstoneteste = {SpawnPrefab("summonstone"), SpawnPrefab("summonstonerhino"), SpawnPrefab("summonstonechop"), SpawnPrefab("summonstonejill"), SpawnPrefab("summonstoneshelly")}
    local i = 0
    if spawn_pt ~= nil then
        local summonstone = summonstoneteste[pos]
        if summonstone ~= nil then
            summonstone.Physics:Teleport(spawn_pt:Get())
            summonstone:FacePoint(pt:Get())
            return summonstone
        end       
    end
end
------------------------------------
local function OnPutInInventory(inst)
    if inst.fixtask == nil then
        inst.fixtask = inst:DoTaskInTime(1, FixSummonStone)
    end
end

local StartRespawn

local function StopRespawn(inst)
    --print("chester_eyebone - StopRespawn")
    if inst.respawntask ~= nil then
        inst.respawntask:Cancel()
        inst.respawntask = nil
        inst.respawntime = nil
    end
end


local function RebindSummonStone(inst, summonstone)
    summonstone = summonstone --or TheSim:FindFirstEntityWithTag("sourceofmagic")
    if summonstone ~= nil then
        --inst.AnimState:PlayAnimation("idle_loop", true)
        --OpenEye(inst)
        inst:ListenForEvent("death", function() StartRespawn(inst, TUNING.CHESTER_RESPAWN_TIME) end, summonstone)
        
        -- if chester.components.follower.leader ~= inst then
        --     chester.components.follower:SetLeader(inst)
        -- end
        return true
    end
end 


local function RespawnSummonStone(inst)
    --print("chester_eyebone - RespawnChester")
    StopRespawn(inst)
    --RebindSummonStone(inst, SpawnItem(inst, 1)) -- Skip
    RebindSummonStone(inst, SpawnItem(inst, 2)) -- Rhino
    RebindSummonStone(inst, SpawnItem(inst, 3)) -- Chop
    RebindSummonStone(inst, SpawnItem(inst, 4)) -- Jill
    RebindSummonStone(inst, SpawnItem(inst, 5)) -- Shelly
end



StartRespawn = function(inst, time)
    StopRespawn(inst)
    
    time = time or 0
    inst.respawntask = inst:DoTaskInTime(time, RespawnSummonStone)
    inst.respawntime = GetTime() + time
    --inst.AnimState:PlayAnimation("dead", true)
    --CloseEye(inst)
end

local function FixSummonStone(inst)
    inst.fixtask = nil
    --take an existing chester if there is one
    if not RebindSummonStone(inst) then
        --inst.AnimState:PlayAnimation("dead", true)
        --CloseEye(inst)
        
        if inst.components.inventoryitem.owner ~= nil then
            local time_remaining = 0
            local time = GetTime()
            if inst.respawntime and inst.respawntime > time then
                time_remaining = inst.respawntime - time		
            end
            StartRespawn(inst, time_remaining)
        end
    end
end

local function OnSave(inst, data)
    --print("chester_eyebone - OnSave")
    --data.EyeboneState = inst.EyeboneState
    if inst.respawntime ~= nil then
        local time = GetTime()
        if inst.respawntime > time then
            data.respawntimeremaining = inst.respawntime - time
        end
    end
end

local function OnLoad(inst, data)
    if data == nil then
        return
    end
    
    if data.respawntimeremaining ~= nil then
        inst.respawntime = data.respawntimeremaining + GetTime()
    end
end

local function GetStatus(inst)
    --print("smallbird - GetStatus")
    if inst.respawntask ~= nil then
        return "WAITING"
    end
end

-- @param inst
-- @param attacker
-- @param target
-- @return

-- Consumir 1 de fome por ataque
local function onattack_jaostaff(inst, attacker, target)
    
    if attacker and attacker.components.hunger then
        attacker.components.hunger:DoDelta(-1)
    end
    
end

local function fn()
    -- Instanciar o cajado
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    --inst.entity:AddTransform()
    --inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    
    -- Gerar estrutura
    MakeInventoryPhysics(inst)
    
    -- Ligar animacoes ao cajado
    inst.AnimState:SetBank("jaostaff")
    inst.AnimState:SetBuild("jaostaff")
    inst.AnimState:PlayAnimation("idle")
    
    if not TheWorld.ismastersim then
        return inst
    end
    
        inst:AddTag("irreplaceable")
        inst:AddTag("nonpotatable")
    
    -- Fazer troca de animacoes do cajado ao equipar
    local function OnEquip(inst, owner)
        inst.components.burnable:Ignite()
        owner.AnimState:OverrideSymbol("swap_object", "swap_jaostaff", "swap_orbstaff")
        owner.AnimState:Show("ARM_carry")
        owner.AnimState:Hide("ARM_normal")
        inst.SoundEmitter:SetParameter("torch", "intensity", 1)
        
        if inst.fire == nil then
            inst.fire = SpawnPrefab("torchfire")
            local follower = inst.fire.entity:AddFollower()
            follower:FollowSymbol(owner.GUID, "swap_object", 0, -220, 1)
        end
    end
    
    -- Fazer troca de animacoes do cajado ao desequipar
    local function OnUnequip(inst, owner)
        inst.components.burnable:Extinguish()
        owner.AnimState:OverrideSymbol("swap_object", "swap_jaostaff", "swap_orbstaff")
        owner.AnimState:Hide("ARM_carry")
        owner.AnimState:Show("ARM_normal")
        
        if inst.fire ~= nil then
            inst.fire:Remove()
            inst.fire = nil
        end
	
        inst.SoundEmitter:KillSound("torch")
        inst.SoundEmitter:PlaySound("dontstarve/common/fireOut")
        
        owner.components.combat.damage = owner.components.combat.defaultdamage 
    end
    
    -- Ao guardar
    local function onpocket(inst, owner)
        inst.components.burnable:Extinguish()
    end
    
    -- Pegar local do mouse para teleportar
    local function blinkstaff_reticuletargetfn()
        local player = ThePlayer
        local rotation = player.Transform:GetRotation() * DEGREES
        local pos = player:GetPosition()
        for r = 13, 1, -1 do
            local numtries = 2 * PI * r
            local pt = FindWalkableOffset(pos, rotation, r, numtries)
            if pt ~= nil then
                return pt + pos
            end
        end
    end
    
    -- Teleportar
    local function onblink(staff, pos, caster)
        if caster.components.sanity ~= nil then
            caster.components.sanity:DoDelta(-1)
        end
    end 
    
    inst.entity:SetPristine()
    
    -- Lista de componentes:
    inst:AddComponent("lighter")
    
    inst:AddComponent("burnable")
    inst.components.burnable.canlight = false
    inst.components.burnable.fxprefab = nil
    
    inst:AddComponent("blinkstaff")
    inst.components.blinkstaff.onblinkfn = onblink
    
    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.keepondeath = true
    inst.components.inventoryitem.imagename = "jaostaff"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/jaostaff.xml"
    
    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip( OnEquip )
    inst.components.equippable:SetOnUnequip( OnUnequip )
    inst.components.inventoryitem.keepondeath = true
    
    inst:AddComponent("inspectable")
    
    inst:AddTag("shadow")
    inst:AddComponent("weapon")
    inst.components.weapon:SetOnAttack(onattack_jaostaff)
    inst.components.weapon:SetDamage(30)
    inst.components.weapon:SetRange(8, 10)
    inst.components.weapon:SetProjectile("fire_projectile")
    
        inst.OnLoad = OnLoad
    inst.OnSave = OnSave
    
    inst.fixtask = inst:DoTaskInTime(1, FixSummonStone)
    
    return inst
    
end

return  Prefab("common/inventory/jaostaff", fn, assets)