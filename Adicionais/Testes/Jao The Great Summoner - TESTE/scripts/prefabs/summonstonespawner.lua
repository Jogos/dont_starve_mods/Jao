-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 			 --
-- Any unauthorized use will be reported to the DMCA. 							 --
-- To use any file or sprite ask my permission.									 --
--																				 --
-- Author: Paulo Victor de Oliveira Leal										 --
-- Contact: ciclopiano@gmail.com												 --
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------



-- Animacoes e imagens necessarias
local assets =
{
    Asset("ANIM", "anim/sourceofmagic.zip"),
    Asset("ATLAS", "images/inventoryimages/sourceofmagic_atlas.xml"),
    Asset("IMAGE", "images/inventoryimages/sourceofmagic_atlas.tex"),
    Asset("ATLAS", "images/inventoryimages/sourceofmagic.xml"),
    Asset("IMAGE", "images/inventoryimages/sourceofmagic.tex"),
}

-- Scripts necessarios
local prefabs = 
{
}

local teste = false
local SPAWN_DIST = 0

local function gerar_sd( )

    local val = math.random(2,30)
    --teste = false
    --while teste == false do      
     --   if (val >= 2 and val <= 30) then
     --       teste = true
    --    end
    --    val = math.random()
    --end
    return val
end

local function GetSpawnPoint(pt)
    local theta = math.random() * 2 * PI
    SPAWN_DIST = gerar_sd( )
    local radius = SPAWN_DIST
    local offset = FindWalkableOffset(pt, theta, radius, 12, true)
    return offset ~= nil and (pt + offset) or nil
end

local function SpawnItem(inst, pos)
    local pt = inst:GetPosition()
    
    local spawn_pt = GetSpawnPoint(pt)
    local summonstoneteste = {SpawnPrefab("summonstone"), SpawnPrefab("summonstonerhino"), SpawnPrefab("summonstonechop"), SpawnPrefab("summonstonejill"), SpawnPrefab("summonstoneshelly")}
    local i = 0
    if spawn_pt ~= nil then
        local summonstone = summonstoneteste[pos]
        if summonstone ~= nil then
            summonstone.Physics:Teleport(spawn_pt:Get())
            summonstone:FacePoint(pt:Get())
            return summonstone
        end       
    end
end
------------------------------------
local function OnPutInInventory(inst)
    if inst.fixtask == nil then
        inst.fixtask = inst:DoTaskInTime(1, FixSummonStone)
    end
end

local StartRespawn

local function StopRespawn(inst)
    --print("chester_eyebone - StopRespawn")
    if inst.respawntask ~= nil then
        inst.respawntask:Cancel()
        inst.respawntask = nil
        inst.respawntime = nil
    end
end


local function RebindSummonStone(inst, summonstone)
    summonstone = summonstone --or TheSim:FindFirstEntityWithTag("sourceofmagic")
    if summonstone ~= nil then
        --inst.AnimState:PlayAnimation("idle_loop", true)
        --OpenEye(inst)
        inst:ListenForEvent("death", function() StartRespawn(inst, TUNING.CHESTER_RESPAWN_TIME) end, summonstone)
        
        -- if chester.components.follower.leader ~= inst then
        --     chester.components.follower:SetLeader(inst)
        -- end
        return true
    end
end 


local function RespawnSummonStone(inst)
    --print("chester_eyebone - RespawnChester")
    StopRespawn(inst)
    --RebindSummonStone(inst, SpawnItem(inst, 1)) -- Skip
    RebindSummonStone(inst, SpawnItem(inst, 2)) -- Rhino
    RebindSummonStone(inst, SpawnItem(inst, 3)) -- Chop
    RebindSummonStone(inst, SpawnItem(inst, 4)) -- Jill
    RebindSummonStone(inst, SpawnItem(inst, 5)) -- Shelly
end



StartRespawn = function(inst, time)
    StopRespawn(inst)
    
    time = time or 0
    inst.respawntask = inst:DoTaskInTime(time, RespawnSummonStone)
    inst.respawntime = GetTime() + time
    --inst.AnimState:PlayAnimation("dead", true)
    --CloseEye(inst)
end

local function FixSummonStone(inst)
    inst.fixtask = nil
    --take an existing chester if there is one
    if not RebindSummonStone(inst) then
        --inst.AnimState:PlayAnimation("dead", true)
        --CloseEye(inst)
        
        if inst.components.inventoryitem.owner ~= nil then
            local time_remaining = 0
            local time = GetTime()
            if inst.respawntime and inst.respawntime > time then
                time_remaining = inst.respawntime - time		
            end
            StartRespawn(inst, time_remaining)
        end
    end
end

local function OnSave(inst, data)
    --print("chester_eyebone - OnSave")
    --data.EyeboneState = inst.EyeboneState
    if inst.respawntime ~= nil then
        local time = GetTime()
        if inst.respawntime > time then
            data.respawntimeremaining = inst.respawntime - time
        end
    end
end

local function OnLoad(inst, data)
    if data == nil then
        return
    end
    
    if data.respawntimeremaining ~= nil then
        inst.respawntime = data.respawntimeremaining + GetTime()
    end
end

local function GetStatus(inst)
    --print("smallbird - GetStatus")
    if inst.respawntask ~= nil then
        return "WAITING"
    end
end
------------------------------------


-- Principal
local function fn(Sim)
    
    -- Instanciar o cajado
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddMiniMapEntity()
    MakeInventoryPhysics(inst)
    
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    
    inst:AddTag("summonstone")
    inst:AddTag("irreplaceable")
    inst:AddTag("nonpotatable")
    
    inst.MiniMapEntity:SetIcon("sourceofmagic_atlas.tex")
    inst.MiniMapEntity:SetPriority(5)
    
    if not TheWorld.ismastersim then
        return inst
    end
    
    inst.entity:SetPristine() 
    -- Ligar animacoes ao cajado
    inst.AnimState:SetBank("grail")
    inst.AnimState:SetBuild("grail")
    inst.AnimState:PlayAnimation("idle", false)
    
    MakeHauntableLaunch(inst)
    inst:AddComponent("inspectable")
    
    if not inst.components.characterspecific then
        inst:AddComponent("characterspecific")
    end
    
    inst.components.characterspecific:SetOwner("jao")
    inst.components.characterspecific:SetStorable(true)
    inst.components.characterspecific:SetComment("I need my power!...") 
    
    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.keepondeath = true
    inst.components.inventoryitem.imagename = "sourceofmagic"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/sourceofmagic.xml"
    
    
    --inst.fixtask = inst:DoTaskInTime(1, SpawnItem)
    
    inst.OnLoad = OnLoad
    inst.OnSave = OnSave
    
    inst.fixtask = inst:DoTaskInTime(1, FixSummonStone)
    
    return inst
end

return Prefab( "common/inventory/sourceofmagic", fn, assets) 