-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		 --
-- Any unauthorized use will be reported to the DMCA. 				 --
-- To use any file or sprite ask my permission.					 --
--										 --
-- Author: Paulo Victor de Oliveira Leal					 --
-- Contact: ciclopiano@gmail.com						 --
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------


-- import
local MakePlayerCharacter = require "prefabs/player_common"

-- Imagens e animacoes
local assets = {
    Asset( "ANIM", "anim/jao.zip" ),
    Asset( "ANIM", "anim/ghost_jao_build.zip" ),
    Asset( "SOUND", "sound/webber.fsb"),
}

-- Scripts especiais
local prefabs = {
}

-- Itens iniciais de inventario
local start_inv = {
    "jaostaff", 	-- Cajado
    --"sourceofmagic",	-- Item para invocar
    "book_tentacles",	-- Livro dos tentaculos
    --"goldnugget",
    --"livinglog",
    --"saddle_basic",
    --"razor",
    "summonstone"
}


-- Inicializacao no cliente e no host
local common_postinit = function(inst) 
    -- Icone do minimapa
    inst.MiniMapEntity:SetIcon( "jao.tex" )
    -- Tags
    inst:AddTag("bookbuilder")
    inst:AddTag("jaobuilder")
    inst:AddTag("insomniac")
    
end

-- Perca de sanidade proximo de fogo
local function sanityfn(inst)
    local x,y,z = inst.Transform:GetWorldPosition()	
    local delta = 0
    local max_rad = 10
    local ents = TheSim:FindEntities(x,y,z, max_rad, {"fire"})
    for k,v in pairs(ents) do 
    	if v.components.burnable and v.components.burnable.burning then
            local sz = -TUNING.SANITYAURA_TINY
            local rad = v.components.burnable:GetLargestLightRadius() or 5
            sz = sz * ( math.min(max_rad, rad) / max_rad )
            local distsq = inst:GetDistanceSqToInst(v)
            delta = delta + sz/math.max(1, distsq)
    	end
    end
    
    return delta
end

-- Principal
local master_postinit = function(inst)
    
    -- Sons que o persongem ira fazer
    inst.soundsname = "wilson"	
    
    -- Permitir que leia livros
    inst:AddComponent("reader")	
    
    -- Estatisticas
    inst.components.health.fire_damage_scale = 1
    inst.components.health:SetMaxHealth(100)
    inst.components.health:StartRegen(1, 2)
    inst.components.hunger:SetMax(200)
    inst.components.sanity:SetMax(100)
    inst.components.sanity.custom_rate_fn = sanityfn
    inst.components.sanity.night_drain_mult = 0
    inst.components.sanity.neg_aura_mult = 0
    
    -- Dano realizado
    inst.components.combat.damagemultiplier = 1
    
    -- Conhecimento de magia
    inst.components.builder.magic_bonus = 2
    
    -- Taxas de temperatura
    inst.components.temperature.mintemp = 20
    inst.components.temperature.inherentsummerinsulation = TUNING.INSULATION_LARGE
    inst.components.temperature.inherentinsulation = TUNING.INSULATION_LARGE
    
    -- Penalidade de ressucitar
    inst.components.health.SetPenalty = function(self, penalty)
        self.penalty = math.clamp(penalty, 0, 0)
    end
    
    
    -- Desinvocar pets
    inst:ListenForEvent("onattackother",function(inst,data)
        print(data.target) --puts prefab name to log.txt
        local damage_mult = 1
        if (data.target:HasTag("summonedbyplayer") or
            data.target.prefab == "summonskip"     or 
            data.target.prefab == "summonrhino"    or 
            data.target.prefab == "summonchop"     or
            data.target.prefab == "summonshelly"   or
            data.target.prefab == "summonjill" )   then
            inst.components.talker:Say("Go rest for a while...!")
            damage_mult = 10000            
        end
        inst.components.combat.damagemultiplier = damage_mult
    end)
    
    
end

return MakePlayerCharacter("jao", prefabs, assets, common_postinit, master_postinit, start_inv)
