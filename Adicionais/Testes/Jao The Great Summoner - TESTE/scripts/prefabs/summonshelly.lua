-----------------------------------------------------------------------------------
-- This file has been developed exclusively for the mod "Jão the Great Summoner" --
--(http://steamcommunity.com/sharedfiles/filedetails/?id=572470943). 		 --
-- Any unauthorized use will be reported to the DMCA. 				 --
-- To use any file or sprite ask my permission.					 --
--										 --
-- Author: Paulo Victor de Oliveira Leal					 --
-- Contact: ciclopiano@gmail.com						 --
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

require "prefabutil"
-- Instanciar o cerebro
local brain = require "brains/summonshellybrain"

-- Imagens e animacoes a importar
local assets = {
    Asset("ATLAS", "images/inventoryimages/summonshelly.xml"),
    Asset("ATLAS", "images/inventoryimages/summonshelly.xml"),
    -- Container
    Asset("ANIM", "anim/ui_chester_shadow_3x4.zip"),
    -- Basico
    Asset("ANIM", "anim/bat_basic.zip"),
    -- Sons
    Asset("SOUND", "sound/bat.fsb"),
    Asset("SOUND", "sound/chester.fsb"),
    
}

-- Scripts necessarios
local prefabs = {
    "jao",
    "sourceofmagic"
}

---------------------------------------------------------------------
------------------------ TRATAR COMO PET ----------------------------
---------------------------------------------------------------------

-- Ligar ao mestre
local function linkToBuilder(inst, builder)
    if not builder.components.leader then
        builder:AddComponent("leader")
    end
    builder.components.leader:AddFollower(inst, true)
    
    --builder.components.health:DoDelta(-30, nil, nil, nil, nil, true)
    builder.components.sanity:DoDelta(-20)
    
    -- Emitir sons e animacoes
    if builder.components.combat.hurtsound ~= nil and builder.SoundEmitter ~= nil then
        builder.SoundEmitter:PlaySound(builder.components.combat.hurtsound)
    end
    
    builder:PushEvent("damaged", {})
end

---------------------------------------------------------------------
------------------------ SISTEMA DE ALVO ----------------------------
---------------------------------------------------------------------

-- Localizar alvo
local function NormalRetargetFn(inst)
    return FindEntity(inst, TUNING.PIG_TARGET_DIST, function(guy)
        return guy:HasTag("monster") or guy:HasTag("shadowcreature") and guy.components.health and not guy.components.health:IsDead()
        and inst.components.combat:CanTarget(guy)
    end, nil, { "character" }, nil)
end

-- Reacao ao ser atacado
local function OnAttacked(inst, data)
    local attacker = data.attacker
    inst.components.combat:SetTarget(attacker)
    inst.components.combat:ShareTarget(attacker, 30, function(dude)
        return dude:HasTag("summonedbyplayer") and dude.components.follower.leader == inst.components.follower.leader
    end, 5)
end

-- Intrucao para atacar outro
local function OnAttackOther(inst, data)
    local target = data.target
    inst.components.combat:ShareTarget(target, 30, function(dude)
        return dude:HasTag("summonedbyplayer") and dude.components.follower.leader == inst.components.follower.leader
    end, 5)
end
---------------------------------------------------------------------
--------------------- TRATAR COMO CONTAINER -------------------------
---------------------------------------------------------------------

local function OnOpen(inst)
    if not inst.components.health:IsDead() then
        inst.sg:GoToState("open")
    end
end

local function OnClose(inst)
    if not inst.components.health:IsDead() and inst.sg.currentstate.name ~= "transition" then
        inst.sg:GoToState("close")
    end
end

-- Principal
local function fn()
    
    -- Instanciar pet
    local inst = CreateEntity()
    
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()
    
    -- Gerar estrutura
    MakeGhostPhysics(inst, 1, .5)
    
    -- Construcao da sombra e outros
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.entity:SetPristine()
    inst.Transform:SetFourFaced()
    
    -- Escala
    local scaleFactor = 0.75
    inst.Transform:SetScale(scaleFactor, scaleFactor, scaleFactor)
    
    -- Ligar animacoes ao character
    inst.AnimState:SetBank("bat")
    inst.AnimState:SetBuild("bat_basic")
    --inst.AnimState:PlayAnimation("idle")--, true)
    --inst.AnimState:Hide("ARM_carry")
    --inst.AnimState:Show("ARM_normal")
    
    -- Setar icone no mapa
    inst.MiniMapEntity:SetIcon("shelly.tex")
    inst.MiniMapEntity:SetPriority(4)
    
    -- Tags de controle
    inst:AddTag("summonshelly")
    inst:AddTag("sheltercarrier")
    inst:AddTag("summonedbyplayer")
    inst:AddTag("scarytoprey")
    inst:AddTag("flying")
    
    if not TheWorld.ismastersim then
        return inst
    end
    
    -- Lista de componentes:    
    inst:AddComponent("combat")
    inst.components.combat.hiteffectsymbol = "bat_body"
    inst.components.combat:SetDefaultDamage(1)
    inst.components.combat:SetAttackPeriod(TUNING.BAT_ATTACK_PERIOD)
    inst.components.combat:SetRange(TUNING.BAT_ATTACK_DIST)
    inst.components.combat:SetRetargetFunction(3, NormalRetargetFn)
    
    local self = inst.components.combat
    local old = self.GetAttacked
    
    function self:GetAttacked(attacker, damage, weapon, stimuli)
        if attacker and (attacker:HasTag("player") and not attacker:HasTag("jaobuilder")) then
            return true
        end
        return old(self, attacker, damage, weapon, stimuli)
    end
    -- Fim do combate
    
    
    
    
    
    print("   container")
    inst:AddComponent("container")
    
    local containers = GLOBAL.require("containers")
    local oldwidgetsetup = containers.widgetsetupcontainers.widgetsetup = function(container, prefab) if not prefab and container.inst.prefab == "pak" then prefab = "summonshelly" end oldwidgetsetup(container, prefab)end
    
    inst.components.container:WidgetSetup("chester")
    inst.components.container.onopenfn = OnOpen
    inst.components.container.onclosefn = OnClose
    
    inst:AddComponent("follower")
    
    inst:AddComponent("followersitcommand")
    
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(1000)
    inst.components.health.fire_damage_scale = 0
    
    MakeMediumBurnableCharacter(inst, "bat_body")
    MakeMediumFreezableCharacter(inst, "bat_body")
    
    inst:AddComponent("sleeper")
    inst.components.sleeper:SetResistance(3)
    inst.components.sleeper.sleeptestfn = DefaultSleepTest
    inst.components.sleeper.waketestfn = DefaultWakeTest
    
    inst:AddComponent("inspectable")
    
    inst:AddComponent("timer")
    
    inst:AddComponent("inventory")
    
    inst:AddComponent("locomotor")
    inst.components.locomotor:SetSlowMultiplier( 1 )
    inst.components.locomotor:SetTriggersCreep(false)
    inst.components.locomotor.pathcaps = { ignorecreep = true }
    inst.components.locomotor.walkspeed = 10
     
    inst:AddComponent("lootdropper")
    
    inst:AddComponent("talker")
    inst.components.talker:StopIgnoringAll()
    
    -- Fim dos componentes
    
    -- Funcoes globais
    
    inst:SetBrain(brain)
    inst:SetStateGraph("SGsummonshelly")
    
    
    inst:AddComponent("perishable")
    inst.components.perishable.onperishreplacement = "sourceofmagic"
    inst.OnBuilt = linkToBuilder
    
    inst:ListenForEvent("attacked", OnAttacked)  
    inst:ListenForEvent("onattackother", OnAttackOther)
    
    return inst
end

return Prefab("common/summonshelly", fn, assets, prefabs)